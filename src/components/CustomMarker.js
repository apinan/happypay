import React, { Component } from 'react';
import {
    View, Text, StyleSheet, StatusBar
} from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';



export default class NavBar extends Component {
    constructor(props) {
        super()
    }
    render() {
        const { backTo } = this.props
        return (
            <View style={{ ...styleScoped.sectionNavbar, paddingBottom: backTo ? hp('1%') : hp('2%') }}>
                <StatusBar barStyle="dark-content" backgroundColor="transparent" translucent={true} />
                <View style={{ width: '20%' }}>
                    {backTo
                        ?
                        <Icon name="chevron-left" size={hp('4%')} color="#ED4F9D" onPress={() => Actions.push(this.props.backTo)} />
                        :
                        <View></View>
                    }
                </View>
                <View style={{ width: '60%' }}>
                    <Text style={{ fontSize: hp('2.3%'), textAlign: 'center' }}>{this.props.name}</Text>
                </View>
                <View style={{ width: '20%' }}>
                    <Text> </Text>
                </View>
            </View>
        )
    }
};


const styleScoped = StyleSheet.create({
    sectionNavbar: {
        paddingHorizontal: hp('2%'),
        marginTop: hp('3%'),
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: 'rgba(0,0,0,0.16)',
        paddingTop: hp('3%')
    }
})


