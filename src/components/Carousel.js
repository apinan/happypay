import React, { Component } from 'react';
import {
    View, Text, StyleSheet, StatusBar, TouchableOpacity, Dimensions, Image
} from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import theme from '../constants'
import Swiper from 'react-native-swiper'



export default class Carousel extends Component {
    constructor(props) {
        super()
    }
    state = {
        content: [
            {
                image: require('../assets/image/merry.png')
            },
            {
                image: require('../assets/image/merry.png')
            },
            {
                image: require('../assets/image/merry.png')
            },
        ]
    }
    render() {
        const { content } = this.state
        return (
            <View style={styles.container} >
                <Swiper
                    style={styles.wrapper}
                    horizontal={true}
                    autoplay
                    style={{ borderRadius: 10 }}
                    activeDot={
                        <View style={styles.activeDot} />
                    }
                    dot={
                        <View style={styles.dot}></View>
                    }
                    paginationStyle={{ bottom: -15 }}
                    loop
                    autoplayTimeout={5}
                >
                    {
                        content.map((el, index) => {
                            return (
                                <View style={styles.silde} key={index} >
                                    <Image source={el.image} style={{ width: '100%', height: '100%', resizeMode: 'stretch' }} />
                                </View>
                            )
                        })
                    }
                </Swiper>
            </View>
        )
    }
};


const styles = StyleSheet.create({
    dot: {
        width: hp('1%'),
        height: hp('1%'),
        backgroundColor: "#707070",
        borderRadius: hp('50%'),
        marginRight: hp('0.5%'),
        bottom: 0
    },
    activeDot: {
        width: wp('7%'),
        height: hp('1%'),
        backgroundColor: theme.colors.primary,
        borderRadius: hp('50%'),
        marginRight: hp('0.5%'),
    },
    container: {
        flex: 1,
        borderRadius: 10
    },

    wrapper: {
        borderRadius: 10
    },

    silde: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
        borderRadius: 10
    },

})


