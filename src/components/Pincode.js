
import React, { Component } from 'react';
import {
    View, Text, StyleSheet, TouchableOpacity,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Pincode extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pincode: []
        }
    }
    async inputPincode(value) {
        const { pincode } = this.state
        if (pincode.length >= 6) {
            return
        }
        await this.setState(prevState => ({
            pincode: [...prevState.pincode, value]
        }
        ))
        if (this.state.pincode.length >= 6) {
            this.props.pincodeReady(this.state.pincode)
        }
    }

    resetPincode(){
        this.setState({pincode:[]})
    }

    deletePincode() {
        const { pincode } = this.state
        const index = pincode.length - 1
        pincode.splice(index, 1)
        this.setState({ pincode: [...pincode] })
    }

    renderPincode() {
        const { pincode } = this.state
        let defaultPincode = 6
        let loop = []
        for (let index = 0; index < defaultPincode - pincode.length; index++) {
            loop.push(index)
        }
        return (
            <>
                {
                    loop.map((index) => <View style={{ ...styleScoped.pincode }} key={`pincode_${index}`}></View>)
                }
            </>
        )
    }

    renderPincodeActive() {
        const { pincode } = this.state
        let loop = []
        for (let index = 0; index < pincode.length; index++) {
            loop.push(index)
        }
        return (
            <>
                {
                    loop.map((index) => <View style={{ ...styleScoped.pincodeActive }} key={`pincode_active_${index}`}></View>)
                }
            </>
        )
    }





    render() {
        const { pincode } = this.state

        return (
            <View>
                <View style={{ paddingHorizontal: wp('18%') }}>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        {this.renderPincodeActive()}
                        {this.renderPincode()}

                    </View>

                </View>

                <View style={{ paddingHorizontal: hp('2%'), marginTop: hp('1%'), alignSelf: 'center' }}>
                    <View style={{ flexDirection: 'row', width: '100%' }}>
                        <TouchableOpacity style={{ width: '30%' }} onPress={() => this.inputPincode(1)}>
                            <Text style={{ ...styleScoped.numberPadLeft }}>1</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: '30%' }} onPress={() => this.inputPincode(2)}>
                            <Text style={{ ...styleScoped.numberPadCenter }}>2</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: '30%' }} onPress={() => this.inputPincode(3)}>
                            <Text style={{ ...styleScoped.numberPadRight }}>3</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row', width: '100%' }}>
                        <TouchableOpacity style={{ width: '30%' }} onPress={() => this.inputPincode(4)}>
                            <Text style={{ ...styleScoped.numberPadLeft }}>4</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: '30%' }} onPress={() => this.inputPincode(5)}>
                            <Text style={{ ...styleScoped.numberPadCenter }}>5</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: '30%' }} onPress={() => this.inputPincode(6)}>
                            <Text style={{ ...styleScoped.numberPadRight }}>6</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row', width: '100%' }}>
                        <TouchableOpacity style={{ width: '30%' }} onPress={() => this.inputPincode(7)}>
                            <Text style={{ ...styleScoped.numberPadLeft }}>7</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: '30%' }} onPress={() => this.inputPincode(8)}>
                            <Text style={{ ...styleScoped.numberPadCenter }}>8</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: '30%' }} onPress={() => this.inputPincode(9)}>
                            <Text style={{ ...styleScoped.numberPadRight }}>9</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center' }}>
                        <TouchableOpacity style={{ width: '30%' }} onPress={() => this.props.onCancle()}>
                            <Text style={{ textAlign: 'center', fontSize: hp('1.8%'), color: 'rgba(0,0,0,0.16)' }}>ย้อนกลับ</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: '30%' }} onPress={() => this.inputPincode(0)}>
                            <Text style={{ ...styleScoped.numberPadCenter }}>0</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: '30%' }} onPress={() => this.deletePincode()}>
                            <Icon name="backspace-outline" style={{ textAlign: 'center', color: '#12173C' }} size={hp('3%')} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        )
    }
};

const styleScoped = StyleSheet.create({
    pincode: {
        width: hp('2%'),
        height: hp('2%'),
        borderRadius: 50,
        backgroundColor: 'rgba(0,0,0,0.16)'
    },
    pincodeActive: {
        width: hp('2%'),
        height: hp('2%'),
        borderRadius: 50,
        backgroundColor: '#ED4F9D'
    },
    numberPadLeft: {
        fontSize: hp('3%'),
        textAlign: 'center',
        color: '#12173C',
        height: hp('10%'),
        textAlignVertical: 'center'
    },
    numberPadCenter: {
        fontSize: hp('3%'),
        textAlign: 'center',
        color: '#12173C',
        height: hp('10%'),
        textAlignVertical: 'center'
    },
    numberPadRight: {
        fontSize: hp('3%'),
        textAlign: 'center',
        color: '#12173C',
        height: hp('10%'),
        textAlignVertical: 'center'
    }
})


