import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView
} from 'react-native';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


export default class MenuBar extends Component {
    render() {
        return (
            <View style={{ paddingVertical: hp('1%'), paddingHorizontal: hp('2%'), backgroundColor: '#1B1C48', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>

                <TouchableOpacity>
                    <Icon name="autorenew" size={hp('3%')} style={{ color: 'white', textAlign: 'center', marginBottom: hp('0.5%') }} />
                    <Text style={{ color: 'white', textAlign: 'center' }}>Transection</Text>
                </TouchableOpacity>

                <TouchableOpacity>
                    <Icon name="home" size={hp('3%')} style={{ color: 'white', textAlign: 'center', marginBottom: hp('0.5%') }} />
                    <Text style={{ color: 'white', textAlign: 'center' }}>Home</Text>
                </TouchableOpacity>

                <TouchableOpacity>
                    <Icon name="heart" size={hp('3%')} style={{ color: 'white', textAlign: 'center', marginBottom: hp('0.5%') }} />
                    <Text style={{ color: 'white', textAlign: 'center' }}>Favorite</Text>
                </TouchableOpacity>

                <TouchableOpacity>
                    <Icon name="account" size={hp('3%')} style={{ color: 'white', textAlign: 'center', marginBottom: hp('0.5%') }} />
                    <Text style={{ color: 'white', textAlign: 'center' }}>Profile</Text>
                </TouchableOpacity>

            </View>
        )
    }
};


const styleScoped = StyleSheet.create({
    linearGradient: {
        height: hp('75%'),
        borderBottomRightRadius: hp('13%'),
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,
    }

})


