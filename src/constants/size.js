import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Platform } from 'react-native'


export default {
    base_size: Platform.OS === 'ios' ? hp('0%') : hp('0%'),
    font_weight : Platform.OS === 'ios' ? '400' : '600'
}