import Request from './Request'

const public_key = 'dGM4ZW1pa1dtWndFdGhwMWRMOVJIdWJJVWRNTU5sMlk6'
export const RequestToken = async (params) => {
    const { rememberCard, number, expirationMonth, expirationYear, securityCode, name } = params
    let jsonData = {
        rememberCard,
        card: {
            number,
            expirationMonth,
            expirationYear,
            securityCode,
            name
        }
    }
    Request.setHeaderAuthorization(`Basic ${public_key}`) //set header Authorization by public key
    const { data } = await Request.post('/api/v1/Login', jsonData)
    return data
}