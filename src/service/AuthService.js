import Request from './Request'


export const emailLogin = async (email, password) => {
    let form = new FormData();
    form.append('email', email);
    form.append('password', password);
    Request.setHeaderContentType("multipart/form-data")
    const { data } = await Request.post('/api/v1/Login', form)
    return data
}