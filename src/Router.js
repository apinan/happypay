import React from 'react';
import { Scene, Stack, Router } from 'react-native-router-flux';
// LOGIN 
import LoginMain from './scene/Login/Main'
import ChooseTypeLogin from './scene/Login/ChooseTypeLogin'
import EmailLogin from './scene/Login/EmailLogin'
import Telephone from './scene/Login/Telephone';
import OTP from './scene/Login/OTP';

// Register 
import ChooseTypeRegister from './scene/Register/ChooseTypeRegister'
import EmailRegister from './scene/Register/Email'
import VerifyPhone from './scene/Register/VerifyPhone'
import VerifyOTP from './scene/Register/VerifyOTP'
import RegisterEnterNewPin from './scene/Register/EnterNewPin'
import RegisterConfirmPin from './scene/Register/ConfirmPincode'
import FinishRegister from './scene/Register/AlreadyRegister'



//Main
import Main from './scene/Main'
import Transaction from './scene/Transaction'
import Setting from './scene/Setting'
import Notification from './scene/Notification'
import Favorite from './scene/Favorite'
import Charge from './scene/Charge'
import PaymentSummary from './scene/PaymentSummary'
import ScanQrcode from './scene/ScanQrcode'
import ChargePincode from './scene/ChargePincode'


//setting 
import ChangePassword from './scene/Setting/ChangePassword'
import ChangeLanguage from './scene/Setting/ChangeLanguage'
import UpdateEnterPincodeStepOne from './scene/Update/ChnagePincode/EnterOldPin'
import UpdateEnterPincodeStepTwo from  './scene/Update/ChnagePincode/EnterNewPin'
import UpdateEnterPincodeStepThree from './scene/Update/ChnagePincode/ConfirmNewPin'

//wallet
import MyWallet from './scene/Wallet/MyWallet'
import CreateCreditCard from './scene/Wallet/CreateCreditCard'
import CreateBankAccount from './scene/Wallet/CreateBankAccount'
import CreditAndDetail from './scene/Wallet/CreditAndDetail'
import UpdateCardName from './scene/Wallet/update/CardName'
import UpdateTransactionLimit from './scene/Wallet/update/TransactionLimit'
import AddBankAccount from './scene/Wallet/AddBankAccount'
import AddBankAccountPincode from './scene/Wallet/AddBankAccountPincode'
import CreateCreditCardPincode from './scene/Wallet/CreateCreditCardPincode'
import TopupPincode from './scene/Wallet/TopupPincode'
import TopupSuccess from  './scene/Wallet/TopupSuccess'
import WithDrawPincode from  './scene/Wallet/WithDrawPincode'
import WithDrawSuccess from  './scene/Wallet/WithDrawSuccess'



// paybill
import ListBill from './scene/Paybill/ListBill'
import ChooseOrganization from './scene/Paybill/ChooseOrganization'
import CustomerBillID from './scene/Paybill/CustomerId'
import DetailBillPayment from './scene/Paybill/DetailBillPayment'
import ChooseBillPayment from './scene/Paybill/ChoosePayment'
import BillPayment from './scene/Paybill/BillPayment'
import PayBillPincode from './scene/Paybill/PayBillPincode'


//Profile
import ProfileDetail from './scene/Profile/ProfileDetail'
import Profile from './scene/Profile/Profile'
import UpdateEmail from './scene/Profile/update/Email'
import UpdateTelephone from './scene/Profile/update/Telephone'
import UpdateAddress from './scene/Profile/update/Address'
import UpdateWebsite from './scene/Profile/update/Website'
import UpdateNameTaxInvoice  from './scene/Profile/update/NameTaxInvoice'
import UpdateAddressTaxInvoice from './scene/Profile/update/AddressTaxInvoice'

// /// installment payment
// import Payment from './scene/InstallmentPayment/payment'

// share 
import ShareInstallPayment from './scene/share/InstallmentPayment'
import ShareChooseMethod from './scene/share/ChooseMethod'

import UsageInfomation from './scene/Profile/UsageInfomation'
import NameTaxInvoice from './scene/Profile/NameTaxInvoice'
import EditNameMerchant from './scene/Profile/EditNameMerchant'
const RouterPage = () => {
  return (
    <Router>
      <Stack key="root">
        {/*  Login  */}
        <Scene initial key="LoginMain"   component={LoginMain}   title="LoginMain"   hideNavBar /> 
        <Scene key="ChooseTypeLogin"     component={ChooseTypeLogin}   title="ChooseTypeLogin"   hideNavBar />
        <Scene key="EmailLogin"          component={EmailLogin}   title="EmailLogin"   hideNavBar />   
        {/* Register */}
        <Scene key="ChooseTypeRegister"  component={ChooseTypeRegister}   title="ChooseTypeRegister"   hideNavBar />   
        <Scene key="EmailRegister"       component={EmailRegister}   title="EmailRegister"   hideNavBar />  
        <Scene key="VerifyPhone"         component={VerifyPhone}   title="VerifyPhone"   hideNavBar />
        <Scene key="VerifyOTP"           component={VerifyOTP}   title="VerifyOTP"   hideNavBar />   
        <Scene key="RegisterEnterNewPin"     component={RegisterEnterNewPin}   title="RegisterEnterNewPin"   hideNavBar />   
        <Scene key="RegisterConfirmPin"      component={RegisterConfirmPin}    title="RegisterConfirmPin"   hideNavBar />   
        <Scene key="FinishRegister"          component={FinishRegister}    title="FinishRegister"   hideNavBar />   

        

        




        {/* another */}
        <Scene  key="Main"               component={Main}   title="Main"   hideNavBar />      
        <Scene  key="Transaction"        component={Transaction}   title="Transaction"   hideNavBar /> 
        <Scene  key="Setting"            component={Setting}   title="Setting"   hideNavBar />
        <Scene  key="Notification"       component={Notification}   title="Notification"   hideNavBar /> 
        <Scene  key="Favorite"           component={Favorite}   title="Favorite"   hideNavBar />      
        <Scene  key="Charge"             component={Charge}   title="Charge"   hideNavBar />   
        <Scene  key="PaymentSummary"     component={PaymentSummary}   title="PaymentSummary"   hideNavBar />      
        <Scene  key="ScanQrcode"         component={ScanQrcode}   title="ScanQrcode"   hideNavBar />      
        <Scene  key="ChargePincode"      component={ChargePincode}   title="ChargePincode"   hideNavBar />      

        


        {/* wallet  */}
        <Scene  key="MyWallet"           component={MyWallet}   title="MyWallet"   hideNavBar />
        <Scene  key="CreateCreditCard"   component={CreateCreditCard}   title="CreateCreditCard"   hideNavBar /> 
        <Scene  key="CreateBankAccount"  component={CreateBankAccount}   title="CreateBankAccount"   hideNavBar /> 
        <Scene  key="CreditAndDetail"    component={CreditAndDetail}   title="CreditAndDetail"   hideNavBar /> 
        <Scene  key="UpdateCardName"     component={UpdateCardName}   title="UpdateCardName"   hideNavBar /> 
        <Scene  key="UpdateTransactionLimit"    component={UpdateTransactionLimit}   title="UpdateTransactionLimit"   hideNavBar /> 
        <Scene  key="AddBankAccount"            component={AddBankAccount}   title="AddBankAccount"   hideNavBar /> 
        <Scene  key="AddBankAccountPincode"     component={AddBankAccountPincode}   title="AddBankAccountPincode"   hideNavBar /> 
        <Scene  key="CreateCreditCardPincode"   component={CreateCreditCardPincode}   title="CreateCreditCardPincode"   hideNavBar /> 
        <Scene  key="TopupPincode"              component={TopupPincode}   title="TopupPincode"   hideNavBar /> 
        <Scene  key="TopupSuccess"              component={TopupSuccess}   title="TopupSuccess"   hideNavBar /> 
        <Scene  key="WithDrawPincode"           component={WithDrawPincode}   title="WithDrawPincode"   hideNavBar /> 
        <Scene  key="WithDrawSuccess"           component={WithDrawSuccess}   title="WithDrawSuccess"   hideNavBar /> 

   
        
        
        
        {/* <Scene  key="AddBankAccount"            component={AddBankAccount}   title="AddBankAccount"   hideNavBar />  */}

        
        

        
        

        
        {/* setting */}
        <Scene  key="ChangePassword"     component={ChangePassword}   title="ChangePassword"   hideNavBar /> 
        <Scene  key="ChangeLanguage"     component={ChangeLanguage}   title="ChangeLanguage"   hideNavBar /> 


        <Scene  key="UpdateEnterPincodeStepOne"     component={UpdateEnterPincodeStepOne}   title="UpdateEnterPincodeStepOne"   hideNavBar /> 
        <Scene  key="UpdateEnterPincodeStepTwo"     component={UpdateEnterPincodeStepTwo}   title="UpdateEnterPincodeStepTwo"   hideNavBar /> 
        <Scene  key="UpdateEnterPincodeStepThree"   component={UpdateEnterPincodeStepThree}   title="UpdateEnterPincodeStepThree"   hideNavBar /> 

        


        {/* profile */}
        <Scene  key="Profile"            component={Profile}   title="Profile"   hideNavBar />      
        <Scene  key="ProfileDetail"      component={ProfileDetail}   title="ProfileDetail"   hideNavBar />   
        <Scene  key="UpdateEmail"        component={UpdateEmail}   title="Email"   hideNavBar />
        <Scene  key="UpdateTelephone"    component={UpdateTelephone}   title="Telephone"   hideNavBar /> 
        <Scene  key="UpdateAddress"      component={UpdateAddress}   title="Address"   hideNavBar /> 
        <Scene  key="UpdateWebsite"      component={UpdateWebsite}   title="Website"   hideNavBar />      
        <Scene  key="UpdateNameTaxInvoice"     component={UpdateNameTaxInvoice}      title="Name for Tax Invoice"   hideNavBar />      
        <Scene  key="UpdateAddressTaxInvoice"  component={UpdateAddressTaxInvoice}   title="Address for Tax Invoice"   hideNavBar />      
        <Scene  key="Favorite"           component={Favorite}   title="Favorite"   hideNavBar />      

        {/* paybill */}
        <Scene  key="ListBill"           component={ListBill}   title="ListBill"   hideNavBar />      
        <Scene  key="ChooseOrganization"       component={ChooseOrganization}   title="ChooseOrganization"   hideNavBar />      
        <Scene  key="CustomerBillID"     component={CustomerBillID}   title="CustomerBillID"   hideNavBar />      
        <Scene  key="DetailBillPayment"  component={DetailBillPayment}   title="DetailBillPayment"   hideNavBar />      
        <Scene  key="ChooseBillPayment"  component={ChooseBillPayment}   title="ChooseBillPayment"   hideNavBar />      
        <Scene  key="BillPayment"        component={BillPayment}   title="BillPayment"   hideNavBar />      
        <Scene  key="PayBillPincode"     component={PayBillPincode}   title="PayBillPincode"   hideNavBar />      



        
        {/* share */}
        <Scene  key="ShareInstallPayment"   component={ShareInstallPayment}   title="ShareInstallPayment"   hideNavBar />      
        <Scene  key="ShareChooseMethod"     component={ShareChooseMethod}   title="ShareChooseMethod"   hideNavBar />      

        
        
        {/* installment payment
        <Scene  key="Payment"           component={Payment}   title="Payment"   hideNavBar />       */}





        



        <Scene  key="Telephone"          component={Telephone}   title="Telephone"   hideNavBar />      
        <Scene  key="OTP"                component={OTP}   title="OTP"   hideNavBar />      
        <Scene  key="UsageInfomation"    component={UsageInfomation}   title="UsageInfomation"   hideNavBar />      
        <Scene  key="NameTaxInvoice"     component={NameTaxInvoice}   title="NameTaxInvoice"   hideNavBar /> 
        <Scene  key="EditNameMerchant"   component={EditNameMerchant}   title="EditNameMerchant"   hideNavBar />      

      </Stack>
    </Router>
  );
};

export default RouterPage;