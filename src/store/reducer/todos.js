const initialState = {
    money: '35,000',
    main: 'Main'
}
const todos = (state = initialState, action) => {
    switch (action.type) {
        case 'CHANGE_MAIN_MENU':
            return {
                ...state,
                main: action.payload
            }
        default: return state
    }
}

export default todos