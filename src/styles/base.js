import { StyleSheet, Platform } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import theme from '../constants'

export default StyleSheet.create({
    container: {
        paddingHorizontal: hp('2%') 
    },
    cardCustom: {
        paddingVertical: hp('2%'),
        paddingHorizontal: hp('1%'),
        backgroundColor: 'white',
        borderRadius: 10
    },
    shadowDefault: {
        shadowColor: "#E2E2E7",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,
    },
    imageContain: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain'
    },
    imageStretch: {
        height: '100%',
        width: '100%',
        resizeMode: 'stretch'
    },
    divider: {
        borderBottomWidth: 0.5,
        borderBottomColor: 'rgba(0,0,0,0.16)',
    },
    dividerVertical: {
        borderRightColor: 'rgba(0,0,0,0.16)',
        borderRightWidth: 0.5,
        height: '100%'
    },
    btnPrimary: {
        borderRadius: 50,
        backgroundColor: theme.colors.primary,
        padding: hp('1.8%') ,
        fontSize: hp('2%')
    },
    btnSecondary: {
        padding: hp('1.8%') ,
        borderRadius: 50,
        borderWidth: 0.5,
        borderColor: '#C6C6C6'
    },
    bottomSheetForlogin: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.5,
        shadowRadius: 30,
        elevation: 20,
        backgroundColor: 'white',
        paddingHorizontal: hp('2%') ,
        paddingBottom: Platform.OS =='ios' ? hp('8%') : hp('4%'),
        paddingTop: hp('5%'),
        borderTopLeftRadius: 45,
        borderTopRightRadius: 45,
        marginBottom: hp('-2%') 
    },
    textNameTitleScene: {
        fontSize: hp('5%') ,
        fontWeight: 'bold',
        color: theme.colors.text
    }
})