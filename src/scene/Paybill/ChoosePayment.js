import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground, StatusBar
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'
import { Button } from 'react-native-elements';




export default class DetailBillPayment extends Component {


    render() {
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Water Bill" backTo="DetailBillPayment"></Navbar>
                <View style={{
                    flex: 1,
                    position: 'relative',
                    zIndex: 20,
                    backgroundColor: '#F2F2F7',
                    paddingBottom: hp('3%'),
                    paddingTop: hp('3%')
                }}>


                    <View style={{ paddingVertical: hp('1%'),  flexDirection: 'column', justifyContent: 'space-between', flex: 1 }}>

                        <View style={{ ...styles.container, }}>
                            <View style={{ borderRadius: 20, backgroundColor: 'white', padding: hp('2%'), marginBottom: hp('2%') }}>
                                <View style={{ ...styleScoped.ListBill }} >
                                    <View style={{ ...styleScoped.iconBill }}>
                                        <Image source={require('../../assets/icon/Water1.png')} style={{ ...styles.imageContain }} />
                                    </View>
                                    <View>
                                        <Text style={{ fontSize: hp('2%'), fontWeight: '300' }}>การประปานครหลวง</Text>
                                        <Text style={{ fontSize: hp('1.6%'), color: '#707070' }}>บิลค่าน้ำ</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: hp('2%') }}>
                                    <Text style={{ fontSize: hp('2.2%'), color: '#12173C' }}>Total Amount</Text>
                                    <Text style={{ fontSize: hp('2.2%'), color: '#12173C' }}><Text style={{ color: '#ED4F9D' }}>฿</Text> 35,600</Text>
                                </View>
                            </View>

                            <ImageBackground source={require('../../assets/image/BgHorizontal.png')} style={{ paddingVertical: hp('2%'), paddingHorizontal: hp('2%'), marginBottom: hp('2%') }} imageStyle={{ borderRadius: 20 }} >
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                        <View style={{ height: hp('2%'), width: hp('2%') }}>
                                            {/* <Image source={require('../../assets/icon/')} /> */}
                                        </View>
                                        <Text style={{ fontSize: hp('2.2%') }}>Happy Pay Wallet</Text>
                                    </View>
                                    <View>
                                        <Text style={{ fontSize: hp('2.2%') }}><Text style={{ color: '#ED4F9D' }}>฿</Text> 55,6000</Text>
                                    </View>
                                </View>
                                <View style={{
                                    borderRadius: 10,
                                    backgroundColor: 'rgba(0,0,0,0.16)',
                                    padding: hp('1%'),
                                    marginTop: hp('2%'),
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    width: hp('12%'),
                                    alignItems: 'center'
                                }}
                                >
                                    <Icon name="check" size={hp('2.4%')} style={{ color: 'white', marginRight: hp('1%') }} />
                                    <Text style={{ color: 'white', fontSize: hp('2%') }}>Select</Text>
                                </View>
                            </ImageBackground>

                            <ImageBackground source={require('../../assets/image/bg_kbank.png')} style={{ paddingVertical: hp('2%'), paddingHorizontal: hp('2%'), marginBottom: hp('2%') }} imageStyle={{ borderRadius: 20 }} >
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start' }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginBottom: hp('2%') }}>
                                        <View style={{ height: hp('4%'), width: hp('4%'), marginRight: hp('1%') }}>
                                            <Image source={require('../../assets/icon/kbank_white.png')} style={{ ...styles.imageContain }} />
                                        </View>
                                        <Text style={{ fontSize: hp('2.2%'), color: 'white' }}>บัตรส่วนตัว</Text>
                                    </View>
                                    <View style={{ marginTop: hp('0.8%') }}>
                                        <Text style={{ fontSize: hp('2.2%'), color: 'rgba(0,0,0,.16)' }}>฿ 55,6000</Text>
                                    </View>
                                </View>
                                <View style={{
                                    borderRadius: 10,
                                    borderColor: 'rgba(0,0,0,0.16)',
                                    borderWidth: 1,
                                    padding: hp('1%'),
                                    marginTop: hp('1%'),
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    width: hp('12%'),
                                    alignItems: 'center'
                                }}
                                >
                                    <Icon name="circle-outline" size={hp('2.4%')} style={{ color: 'rgba(0,0,0,0.16)', marginRight: hp('1%') }} />
                                    <Text style={{ color: 'rgba(0,0,0,.16)', fontSize: hp('2%') }}>Select</Text>
                                </View>
                            </ImageBackground>


                        </View>

                        <View style={{ ...styles.container, backgroundColor: 'white', paddingVertical: hp('2%'), borderTopWidth: 1, borderTopColor: '#C6C6C6', marginBottom: hp('-5%') }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: hp('4%') }}>
                                <Text style={{ fontSize: hp('2%'), color: '#12173C' }}>Total</Text>
                                <Text style={{ fontSize: hp('2%'), color: '#12173C' }}><Text style={{ color: '#ED4F9D' }}>฿</Text> 35,600</Text>
                            </View>
                            <Button title="Check bill" buttonStyle={{ ...styles.btnPrimary }} onPress={() => Actions.push('PayBillPincode')} />
                            <View style={{ marginBottom: hp('5%') }}></View>
                        </View>


                    </View>
                </View>




            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    boxInputSearch: {
        width: '100%',
        padding: hp('1%'),
        backgroundColor: '#EEEEF0',
        borderRadius: hp('1%'),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    boxListTransaction: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: hp('2%')
    },
    ListBill: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    iconBill: {
        width: hp('5%'),
        height: hp('5%'),
        marginRight: hp('2%')
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    }

})


