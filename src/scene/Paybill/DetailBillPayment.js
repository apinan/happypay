import React, { Component } from 'react';
import {
    View, Text, Image, StyleSheet,
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Navbar from '../../components/Navbar'
import { Button } from 'react-native-elements';




export default class DetailBillPayment extends Component {


    render() {
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Water Bill" backTo="CustomerBillID"></Navbar>
                <View style={{
                    backgroundColor: 'white',
                    paddingBottom: hp('3%'),
                }}>

                    <View style={{ backgroundColor: 'white', ...styles.container,marginTop: hp('3%') }}>

                        <View style={{ ...styleScoped.ListBill }} >
                            <View style={{ ...styleScoped.iconBill }}>
                                <Image source={require('../../assets/icon/Water1.png')} style={{ ...styles.imageContain }} />
                            </View>
                            <View>
                                <Text style={{ fontSize: hp('2%'), fontWeight: '300' }}>การประปานครหลวง</Text>
                                <Text style={{ fontSize: hp('1.6%'), color: '#707070' }}>บิลค่าน้ำ</Text>
                            </View>
                        </View>

                        <View style={{ ...styles.divider, paddingVertical: hp('1%') }}></View>

                        <View style={{ marginTop: hp('3%'), flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: hp('1.8%'), color: '#14133E' }}>เลขที่แจ้งค่าน้ำ</Text>
                            <Text style={{ fontSize: hp('1.8%'), color: '#14133E' }}>455896153859</Text>
                        </View>

                        <View style={{ marginTop: hp('2%'), flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: hp('1.8%'), color: '#14133E' }}>รหัสผู้ใช้</Text>
                            <Text style={{ fontSize: hp('1.8%'), color: '#14133E' }}>455896153859</Text>
                        </View>

                        <View style={{ ...styles.divider, paddingVertical: hp('2%') }}></View>

                        <View style={{ marginTop: hp('2%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start', marginBottom: hp('3%') }}>
                            <Text style={{ fontSize: hp('1.8%'), color: '#14133E' }}>Total Amount</Text>
                            <Text style={{ fontSize: hp('4%'), color: '#14133E' }}><Text style={{ color: '#ED4F9D' }}>฿</Text> 35,000</Text>
                        </View>


                        <Button title="Check bill" buttonStyle={{ ...styles.btnPrimary }} onPress={() => Actions.push('ChooseBillPayment')} />

                    </View>
                </View>




            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    boxInputSearch: {
        width: '100%',
        padding: hp('1%'),
        backgroundColor: '#EEEEF0',
        borderRadius: hp('1%'),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    boxListTransaction: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: hp('2%')
    },
    ListBill: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingVertical: hp('2%')
    },
    iconBill: {
        width: hp('5%'),
        height: hp('5%'),
        marginRight: hp('2%')
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    }

})


