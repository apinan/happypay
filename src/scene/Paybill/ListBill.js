import React, { Component } from 'react';
import {
    View, Text, Image, StyleSheet, TextInput, TouchableOpacity, Platform
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'



export default class Transaction extends Component {


    render() {
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Pay Bill" backTo="Main"></Navbar>
                <View style={{
                    flex: 1, 
                    position: 'relative',
                    zIndex: 20,
                    backgroundColor: '#F2F2F7',
                    paddingBottom: hp('3%'),
                }}>
                    <View style={{ marginTop: hp('3%'), ...styles.container }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginBottom: hp('3%') }}>
                            <View style={{ ...styleScoped.boxInputSearch, marginRight: hp('1%') }}>
                                <Icon name="magnify" size={hp('2.5%')} style={{ color: '#767680', marginRight: hp('2%') }} />
                                <TextInput placeholder="Search" style={{ fontSize: hp('2%'), width: '85%' , padding:0 }} />
                            </View>
                        </View>
                    </View>

                    <View style={{ backgroundColor: 'white', ...styles.container, paddingVertical: hp('1%') }}>

                        <TouchableOpacity style={{ ...styleScoped.ListBill }} onPress={() => Actions.push('ChooseOrganization')}>
                            <View style={{ ...styleScoped.iconBill }}>
                                <Image source={require('../../assets/icon/CreditCard.png')} style={{ ...styles.imageContain }} />
                            </View>
                            <Text style={{ fontSize: hp('2%'), fontWeight: '300' }}>Credit Card Bills</Text>
                        </TouchableOpacity>
                        <View style={{ ...styles.divider }}></View>

                        <View style={{ ...styleScoped.ListBill }} >
                            <View style={{ ...styleScoped.iconBill }}>
                                <Image source={require('../../assets/icon/WaterBills.png')} style={{ ...styles.imageContain }} />
                            </View>
                            <Text style={{ fontSize: hp('2%'), fontWeight: '300' }}>Water bills</Text>
                        </View>
                        <View style={{ ...styles.divider }}></View>


                        <View style={{ ...styleScoped.ListBill }} >
                            <View style={{ ...styleScoped.iconBill }}>
                                <Image source={require('../../assets/icon/Electriccity.png')} style={{ ...styles.imageContain }} />
                            </View>
                            <Text style={{ fontSize: hp('2%'), fontWeight: '300' }}>Electricity</Text>
                        </View>
                        <View style={{ ...styles.divider }}></View>



                        <View style={{ ...styleScoped.ListBill }} >
                            <View style={{ ...styleScoped.iconBill }}>
                                <Image source={require('../../assets/icon/Tax.png')} style={{ ...styles.imageContain }} />
                            </View>
                            <Text style={{ fontSize: hp('2%'), fontWeight: '300' }}>Tax</Text>
                        </View>
                        <View style={{ ...styles.divider }}></View>

                        <View style={{ ...styleScoped.ListBill }} >
                            <View style={{ ...styleScoped.iconBill }}>
                                <Image source={require('../../assets/icon/Phone.png')} style={{ ...styles.imageContain }} />
                            </View>
                            <Text style={{ fontSize: hp('2%'), fontWeight: '300' }}>ธนาคารอาคารสงเคราะห์</Text>
                        </View>
                        <View style={{ ...styles.divider }}></View>



                        <View style={{ ...styleScoped.ListBill }} >
                            <View style={{ ...styleScoped.iconBill }}>
                                <Image source={require('../../assets/icon/Insurance.png')} style={{ ...styles.imageContain }} />
                            </View>
                            <Text style={{ fontSize: hp('2%'), fontWeight: '300' }}>Insurance and Social Security Bill</Text>
                        </View>
                        <View style={{ ...styles.divider }}></View>

                        <View style={{ ...styleScoped.ListBill }} >
                            <View style={{ ...styleScoped.iconBill }}>
                                <Image source={require('../../assets/icon/Home.png')} style={{ ...styles.imageContain }} />
                            </View>
                            <Text style={{ fontSize: hp('2%'), fontWeight: '300' }}>Estate Bills</Text>
                        </View>
                        <View style={{ ...styles.divider }}></View>


                        <View style={{ ...styleScoped.ListBill }} >
                            <View style={{ ...styleScoped.iconBill }}>
                                <Image source={require('../../assets/icon/other.png')} style={{ ...styles.imageContain }} />
                            </View>
                            <Text style={{ fontSize: hp('2%'), fontWeight: '300' }}>Other Bills</Text>
                        </View>


                    </View>

                </View>




            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    boxInputSearch: {
        ...Platform.select({
            ios: {
                width: '100%',
                padding: hp('1%'),
                backgroundColor: '#EEEEF0',
                borderRadius: hp('1%'),
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
            },
            android: {
                width: '100%',
                paddingVertical: hp('0.5%'),
                paddingHorizontal: hp('2%'),
                backgroundColor: '#EEEEF0',
                borderRadius: hp('1%'),
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
            }
        })

    },
    boxListTransaction: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: hp('2%')
    },
    ListBill: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingVertical: hp('1.5%')
    },
    iconBill: {
        width: hp('4%'),
        height: hp('4%'),
        marginRight: hp('2%')
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    }

})


