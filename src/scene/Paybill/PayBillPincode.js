
import React, { Component } from 'react';
import {
    View, Text, StyleSheet, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Pincode from '../../components/Pincode'



export default class PaybillPincode extends Component {

    constructor(props) {
        super(props);
    }

    checkPincode(value) {
        Actions.push('BillPayment')
    }

    render() {
        return (
            <ImageBackground source={require('../../assets/image/Bg01.png')} style={{ flex: 1 }} >
                <View style={{ marginTop: hp('8%'), ...styles.container }}>
                    <Icon name="chevron-left" size={hp('4%')} color="#ED4F9D" onPress={() => Actions.push('BillPayment')} />
                </View>
                <View style={{ marginTop: hp('8%'), ...styles.container, paddingHorizontal: hp('4%') }}>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>Enter</Text>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>Pin Code.</Text>
                </View>


                <View style={{ marginTop: hp('10%') }}>
                    <Pincode
                        onCancle={() => { Actions.push('BillPayment') }}
                        pincodeReady={(pincode) => this.checkPincode(pincode)}></Pincode>
                </View>

            </ImageBackground >
        )
    }
};

const styleScoped = StyleSheet.create({

})


