import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground, Platform
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'
import { Button } from 'react-native-elements';
import theme from '../../constants'



export default class CustomerId extends Component {

    constructor(props) {
        super(props)
        this.state = {
            customerId: '013072225'
        }
    }
    render() {
        const { customerId } = this.state
        let colorInputCustomerId = customerId ? theme.colors.primary : theme.colors.secondary
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Water Bill" backTo="ChooseOrganization"></Navbar>
                <View style={{
                    flex: 1, position: 'relative',
                    zIndex: 20,
                    backgroundColor: 'white',
                    paddingBottom: hp('3%'),
                    paddingTop: hp('3%')
                }}>

                    <View style={{ backgroundColor: 'white', ...styles.container, paddingVertical: hp('1%') }}>

                        <View style={{ ...styleScoped.ListBill }} >
                            <View style={{ ...styleScoped.iconBill }}>
                                <Image source={require('../../assets/icon/Water1.png')} style={{ ...styles.imageContain }} />
                            </View>
                            <View>
                                <Text style={{ fontSize: hp('2%'), fontWeight: '300' }}>การประปานครหลวง</Text>
                                <Text style={{ fontSize: hp('1.6%'), color: '#707070' }}>บิลค่าน้ำ</Text>
                            </View>
                        </View>

                        <View style={{ marginTop: hp('2%'), flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text style={{ fontSize: hp('2%'), color: '#14133E' }}>Customer ID</Text>
                            <TextInput
                                style={{
                                    fontSize: hp('2%'),
                                    color: '#ED4F9D',
                                    width: '40%',
                                    textAlign: 'right',
                                    padding: 0
                                }}
                                value={customerId}
                                onChangeText={(text) => this.setState({ customerId: text })}></TextInput>
                        </View>
                        <View style={{ ...styleScoped.customDivider, borderBottomColor: colorInputCustomerId }}></View>

                        <Button title="Next" buttonStyle={{ ...styles.btnPrimary }} onPress={() => Actions.push('DetailBillPayment')} />

                    </View>
                </View>




            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    customDivider: {
        ...styles.divider,
        paddingVertical: Platform.OS === 'ios' ? hp('1%') : hp('0.5%'),
        borderBottomColor: '#ED4F9D',
        marginBottom: hp('5%')
    },
    boxInputSearch: {
        width: '100%',
        padding: hp('1%'),
        backgroundColor: '#EEEEF0',
        borderRadius: hp('1%'),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    boxListTransaction: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: hp('2%')
    },
    ListBill: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingVertical: hp('2%')
    },
    iconBill: {
        width: hp('5%'),
        height: hp('5%'),
        marginRight: hp('2%')
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    }

})


