import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'



export default class ChooseOrganization extends Component {


    render() {
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Water Bill" backTo="ListBill"></Navbar>
                <View style={{
                    flex: 1, position: 'relative',
                    zIndex: 20,
                    backgroundColor: '#F2F2F7',
                    paddingBottom: hp('3%'),
                    paddingTop: hp('3%')
                }}>

                    <View style={{ backgroundColor: 'white', ...styles.container}}>

                        <TouchableOpacity style={{ ...styleScoped.ListBill }} onPress={() => Actions.push('CustomerBillID')}>
                            <View style={{ ...styleScoped.iconBill }}>
                                <Image source={require('../../assets/icon/Water1.png')} style={{ ...styles.imageContain }} />
                            </View>
                            <Text style={{ fontSize: hp('2%'), fontWeight: '300' }}>การประปานครหลวง</Text>
                        </TouchableOpacity>
                        <View style={{ ...styles.divider }}></View>

                        <TouchableOpacity style={{ ...styleScoped.ListBill }} onPress={() => Actions.push('')}>
                            <View style={{ ...styleScoped.iconBill }}>
                                <Image source={require('../../assets/icon/Water2.png')} style={{ ...styles.imageContain }} />
                            </View>
                            <Text style={{ fontSize: hp('2%'), fontWeight: '300' }}>การประปาส่วนภูมิภาค</Text>
                        </TouchableOpacity>

                    </View>

                </View>
            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    boxInputSearch: {
        width: '100%',
        padding: hp('1%'),
        backgroundColor: '#EEEEF0',
        borderRadius: hp('1%'),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    boxListTransaction: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: hp('2%')
    },
    ListBill: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingVertical: hp('2%')
    },
    iconBill: {
        width: hp('5%'),
        height: hp('3%'),
        marginRight: hp('2%')
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    }

})


