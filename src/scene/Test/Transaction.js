import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground, Platform, KeyboardAvoidingView
} from 'react-native';
import styles from '../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../components/Navbar'
import { Overlay, Button } from 'react-native-elements'



export default class Transaction extends Component {

    constructor(props) {
        super(props)
        this.state = {
            visibleFilter: false
        }
    }


    toggleFilter() {
        this.setState({ visibleFilter: this.state.visibleFilter ? false : true })
    }

    renderTranfer() {
        return (
            <View style={{ ...styleScoped.boxListTransaction }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                    <View style={{ height: hp('4%'), width: hp('4%'), marginRight: hp('2%') }}>
                        <Image source={require('../assets/icon/getmoney.png')} style={{ ...styles.imageContain }} />
                    </View>
                    <View >
                        <Text style={{ color: '#12173C', fontSize: hp('1.8%'), marginBottom: hp('0.5%') }}>Transfer</Text>
                        <Text style={{ color: '#707070', fontSize: hp('1.5%') }}>09-10-2020 00:04 AM</Text>
                    </View>
                </View>
                <Text style={{ fontSize: hp('2%'), color: '#00A94F' }}> +10,000.00</Text>
            </View>
        )
    }

    renderPayBill() {
        return (
            <View style={{ ...styleScoped.boxListTransaction }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                    <View style={{ height: hp('4%'), width: hp('4%'), marginRight: hp('2%') }}>
                        <Image source={require('../assets/icon/bill.png')} style={{ ...styles.imageContain }} />
                    </View>
                    <View >
                        <Text style={{ color: '#12173C', fontSize: hp('1.8%'), marginBottom: hp('0.5%') }}>PayBills</Text>
                        <Text style={{ color: '#707070', fontSize: hp('1.5%') }}>09-10-2020 00:04 AM</Text>
                    </View>
                </View>
                <Text style={{ fontSize: hp('2%'), color: '#12173C' }}> -10,000.00</Text>
            </View>
        )
    }

    renderWithdraw() {
        return (
            <View style={{ ...styleScoped.boxListTransaction }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                    <View style={{ height: hp('4%'), width: hp('4%'), marginRight: hp('2%') }}>
                        <Image source={require('../assets/icon/withdraw.png')} style={{ ...styles.imageContain }} />
                    </View>
                    <View >
                        <Text style={{ color: '#12173C', fontSize: hp('1.8%'), marginBottom: hp('0.5%') }}>Withdraw</Text>
                        <Text style={{ color: '#707070', fontSize: hp('1.5%') }}>09-10-2020 00:04 AM</Text>
                    </View>
                </View>
                <Text style={{ fontSize: hp('2%'), color: '#12173C' }}> -10,000.00</Text>
            </View>
        )
    }

    renderTopup() {
        return (
            <View style={{ ...styleScoped.boxListTransaction }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                    <View style={{ height: hp('4%'), width: hp('4%'), marginRight: hp('2%') }}>
                        <Image source={require('../assets/icon/topup.png')} style={{ ...styles.imageContain }} />
                    </View>
                    <View >
                        <Text style={{ color: '#12173C', fontSize: hp('1.8%'), marginBottom: hp('0.5%') }}>Topup</Text>
                        <Text style={{ color: '#707070', fontSize: hp('1.5%') }}>09-10-2020 00:04 AM</Text>
                    </View>
                </View>
                <Text style={{ fontSize: hp('2%'), color: '#00A94F' }}> -10,000.00</Text>
            </View>
        )
    }


    renderOverlayFilter() {
        const { visibleFilter } = this.state
        return (
            <Overlay isVisible={visibleFilter}
                onBackdropPress={() => this.toggleFilter()}
                overlayStyle={{ ...styleScoped.boxOverlayFilter }} >
                <Text style={{ ...styleScoped.textTitleOverlayFilter }}>Search Filter</Text>
                <View style={{ ...styleScoped.warpperSelectFilter }}>
                    <TouchableOpacity style={{ ...styleScoped.boxSelectFilter }}>
                        <Text style={{ ...styleScoped.textSelectFilter }}>เรียกเก็บเงิน</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ ...styleScoped.boxSelectFilter }}>
                        <Text style={{ ...styleScoped.textSelectFilter }}>Pay Bill</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ ...styleScoped.boxSelectFilter }}>
                        <Text style={{ ...styleScoped.textSelectFilter }}>Topup</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ ...styleScoped.boxSelectFilter }}>
                        <Text style={{ ...styleScoped.textSelectFilter }}>Withdraw</Text>
                    </TouchableOpacity>
                </View>
                <Button title="Confirm Filter" buttonStyle={{ ...styles.btnPrimary }} onPress={() => this.toggleFilter()} />

            </Overlay>
        )
    }



    render() {
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Transaction" ></Navbar>
                <ScrollView style={{
                    flex: 1, position: 'relative',
                    zIndex: 20,
                    backgroundColor: 'white',
                    borderBottomLeftRadius: hp('5%'),
                    borderBottomRightRadius: hp('5%'),
                    paddingBottom: hp('3%'),
                    paddingTop: hp('3%')
                }}>
                    <View style={{ ...styles.container }}>
                        <KeyboardAvoidingView >
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginBottom: hp('3%') }}>
                                <View style={{ ...styleScoped.boxInputSearch, marginRight: hp('1%') }}>
                                    <Icon name="magnify" size={hp('2.5%')} style={{ color: '#767680', marginRight: hp('2%') }} />
                                    <TextInput placeholder="Search" style={{ fontSize: hp('2%'), width: '80%', padding: 0 }} />
                                </View>
                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: "center" }} onPress={() => this.toggleFilter()}>
                                    <Icon name="tune" size={hp('3.3%')} color="#ED4F9D" style={{ marginRight: hp('1%') }} />
                                    <Text style={{ fontSize: hp('2%'), color: '#ED4F9D' }} >Filter</Text>
                                </TouchableOpacity>

                            </View>
                        </KeyboardAvoidingView>



                        {this.renderTranfer()}
                        <View style={{ ...styles.divider }}></View>
                        {this.renderTranfer()}
                        <View style={{ ...styles.divider }}></View>
                        {this.renderPayBill()}
                        <View style={{ ...styles.divider }}></View>
                        {this.renderWithdraw()}
                        <View style={{ ...styles.divider }}></View>
                        {this.renderTopup()}

                    </View>

                </ScrollView>

                {/* menu bar */}
                <View style={{ height: hp('15%'), marginTop: hp('-5%'), position: 'relative', zIndex: 0 }}>
                    <ImageBackground source={require('../assets/image/bg_main_bottom.png')} style={{ flex: 1 }}>
                        <View style={{ paddingTop: hp('6%'), ...styles.container }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: hp('1%') }}>
                                <TouchableOpacity onPress={() => Actions.push('Main')}>
                                    <View style={{ height: hp('3%'), width: wp('20%') }}>
                                        <Image source={require('../assets/image/menu_home.png')} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                                    </View>
                                    <Text style={{ textAlign: 'center', marginTop: hp('1%'), color: 'white', fontSize: hp('1.6%') }}>Home</Text>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <View style={{ height: hp('3%'), width: wp('20%') }}>
                                        <Image source={require('../assets/image/menu_transaction_active.png')} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                                    </View>
                                    <Text style={{ textAlign: 'center', marginTop: hp('1%'), color: '#979797', fontSize: hp('1.6%') }}>Transaction</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Actions.push('Setting')}>
                                    <View style={{ height: hp('3%'), width: wp('20%') }}>
                                        <Image source={require('../assets/image/menu_setting.png')} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                                    </View>
                                    <Text style={{ textAlign: 'center', marginTop: hp('1%'), color: '#979797', fontSize: hp('1.6%') }}>Setting</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ImageBackground>
                </View>


                {this.renderOverlayFilter()}

            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    boxOverlayFilter: {
        paddingVertical: hp('2%'),
        paddingHorizontal: hp('2%'),
        borderRadius: 20,
        width: wp('90%'),
        marginTop: hp('-40%')
    },
    boxSelectFilter: {
        padding: hp('1.8%'),
        borderColor: 'rgba(0,0,0,0.16)',
        borderRadius: 50,
        borderWidth: 0.5,
        width: '49%',
        marginTop: hp('1%')
    },
    textSelectFilter: {
        fontSize: hp('1.8%'),
        color: '#12173C',
        textAlign: 'center'
    },
    textTitleOverlayFilter: {
        fontSize: hp('2.2%'),
        color: '#12173C',
    },
    warpperSelectFilter: {
        marginVertical: hp('1.5%'),
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    },
    boxInputSearch: {
        width: wp('72%'),
        paddingVertical: Platform.OS === 'ios' ? hp('1%') : hp('0.5%'),
        paddingHorizontal: hp('1%'),
        backgroundColor: '#EEEEF0',
        borderRadius: hp('1%'),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    boxListTransaction: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: hp('2%')
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    }

})


