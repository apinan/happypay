import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'
import { Switch } from 'react-native-switch';
import { GoogleSignin } from '@react-native-community/google-signin';

import AsyncStorage from '@react-native-async-storage/async-storage';



export default class Transaction extends Component {

    state = {
        valueSwicth: true
    }
    async logout() {

        try {

            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();


            // setuserInfo([]);
        } catch (error) {
            console.error(error);
        }

        await AsyncStorage.removeItem('googleLogin')
        await AsyncStorage.removeItem('user_pincode')

        Actions.push('LoginMain')

    }
    render() {
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Setting" ></Navbar>
                <View style={{
                    flex: 1,
                    position: 'relative',
                    zIndex: 20,
                    backgroundColor: '#F2F2F7',
                    borderBottomLeftRadius: hp('5%'),
                    borderBottomRightRadius: hp('5%'),
                    paddingBottom: hp('3%'),
                    paddingTop: hp('3%')
                }}>
                    {/* password */}
                    <View style={{ ...styles.container, }}>
                        <Text style={{ fontSize: hp('2%'), fontWeight: '200', color: 'rgba(0,0,0,0.16)' }}>Password</Text>
                    </View>
                    <View style={{ ...styleScoped.boxListSetting, ...styles.container }}>
                        <TouchableOpacity style={{ ...styleScoped.listSetting }} onPress={() => Actions.push('ChangePassword')}>
                            <Text style={{ ...styleScoped.titleSetting }}>Change Password</Text>
                            <Icon name="chevron-right" size={hp('2.3%')} />
                        </TouchableOpacity>
                        <View style={{ ...styles.divider }}></View>
                        <TouchableOpacity style={{ ...styleScoped.listSetting }} onPress={() => Actions.push('UpdateEnterPincodeStepOne')}>
                            <Text style={{ ...styleScoped.titleSetting }}>Change Pin Code</Text>
                            <Icon name="chevron-right" size={hp('2.3%')} />
                        </TouchableOpacity>
                    </View>
                    {/* end password */}

                    {/* Notification */}
                    <View style={{ ...styles.container, marginTop: hp('3%') }}>
                        <Text style={{ fontSize: hp('2%'), fontWeight: '200', color: 'rgba(0,0,0,0.16)' }}>Notification</Text>
                    </View>
                    <View style={{ ...styleScoped.boxListSetting, ...styles.container }}>
                        <View style={{ ...styleScoped.listSetting }}>
                            <Text style={{ ...styleScoped.titleSetting }}>Recive notification</Text>
                            <Switch
                                value={this.state.valueSwicth}
                                onValueChange={(val) => this.setState({ valueSwicth: val })}
                                disabled={false}
                                activeText={'On'}
                                inActiveText={'Off'}
                                circleSize={15}
                                barHeight={20}
                                circleBorderWidth={0}
                                backgroundActive={'#32D74B'}
                                backgroundInactive={'gray'}
                                circleActiveColor={'white'}
                                circleInActiveColor={'white'}
                                changeValueImmediately={true}
                                changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
                                innerCircleStyle={{ alignItems: "center", justifyContent: "center" }} // style for inner animated circle for what you (may) be rendering inside the circle
                                outerCircleStyle={{}} // style for outer animated circle
                                renderActiveText={false}
                                renderInActiveText={false}
                                switchLeftPx={1.8} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
                                switchRightPx={1.8} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
                                switchWidthMultiplier={2.5} // multipled by the `circleSize` prop to calculate total width of the Switch
                                switchBorderRadius={30} // Sets the border Radius of the switch slider. If unset, it remains the circleSize.
                            />
                        </View>
                    </View>
                    {/* end Notification */}

                    {/* Language */}
                    <View style={{ ...styles.container, marginTop: hp('3%') }}>
                        <Text style={{ fontSize: hp('2%'), fontWeight: '200', color: 'rgba(0,0,0,0.16)' }}>Language</Text>
                    </View>
                    <View style={{ ...styleScoped.boxListSetting, ...styles.container }}>
                        <TouchableOpacity style={{ ...styleScoped.listSetting }} onPress={() => Actions.push('ChangeLanguage')}>
                            <Text style={{ ...styleScoped.titleSetting }}>Change Language</Text>
                            <Icon name="chevron-right" size={hp('2.3%')} />
                        </TouchableOpacity>
                    </View>
                    {/* end Language */}

                    {/* Logout */}
                    <View style={{ ...styleScoped.boxListSetting, ...styles.container, marginTop: hp('4%') }}>
                        <TouchableOpacity style={{ ...styleScoped.listSetting }} onPress={() => this.logout()}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <Icon name="logout" size={hp('2.3%')} style={{ marginRight: hp('1%'), color: '#EB001B' }} />
                                <Text style={{ ...styleScoped.titleSetting, color: '#EB001B' }}>Logout</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    {/* end Logout */}

                </View>

            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    boxInputSearch: {
        width: wp('72%'),
        padding: hp('1%'),
        backgroundColor: '#EEEEF0',
        borderRadius: hp('1%'),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    boxListSetting: {
        backgroundColor: 'white',
        marginTop: hp('1%'),
    },
    listSetting: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: hp('2%')
    },
    titleSetting: {
        fontSize: hp('1.8%'),
        fontWeight: '300',
        color: '#12173C'
    },
    boxListTransaction: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomColor: '#707070',
        borderBottomWidth: 0.5,
        paddingVertical: hp('2%')
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    }

})


