
import React, { Component } from 'react';
import {
    View, Text, StyleSheet, ImageBackground
} from 'react-native';
import styles from '../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Pincode from '../components/Pincode'
import TouchID from 'react-native-touch-id';
import { sha256 } from 'react-native-sha256';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Button, Overlay } from 'react-native-elements';
import theme from '../constants'
const optionalConfigObject = {
    // title: 'สแกนลายนิ้วมือเพื่อเข้าสู่ระบบ', // Android
    imageColor: '#e00606', // Android
    imageErrorColor: '#ff0000', // Android
    sensorDescription: 'สแกนลายนิ้วมือเพื่อเข้าสู่ระบบ', // Android
    sensorErrorDescription: 'Failed', // Android
    cancelText: 'ยกเลิก', // Android
    fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
    unifiedErrors: false, // use unified error messages (default false)
    passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
};

export default class ChargePincode extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false
        }
    }

    TouchID() {
        TouchID.authenticate('to demo this react-native component', optionalConfigObject)
            .then(success => {
                console.log('finger', success)
                Actions.push('Charge')
            })
            .catch(error => {
                alert('Authentication Failed');
            });
    }

    async checkPincode(value) {
        try {
            const inputPincode = value.join('')
            const user_pincode = await AsyncStorage.getItem('user_pincode')
            const public_key = await AsyncStorage.getItem('public_key')
            const pincode = await sha256(`${inputPincode}${public_key}`)
            if (pincode == user_pincode) {
                Actions.push('Charge')
            } else {
                this.setState({ visible: true })
            }
        } catch (error) {
            console.log(error)
        }
    }

    renderWrongMessage() {
        const { visible } = this.state
        return (
            <>
                <Overlay isVisible={visible} overlayStyle={{ width: wp('50%') }}>
                    <Text style={{ fontSize: hp('2.8%'), textAlign: 'center' }}>Pincode is worng !</Text>
                    <View style={{ marginTop: hp('3%'), alignItems: 'center' }}>
                        <Button title="ตกลง" onPress={() => this.reset()} buttonStyle={{ backgroundColor: theme.colors.primary, width: wp('20%') }} />
                    </View>
                </Overlay>
            </>
        )
    }

    reset() {
        this.refs.pincode.resetPincode()
        this.setState({ visible: false })
    }

    componentDidMount() {
        // this.TouchID()
    }

    render() {
        return (
            <ImageBackground source={require('../assets/image/Bg01.png')} style={{ flex: 1 }} >
                <View style={{ marginTop: hp('8%'), ...styles.container }}>
                    <Icon name="chevron-left" size={hp('4%')} color="#ED4F9D" onPress={() => Actions.push('Main')} />
                </View>
                <View style={{ marginTop: hp('8%'), ...styles.container, paddingHorizontal: hp('4%') }}>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>Enter</Text>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>Pin Code.</Text>
                </View>

                <View style={{ marginTop: hp('10%') }}>
                    <Pincode
                        ref="pincode"
                        onCancle={() => { Actions.push('Main') }}
                        pincodeReady={(pincode) => this.checkPincode(pincode)}></Pincode>
                </View>
                {this.renderWrongMessage()}
            </ImageBackground >
        )
    }
};

const styleScoped = StyleSheet.create({

})


