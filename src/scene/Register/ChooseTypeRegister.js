
import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


export default class LoginMain extends Component {
    render() {
        return (
            <ImageBackground source={require('../../assets/image/Bg01.png')} style={{ flex: 1 }} >
                <View style={{ marginTop: hp('8%'), ...styles.container }}>
                    <Icon name="chevron-left" size={hp('4%')} color="#ED4F9D" onPress={() => Actions.push('LoginMain')} />
                </View>
                <View style={{ marginTop: hp('8%'), ...styles.container, paddingHorizontal: hp('4%') }}>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>Welcome</Text>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>Back.</Text>
                </View>
                <View style={{ flexDirection: 'column', justifyContent: 'flex-end', flex: 1, }}>
                    <View style={{
                        ...styleScoped.shadowLogin,
                        backgroundColor: 'white',
                        ...styles.container,
                        paddingBottom: hp('8%'),
                        paddingTop: hp('5%'),
                        borderTopLeftRadius:hp('5%'),
                        borderTopRightRadius:hp('5%'),
                        marginBottom: hp('-2%')
                    }}>
                        <TouchableOpacity style={{ ...styleScoped.btnLoginStyle }} onPress={() => Actions.push('EmailRegister')}>
                            <View style={{ width: hp('5%'), height: hp('5%'), marginRight: hp('7.5%') }}>
                                <Image source={require('../../assets/image/Email.png')} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                            </View>
                            <Text style={{ fontSize: hp('2%'), color: '#12173C', textAlign: 'center' }}>Register with Email</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ ...styleScoped.btnLoginStyle, marginTop: hp('2.8%') }}>
                            <View style={{ width: hp('5%'), height: hp('5%'), marginRight: hp('7.5%') }}>
                                <Image source={require('../../assets/image/Google.png')} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                            </View>
                            <Text style={{ fontSize: hp('2%'), color: '#12173C', textAlign: 'center' }}>Register with Google</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ ...styleScoped.btnLoginStyle, marginTop: hp('2.8%'), backgroundColor: '#1E82EE', borderColor: '#1E82EE' }}>
                            <View style={{ width: hp('5%'), height: hp('5%'), marginRight: hp('7.5%') }}>
                                <Image source={require('../../assets/image/Facebook.png')} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                            </View>
                            <Text style={{ fontSize: hp('2%'), color: 'white', textAlign: 'center' }}>Register with Facebook</Text>
                        </TouchableOpacity>

                    </View>
                </View>

                <View style={{ ...styles.container, marginTop: hp('2%') }}>

                </View>
            </ImageBackground>
        )
    }
};


const styleScoped = StyleSheet.create({

    btnLoginStyle: {
        padding: hp('1%'),
        backgroundColor: 'white',
        borderRadius: hp('50%'),
        borderWidth: 0.5,
        borderColor: '#C6C6C6',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    btnRegisterStlye: {
        borderRadius: hp('50%'),
        padding: hp('1.8%'),
        fontSize: hp('2%'),
        marginTop: hp('0.5')
    },
    shadowLogin: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.5,
        shadowRadius: 30,

        elevation: 20,
    }
})


