
import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ImageBackground, Platform
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import theme from '../../constants'
import Picker from 'react-native-picker';

export default class LoginMain extends Component {
    state = {
        phone: null,
        country: 'Thailand'
    }
    onCountry() {
        Picker.init({
            pickerData: ["Thailand", "USA", "CANNADA"],
            pickerTitleText: 'Please select currency',
            selectedValue: [this.state.country],
            onPickerConfirm: data => { },
            onPickerCancel: data => { },
            onPickerSelect: data => { }
        });
        Picker.show();
    }
    render() {
        const { phone, country } = this.state
        let colorPhone = phone ? theme.colors.primary : theme.colors.secondary
        return (
            <ImageBackground source={require('../../assets/image/Bg01.png')} style={{ flex: 1 }} >
                <View style={{ marginTop: hp('8%'), ...styles.container }}>
                    <Icon name="chevron-left" size={hp('4%')} color="#ED4F9D" onPress={() => Actions.push('ChooseTypeLogin')} />
                </View>
                <View style={{ marginTop: hp('8%'), ...styles.container, paddingHorizontal: hp('4%') }}>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>Verify Phone</Text>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>Number.</Text>
                </View>
                <View style={{ flexDirection: 'column', justifyContent: 'flex-end', flex: 1, }}>
                    <View style={{
                        ...styleScoped.shadowLogin,
                        backgroundColor: 'white',
                        paddingHorizontal: hp('3%'),
                        paddingBottom: hp('18%'),
                        paddingTop: hp('5%'),
                        borderTopLeftRadius: hp('5%'),
                        borderTopRightRadius: hp('5%'),
                        marginBottom: hp('-2%')
                    }}>

                        <View style={{ ...styleScoped.boxEmail, marginBottom: hp('4%'), borderBottomColor: colorPhone }} >
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Icon name="phone" size={hp('2.5%')} color={colorPhone} style={{ marginRight: hp('2%') }} />
                                <TouchableOpacity style={{ width: wp('10%'), height: hp('3%') }} onPress={() => this.onCountry()}>
                                    <Image source={require('../../assets/image/thailand.png')} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                                </TouchableOpacity>
                                <Icon name="chevron-down" size={hp('3%')} color="#C6C6C6" style={{ marginRight: hp('2%') }} />
                                <TextInput
                                    style={{ fontSize: hp('2%'), padding: 0 }}
                                    color={colorPhone}
                                    value={phone}
                                    placeholder="Telephone Number"
                                    keyboardType="number-pad"
                                    onChangeText={(phone) => this.setState({ phone })}
                                ></TextInput>
                            </View>
                        </View>



                        <Button title="Request OTP" buttonStyle={{ ...styleScoped.btnLoginStyle }} onPress={() => Actions.push('VerifyOTP')} />

                    </View>

                </View>

                <View style={{ ...styles.container, marginTop: hp('2%') }}>

                </View>
            </ImageBackground >
        )
    }
};

const styleScoped = StyleSheet.create({
    boxEmail: {
        paddingVertical: Platform.OS === 'ios' ? hp('1.5%') : hp('1%'),
        borderBottomColor: '#ED4F9D',
        borderBottomWidth: hp('0.1%'),
        marginBottom: hp('2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boxInputPassword: {
        paddingVertical: hp('1.5%'),
        borderBottomColor: '#C6C6C6',
        borderBottomWidth: hp('0.1%'),
        marginBottom: hp('5%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    btnLoginStyleRegister: {
        padding: hp('1.8%'),
        // backgroundColor:'white',
        borderRadius: 50,
        borderWidth: 0.5,
        borderColor: '#C6C6C6'
    },
    btnLoginStyle: {
        borderRadius: 50,
        backgroundColor: '#ED4F9D',
        padding: hp('1.8%'),
        fontSize: hp('2%')
    },
    btnRegisterStlye: {
        borderRadius: hp('50%'),
        padding: hp('1.8%'),
        fontSize: hp('2%'),
        marginTop: hp('0.5')
    },
    shadowLogin: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.5,
        shadowRadius: 30,
        elevation: 20,
    }
})


