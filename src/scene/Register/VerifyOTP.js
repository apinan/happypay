
import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import theme from '../../constants'

export default class LoginMain extends Component {
    state = {
        otp: null
    }
    render() {
        const { otp } = this.state
        let colorOtp = otp ? theme.colors.primary : theme.colors.secondary
        return (
            <ImageBackground source={require('../../assets/image/Bg01.png')} style={{ flex: 1 }} >
                <View style={{ marginTop: hp('8%'), ...styles.container }}>
                    <Icon name="chevron-left" size={hp('4%')} color="#ED4F9D" onPress={() => Actions.push('ChooseTypeLogin')} />
                </View>
                <View style={{ marginTop: hp('8%'), ...styles.container, paddingHorizontal: hp('4%') }}>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>Verify</Text>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>OTP.</Text>
                </View>
                <View style={{ flexDirection: 'column', justifyContent: 'flex-end', flex: 1, }}>
                    <View style={{
                        ...styleScoped.shadowLogin,
                        backgroundColor: 'white',
                        paddingHorizontal: hp('3%'),
                        paddingBottom: hp('14%'),
                        paddingTop: hp('5%'),
                        borderTopLeftRadius: hp('5%'),
                        borderTopRightRadius: hp('5%'),
                        marginBottom: hp('-2%')
                    }}>

                        <View style={{ ...styleScoped.boxEmail, marginBottom: hp('4%'), borderBottomColor: colorOtp }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <TextInput
                                    style={{
                                        fontSize: hp('2%'),
                                        textAlign: 'center',
                                        width: '100%',
                                        fontSize: hp('3%'),
                                        padding: 0
                                    }}
                                    onChangeText={(otp) => this.setState({ otp })}
                                    keyboardType="number-pad"
                                    placeholder="X  X  X  X  X  X"
                                    maxLength={16}
                                    onChangeText={text => {
                                        let textOriginal = text.split(' ').join('')

                                        if (textOriginal.length > 0) {
                                            textOriginal = textOriginal.match(new RegExp('.{1,1}', 'g')).join('  ')
                                        }
                                        let { otp } = this.state
                                        otp = textOriginal
                                        this.setState({ otp })
                                    }}
                                    color={colorOtp}
                                    value={otp}></TextInput>
                            </View>
                        </View>

                        <Button title="Confirm OTP" buttonStyle={{ ...styleScoped.btnLoginStyle }} onPress={() => Actions.push('FinishRegister')} />

                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: hp('5%'), }}>
                            <Text style={{
                                fontSize: hp('1.8%'),
                                textDecorationLine: 'underline',
                                color: '#12173C',
                                marginRight: hp('4%'),
                                fontWeight: '300'
                            }}>Send OTP Again</Text>
                            <Text style={{ fontSize: hp('1.8%'), color: '#707070', fontWeight: '200' }}>01:00</Text>
                        </View>

                    </View>

                </View>

                <View style={{ ...styles.container, marginTop: hp('2%') }}>

                </View>
            </ImageBackground >
        )
    }
};

const styleScoped = StyleSheet.create({
    boxEmail: {
        paddingVertical: hp('1.5%'),
        borderBottomColor: '#ED4F9D',
        borderBottomWidth: hp('0.1%'),
        marginBottom: hp('2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boxInputPassword: {
        paddingVertical: hp('1.5%'),
        borderBottomColor: '#C6C6C6',
        borderBottomWidth: hp('0.1%'),
        marginBottom: hp('5%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    btnLoginStyleRegister: {
        padding: hp('1.8%'),
        // backgroundColor:'white',
        borderRadius: 50,
        borderWidth: 0.5,
        borderColor: '#C6C6C6'
    },
    btnLoginStyle: {
        borderRadius: 50,
        backgroundColor: '#ED4F9D',
        padding: hp('1.8%'),
        fontSize: hp('2%')
    },
    btnRegisterStlye: {
        borderRadius: hp('50%'),
        padding: hp('1.8%'),
        fontSize: hp('2%'),
        marginTop: hp('0.5')
    },
    shadowLogin: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.5,
        shadowRadius: 30,
        elevation: 20,
    }
})


