
import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import theme from '../../constants'

export default class AlreadyRegister extends Component {

    render() {

        return (
            <ImageBackground source={require('../../assets/image/Bg01.png')} style={{ flex: 1 }} >
                <View style={{ marginTop: hp('8%'), ...styles.container }}>
                    <Icon name="chevron-left" size={hp('4%')} color="#ED4F9D" onPress={() => Actions.push('ChooseTypeLogin')} />
                </View>
                <View style={{ marginTop: hp('25%'), ...styles.container, paddingHorizontal: hp('4%') }}>
                    <Text style={{ fontSize: hp('6%'), fontWeight: 'bold', color: theme.colors.text, textAlign: 'center' }}>Already</Text>
                    <Text style={{ marginTop:hp('-1%'), fontSize: hp('6%'), fontWeight: 'bold', color: theme.colors.text, textAlign: 'center' }}>registered.</Text>
                </View>
                <View style={{ paddingHorizontal:hp('4%') , marginTop:hp('4%') }}>
                    <Button title="Done" buttonStyle={{ ...styleScoped.btnLoginStyle }} onPress={() => Actions.push('RegisterEnterNewPin')} />
                </View>
            </ImageBackground >
        )
    }
};

const styleScoped = StyleSheet.create({
    boxEmail: {
        paddingVertical: hp('1.5%'),
        borderBottomColor: '#ED4F9D',
        borderBottomWidth: hp('0.1%'),
        marginBottom: hp('2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boxInputPassword: {
        paddingVertical: hp('1.5%'),
        borderBottomColor: '#C6C6C6',
        borderBottomWidth: hp('0.1%'),
        marginBottom: hp('5%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    btnLoginStyleRegister: {
        padding: hp('1.8%'),
        // backgroundColor:'white',
        borderRadius: 50,
        borderWidth: 0.5,
        borderColor: '#C6C6C6'
    },
    btnLoginStyle: {
        borderRadius: 50,
        backgroundColor: '#ED4F9D',
        padding: hp('1.8%'),
        fontSize: hp('2%')
    },
    btnRegisterStlye: {
        borderRadius: hp('50%'),
        padding: hp('1.8%'),
        fontSize: hp('2%'),
        marginTop: hp('0.5')
    },
    shadowLogin: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.5,
        shadowRadius: 30,
        elevation: 20,
    }
})


