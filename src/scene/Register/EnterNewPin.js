
import React, { Component } from 'react';
import {
    View, Text, StyleSheet, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Pincode from '../../components/Pincode'
import AsyncStorage from '@react-native-async-storage/async-storage';



export default class RegisterEnterNewPin extends Component {

    constructor(props) {
        super(props);
    }

    async checkPincode(value) {
        try {
            let pincode = value.join('')
            console.log('regis_pincode =>',pincode)
            await AsyncStorage.setItem('regis_pincode',pincode)
            Actions.push('RegisterConfirmPin')
        } catch (error) {
            console.log(error)
        }
    }

    render() {
        return (
            <ImageBackground source={require('../../assets/image/Bg01.png')} style={{ flex: 1 }} >
                <View style={{ marginTop: hp('8%'), ...styles.container }}>
                    <Icon name="chevron-left" size={hp('4%')} color="#ED4F9D" onPress={() => Actions.push('ChooseTypeLogin')} />
                </View>
                <View style={{ marginTop: hp('8%'), ...styles.container, paddingHorizontal: hp('4%') }}>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>Enter</Text>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>New Pin.</Text>
                </View>


                <View style={{ marginTop: hp('10%') }}>
                    <Pincode
                        onCancle={() => { Actions.push('ChooseTypeRegister') }}
                        pincodeReady={(pincode) => this.checkPincode(pincode)}></Pincode>
                </View>

            </ImageBackground >
        )
    }
};

const styleScoped = StyleSheet.create({

})


