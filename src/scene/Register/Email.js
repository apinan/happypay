
import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import theme from '../../constants'

export default class LoginMain extends Component {

    state = {
        email: null,
        password: null,
        confirmPassword: null,
        showPassword: false,
        showConfirmPassword: false
    }

    render() {
        const {
            email, password, confirmPassword, showConfirmPassword, showPassword
        } = this.state
        let colorEmailBox = email ? theme.colors.primary : theme.colors.secondary
        let colorPasswordBox = password ? theme.colors.primary : theme.colors.secondary
        let colorConfirmPasswordBox = confirmPassword ? theme.colors.primary : theme.colors.secondary
        return (
            <ImageBackground source={require('../../assets/image/Bg01.png')} style={{ flex: 1 }} >
                <View style={{ marginTop: hp('8%'), ...styles.container }}>
                    <Icon name="chevron-left" size={hp('4%')} color="#ED4F9D" onPress={() => Actions.push('ChooseTypeLogin')} />
                </View>
                <View style={{ marginTop: hp('8%'), ...styles.container, paddingHorizontal: hp('4%') }}>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>Register</Text>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>with Email.</Text>
                </View>
                <View style={{ flexDirection: 'column', justifyContent: 'flex-end', flex: 1, }}>
                    <View style={{
                        ...styleScoped.shadowLogin,
                        backgroundColor: 'white',
                        paddingHorizontal: hp('3%'),
                        paddingBottom: hp('8%'),
                        paddingTop: hp('5%'),
                        borderTopLeftRadius: hp('5%'),
                        borderTopRightRadius: hp('5%'),
                        marginBottom: hp('-2%')
                    }}>
                        {/* email input  */}
                        <View style={{ ...styleScoped.boxEmail, borderBottomColor: colorEmailBox }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Icon
                                    name="email"
                                    size={hp('2.5%')}
                                    color={colorEmailBox}
                                    style={{ marginRight: hp('3%') }} />
                                <TextInput
                                    value={email}
                                    style={{ fontSize: hp('2%'), padding: 0, width: '75%' }}
                                    color={colorEmailBox}
                                    placeholder="Email"
                                    onChangeText={(email) => this.setState({ email })}
                                ></TextInput>
                            </View>
                            <View>
                                <Icon name="check" size={hp('2.5%')} color={colorEmailBox} />
                            </View>
                        </View>
                        {/* end email input */}

                        {/* password input */}
                        <View style={{
                            ...styleScoped.boxInputPassword,
                            marginBottom: hp('2%'),
                            borderBottomColor: colorPasswordBox
                        }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Icon
                                    name="key-variant" size={hp('2.5%')}
                                    color={colorPasswordBox}
                                    style={{ marginRight: hp('3%') }} />
                                <TextInput
                                    value={password}
                                    style={{ fontSize: hp('2%'), padding: 0, width: '75%' }}
                                    color={colorPasswordBox}
                                    placeholder="password"
                                    secureTextEntry={!showPassword}
                                    onChangeText={(password) => this.setState({ password })}
                                ></TextInput>
                            </View>
                            <View>
                                <Icon
                                    name={showPassword ? "eye-off-outline" : "eye-outline"}
                                    size={hp('2.5%')}
                                    color={colorPasswordBox}
                                    onPress={() => this.setState({ showPassword: showPassword ? false : true })}
                                />
                            </View>
                        </View>
                        {/* end password input */}


                        {/* confirmPassword input  */}
                        <View style={{
                            ...styleScoped.boxInputPassword,
                            borderBottomColor: colorConfirmPasswordBox
                        }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                <Icon
                                    name="key-variant" size={hp('2.5%')}
                                    color={colorConfirmPasswordBox}
                                    style={{ marginRight: hp('3%') }} />
                                <TextInput
                                    value={confirmPassword}
                                    style={{ fontSize: hp('2%'), padding: 0, width: '75%' }}
                                    secureTextEntry={!showConfirmPassword}
                                    color={colorConfirmPasswordBox}
                                    placeholder="Confirm password"
                                    onChangeText={(confirmPassword) => this.setState({ confirmPassword })}
                                ></TextInput>
                            </View>
                            <View>
                                <Icon
                                    name={showConfirmPassword ? 'eye-off-outline' : "eye-outline"}
                                    size={hp('2.5%')}
                                    color={colorConfirmPasswordBox}
                                    onPress={() => this.setState({ showConfirmPassword: showConfirmPassword ? false : true })}
                                />
                            </View>
                        </View>


                        <Button title="Register" buttonStyle={{ ...styleScoped.btnLoginStyle }} onPress={() => Actions.push('VerifyPhone')} />
                        <View style={{ borderBottomColor: '#C6C6C6', borderBottomWidth: hp('0.1%'), marginVertical: hp('3%'), flexDirection: 'row', justifyContent: 'center' }}>
                            <View style={{ padding: hp('1%'), backgroundColor: 'white', marginTop: hp('-2.2%'), position: 'absolute' }}>
                                <Text style={{ textAlign: 'center', fontSize: hp('1.5%'), color: '#C6C6C6' }}>or</Text>
                            </View>
                        </View>
                        <TouchableOpacity style={{ ...styleScoped.btnLoginStyleRegister }} onPress={() => Actions.push('LoginMain')}>
                            <Text style={{ fontSize: hp('2%'), color: '#12173C', textAlign: 'center' }}>Sign in</Text>
                        </TouchableOpacity>
                    </View>

                </View>

                <View style={{ ...styles.container, marginTop: hp('2%') }}>

                </View>
            </ImageBackground >
        )
    }
};

const styleScoped = StyleSheet.create({
    boxEmail: {
        paddingVertical: hp('1.5%'),
        borderBottomColor: '#ED4F9D',
        borderBottomWidth: hp('0.1%'),
        marginBottom: hp('2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boxInputPassword: {
        paddingVertical: hp('1.5%'),
        borderBottomColor: '#C6C6C6',
        borderBottomWidth: hp('0.1%'),
        marginBottom: hp('5%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    btnLoginStyleRegister: {
        padding: hp('1.8%'),
        // backgroundColor:'white',
        borderRadius: 50,
        borderWidth: 0.5,
        borderColor: '#C6C6C6'
    },
    btnLoginStyle: {
        borderRadius: 50,
        backgroundColor: '#ED4F9D',
        padding: hp('1.8%'),
        fontSize: hp('2%')
    },
    btnRegisterStlye: {
        borderRadius: hp('50%'),
        padding: hp('1.8%'),
        fontSize: hp('2%'),
        marginTop: hp('0.5')
    },
    shadowLogin: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.5,
        shadowRadius: 30,
        elevation: 20,
    }
})


