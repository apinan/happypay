
import React, { Component } from 'react';
import {
    View, Text, StyleSheet, ImageBackground, Alert
} from 'react-native';
import { Button, Overlay } from 'react-native-elements';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Pincode from '../../components/Pincode'
import AsyncStorage from '@react-native-async-storage/async-storage';
import theme from '../../constants'
import { USER_ID } from '../../utils/env'
import { sha256 } from 'react-native-sha256';
export default class RegisterEnterNewPin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            regis_pincode: null,
            visible: false
        }
    }

    async getRegisPincode() {
        try {
            let pincode = await AsyncStorage.getItem('regis_pincode')
            this.setState({ regis_pincode: pincode })
        } catch (error) {
            console.log(error)
        }
    }

    async checkPincode(value) {
        try {
            const { regis_pincode } = this.state
            let pincode = value.join('')
            if (pincode == regis_pincode) {
                let public_key = `${USER_ID}-${Date.parse(new Date())}`
                await AsyncStorage.setItem('public_key', public_key)
                const dataEncryp = await sha256(`${pincode}${public_key}`)
                await AsyncStorage.setItem('user_pincode', dataEncryp)
                Actions.push('Main')
            } else {
                this.setState({ visible: true })
            }
        } catch (error) {
            console.log(error)
        }

    }

    componentDidMount() {
        this.getRegisPincode()
    }
    reset() {
        this.refs.pincode.resetPincode()
        this.setState({ visible: false })
    }
    renderWrongMessage() {
        const { visible } = this.state
        return (
            <>
                <Overlay isVisible={visible} overlayStyle={{ width: wp('50%') }}>
                    <Text style={{ fontSize: hp('2.8%'), textAlign: 'center' }}>Pincode is worng !</Text>
                    <View style={{ marginTop: hp('3%'), alignItems: 'center' }}>
                        <Button title="ตกลง" onPress={() => this.reset()} buttonStyle={{ backgroundColor: theme.colors.primary, width: wp('20%') }} />
                    </View>
                </Overlay>
            </>
        )
    }

    render() {
        return (
            <ImageBackground source={require('../../assets/image/Bg01.png')} style={{ flex: 1 }} >
                <View style={{ marginTop: hp('8%'), ...styles.container }}>
                    <Icon name="chevron-left" size={hp('4%')} color="#ED4F9D" onPress={() => Actions.push('ChooseTypeLogin')} />
                </View>
                <View style={{ marginTop: hp('8%'), ...styles.container, paddingHorizontal: hp('4%') }}>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>Confirm</Text>
                    <Text style={{ fontSize: hp('5%'), fontWeight: 'bold', color: '#12173C' }}>New Pin.</Text>
                </View>
                <View style={{ marginTop: hp('10%') }}>
                    <Pincode
                        ref='pincode'
                        onCancle={() => { Actions.push('ChooseTypeRegister') }}
                        pincodeReady={(pincode) => this.checkPincode(pincode)}></Pincode>
                </View>
                {this.renderWrongMessage()}
            </ImageBackground >
        )
    }
};

const styleScoped = StyleSheet.create({

})


