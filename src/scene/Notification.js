import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground
} from 'react-native';
import styles from '../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../components/Navbar'



export default class Transaction extends Component {


    renderTranfer() {
        return (
            <View style={{ ...styleScoped.boxListTransaction }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                    <View style={{ height: hp('4%'), width: hp('4%'), marginRight: hp('2%') }}>
                        <Image source={require('../assets/icon/getmoney.png')} style={{ ...styles.imageContain }} />
                    </View>
                    <View >
                        <Text style={{ color: '#12173C', fontSize: hp('1.8%'), marginBottom: hp('0.5%') }}>Transfer</Text>
                        <Text style={{ color: '#707070', fontSize: hp('1.5%') }}>09-10-2020 00:04 AM</Text>
                    </View>
                </View>
                <Text style={{ fontSize: hp('2%'), color: '#00A94F' }}> +10,000.00</Text>
            </View>
        )
    }

    renderPayBill() {
        return (
            <View style={{ ...styleScoped.boxListTransaction }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                    <View style={{ height: hp('4%'), width: hp('4%'), marginRight: hp('2%') }}>
                        <Image source={require('../assets/icon/bill.png')} style={{ ...styles.imageContain }} />
                    </View>
                    <View >
                        <Text style={{ color: '#12173C', fontSize: hp('1.8%'), marginBottom: hp('0.5%') }}>PayBills</Text>
                        <Text style={{ color: '#707070', fontSize: hp('1.5%') }}>09-10-2020 00:04 AM</Text>
                    </View>
                </View>
                <Text style={{ fontSize: hp('2%'), color: '#12173C' }}> -10,000.00</Text>
            </View>
        )
    }

    renderWithdraw() {
        return (
            <View style={{ ...styleScoped.boxListTransaction }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                    <View style={{ height: hp('4%'), width: hp('4%'), marginRight: hp('2%') }}>
                        <Image source={require('../assets/icon/withdraw.png')} style={{ ...styles.imageContain }} />
                    </View>
                    <View >
                        <Text style={{ color: '#12173C', fontSize: hp('1.8%'), marginBottom: hp('0.5%') }}>Withdraw</Text>
                        <Text style={{ color: '#707070', fontSize: hp('1.5%') }}>09-10-2020 00:04 AM</Text>
                    </View>
                </View>
                <Text style={{ fontSize: hp('2%'), color: '#12173C' }}> -10,000.00</Text>
            </View>
        )
    }

    renderTopup() {
        return (
            <View style={{ ...styleScoped.boxListTransaction, borderBottomWidth: 0 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                    <View style={{ height: hp('4%'), width: hp('4%'), marginRight: hp('2%') }}>
                        <Image source={require('../assets/icon/topup.png')} style={{ ...styles.imageContain }} />
                    </View>
                    <View >
                        <Text style={{ color: '#12173C', fontSize: hp('1.8%'), marginBottom: hp('0.5%') }}>Topup</Text>
                        <Text style={{ color: '#707070', fontSize: hp('1.5%') }}>09-10-2020 00:04 AM</Text>
                    </View>
                </View>
                <Text style={{ fontSize: hp('2%'), color: '#00A94F' }}> +10,000.00</Text>
            </View>
        )
    }

    render() {
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Notification" backTo="Main"></Navbar>
                <View style={{
                    flex: 1, 
                    position: 'relative',
                    zIndex: 20,
                    backgroundColor: '#F2F2F7',
                    paddingBottom: hp('3%'),
                    paddingTop: hp('3%')
                }}>
                    <View style={{ ...styles.container, backgroundColor: 'white', paddingVertical: hp('1%') }}>

                        {this.renderTranfer()}
                        <View style={{ ...styles.divider }}></View>
                        {this.renderTranfer()}
                        <View style={{ ...styles.divider }}></View>
                        {this.renderPayBill()}
                        <View style={{ ...styles.divider }}></View>
                        {this.renderWithdraw()}
                        <View style={{ ...styles.divider }}></View>
                        {this.renderTopup()}

                    </View>

                </View>
            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    boxInputSearch: {
        width: wp('72%'),
        padding: hp('1%'),
        backgroundColor: '#EEEEF0',
        borderRadius: hp('1%'),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    boxListTransaction: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: hp('2%')
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    }

})


