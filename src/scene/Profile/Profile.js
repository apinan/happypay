import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'
import AsyncStorage from '@react-native-async-storage/async-storage';


export default class Profile extends Component {
    constructor(props) {
        super(props)
        this.getImageUser()

    }

    state = {
        userImage: require('../../assets/image/Profile_ex.png'),
        userInfo: {
            name: 'พีรณัฐฐ์',
            lastName: 'ไพรศรี'
        }
    }

    async getImageUser() {
        let result = await AsyncStorage.getItem('googleLogin')
        if (result) {
            let userInfo = JSON.parse(result)
            this.setState({userInfo:{
                name:userInfo.givenName,
                lastName:userInfo.familyName
            }})
            this.setState({ userImage: { uri: userInfo.photo } })
        }
    }

    render() {
        const { userImage, userInfo } = this.state
        return (

            <View style={{ flex: 1 }}>
                <Navbar name="Profile Setting" backTo="Main"></Navbar>

                <ScrollView style={{ flex: 1, backgroundColor: '#F2F2F7' }}>

                    <View style={{ ...styles.container }}>

                        <View style={{ marginTop: hp('5%') }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <View style={{ height: hp('15%'), width: hp('15%'), backgroundColor: '#DDDBE1', borderRadius: hp('50%') }}>
                                    <Image source={userImage} style={{ ...styles.imageContain, borderRadius: 50 }} />
                                </View>
                            </View>
                            <TouchableOpacity onPress={() => Actions.push('EditNameMerchant')}>
                                <Text style={{ ...styleScoped.ProfileName }}>{userInfo.name} {userInfo.lastName}</Text>
                                <Text style={{ ...styleScoped.hi }}>Hi. Paipai</Text>
                            </TouchableOpacity>


                        </View>

                    </View>
                    <View style={{ ...styles.container, backgroundColor: 'white', marginTop: hp('2%') }}>
                        <TouchableOpacity style={{ ...styleScoped.boxMenuProfile }} onPress={() => Actions.push('ProfileDetail')}>
                            <Text style={{ fontSize: hp('2%'), color: '#12173C' }}> Profile  </Text>
                            <Icon name="chevron-right" size={hp('3.5%')} color="#12173C" />
                        </TouchableOpacity>
                        <View style={{ ...styles.divider }}></View>
                        <TouchableOpacity style={{ ...styleScoped.boxMenuProfile, borderBottomWidth: 0 }} onPress={() => Actions.push('NameTaxInvoice')}>
                            <Text style={{ fontSize: hp('2%'), color: '#12173C' }}> Name Tax Invoice</Text>
                            <Icon name="chevron-right" size={hp('3.5%')} color="#12173C" />
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    ProfileName: {
        fontSize: hp('3%'),
        marginTop: hp('2.5%'),
        textAlign: 'center'
    },
    hi: {
        fontSize: hp('2%'),
        marginTop: hp('1%'),
        textAlign: 'center',
        color: '#ED4F9D'
    },
    buttonLogout: {
        padding: hp('1.5%'),
        backgroundColor: '#F5F5F5',
        marginTop: hp('10%'),
        width: wp('50%'),
        borderRadius: 50
    },
    boxMenuProfile: {
        ...styles.cardCustom,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    }

})


