import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MenuBar from '../../components/MenuBar'
import Navbar from '../../components/Navbar'
export default class EditNameMerchant extends Component {
    render() {
        return (

            <SafeAreaView style={{ flex: 1 }}>
                <Navbar name="Edit Name Merchant" backTo="Profile"></Navbar>

                <ScrollView style={{ flex: 1 }}>

                    <View style={{ ...styles.container }} >

                        <View style={{ marginTop: hp('3%') }}>
                            <Text style={{ fontSize: hp('1.7%'), color: '#1E1F4D' }}>ชื่อ</Text>
                            <View style={{ padding: hp('2%'), backgroundColor: '#F5F5F5', marginTop: hp('1.5%'), borderRadius: 10 }}>
                                <TextInput value="Thana" style={{ color: '#1E1F4D', fontSize: hp('2%') }}></TextInput>
                            </View>
                            <Text style={{ fontSize: hp('1.7%'), marginTop: hp('2%'), color: '#1E1F4D' }}>นามสกุล</Text>
                            <View style={{ padding: hp('2%'), backgroundColor: '#F5F5F5', marginTop: hp('1.5%'), borderRadius: 10 }}>
                                <TextInput value="Wangdee" style={{ color: '#1E1F4D', fontSize: hp('2%') }}></TextInput>
                            </View>
                        </View >

                        <View style={{ marginTop: hp('5%'), flexDirection: 'row', justifyContent: 'center' }} >

                            <TouchableOpacity style={{ borderColor: '#E4345F', borderWidth:2, padding: hp('1%'), borderRadius: 50, paddingHorizontal: hp('5%') ,backgroundColor:'white' , marginRight:hp('2%') }} >
                                <Text style={{ color: 'black', fontSize: hp('2%'), textAlign: 'center' }}>ยกเลิก</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ backgroundColor: '#E4345F', padding: hp('1%'), borderRadius: 50, ...styleScoped.shadow, paddingHorizontal: hp('5%') }}>
                                <Text style={{ color: 'white', fontSize: hp('2%'), textAlign: 'center' }}>บันทึก</Text>
                            </TouchableOpacity>

                        </View>

                    </View>

                </ScrollView>

                <MenuBar ></MenuBar>

            </SafeAreaView>

        )
    }
};


const styleScoped = StyleSheet.create({
    linearGradient: {
        height: hp('75%'),
        borderBottomRightRadius: hp('13%'),
    },
    boxInfoMerchant: {
        paddingVertical: hp('2%'),
        paddingHorizontal: hp('1%'),
        borderBottomWidth: 2,
        borderBottomColor: '#F3F3F3',
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,
    }

})


