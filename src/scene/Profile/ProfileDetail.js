import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'
export default class ProfileDetail extends Component {
    render() {
        return (

            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <Navbar name="Profile" backTo="Profile"></Navbar>

                <ScrollView style={{ flex: 1 , backgroundColor: '#F2F2F7' }}>

                    <View style={{ ...styles.container, backgroundColor: 'white', marginTop: hp('4%') }} >

                        <View style={{ ...styles.cardCustom, }}>
                            <View style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ ...styleScoped.topic }}> Name </Text>
                                <Text style={{ ...styleScoped.detail }}>  Peeranut </Text>
                            </View>
                            <View style={{ ...styles.divider }}></View>

                            <View style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ ...styleScoped.topic }}> Surname </Text>
                                <Text style={{ ...styleScoped.detail }}>  Prisri </Text>
                            </View>
                            <View style={{ ...styles.divider }}></View>

                            <View style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ ...styleScoped.topic }}> Citizen Id </Text>
                                <Text style={{ ...styleScoped.detail }}>  1100900023410 </Text>
                            </View>
                            <View style={{ ...styles.divider }}></View>

                            <View style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ ...styleScoped.topic }}> Person Type </Text>
                                <Text style={{ ...styleScoped.detail }}>  Individual </Text>
                            </View>
                            <View style={{ ...styles.divider }}></View>

                            <View style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ ...styleScoped.topic }}> Email </Text>
                                <View style={{ ...styleScoped.boxEditDetail }}>
                                    <Text style={{ ...styleScoped.detail }}>  long.pikku@gmailc.om </Text>
                                    <Icon name="chevron-right" size={hp('3%')} color="#12173C"  onPress={()=>Actions.push('UpdateEmail')}/>
                                </View>
                            </View>
                            <View style={{ ...styles.divider }}></View>

                            <View style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ ...styleScoped.topic }}> Tel </Text>
                                <View style={{ ...styleScoped.boxEditDetail }}>
                                    <Text style={{ ...styleScoped.detail }}> +6681 268 2628 </Text>
                                    <Icon name="chevron-right" size={hp('3%')} color="#12173C"  onPress={()=>Actions.push('UpdateTelephone')}/>
                                </View>
                            </View>
                            <View style={{ ...styles.divider }}></View>

                            <View style={{ ...styleScoped.boxInfoMerchant, alignItems: 'flex-start' }}>
                                <Text style={{ ...styleScoped.topic }}> Address </Text>
                                <View style={{ ...styleScoped.boxEditDetail }}>
                                    <Text style={{ ...styleScoped.detail, width: wp('48%'), textAlign: 'right', marginRight: hp('0.8%') }}>  32/172 Soi Lamparo 32 Chatuchak Chan KasemBangkok 10310 </Text>
                                    <Icon name="chevron-right" size={hp('3%')} color="#12173C"  onPress={()=>Actions.push('UpdateAddress')}/>
                                </View>
                            </View>
                            <View style={{ ...styles.divider }}></View>

                            <View style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ ...styleScoped.topic }}> Website </Text>
                                <View style={{ ...styleScoped.boxEditDetail }}>
                                    <Text style={{ ...styleScoped.detail }}>  beaverech.net </Text>
                                    <Icon name="chevron-right" size={hp('3%')} color="#12173C" onPress={()=>Actions.push('UpdateWebsite')}/>
                                </View>
                            </View>
                            <View style={{ ...styles.divider }}></View>

                            <View style={{ ...styleScoped.boxInfoMerchant, borderBottomWidth: 0 }}>
                                <Text style={{ ...styleScoped.topic }}> Facebook </Text>
                                <Text style={{ ...styleScoped.detail }}>  beaver tech </Text>
                            </View>
                        </View>

                    </View>

                </ScrollView>
            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    topic: {
        fontSize: hp('2%'),
        color: '#12173C',
        fontWeight: '300'
    },
    detail: {
        fontSize: hp('2%'),
        color: '#ED4F9D',
        fontWeight: '300'
    },
    boxEditDetail: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    boxInfoMerchant: {
        paddingVertical: hp('2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,
    }

})


