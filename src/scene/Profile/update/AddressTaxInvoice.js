import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView
} from 'react-native';
import styles from '../../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../../components/Navbar'
import { Button } from 'react-native-elements';



export default class updateAddressTaxInvoice  extends Component {
    render() {
        return (

            <View style={{ flex: 1 }}>
                <Navbar name="Address for Tax Invoice" backTo="ProfileDetail"></Navbar>

                <ScrollView style={{ ...styles.container, marginTop: hp('3%') }} >

                    <View>
                        <Text style={{ color: 'rgba(60,60,67,0.6)', fontWeight: '300', fontSize: hp('2%') }}>Address</Text>
                        <TextInput value="+66 081 555 6655" style={{
                            fontSize: hp('2%'),
                            color: '#12173C',
                            marginVertical: Platform.OS === 'ios' ?  hp('2%') : hp('0.3%'),
                            width: '100%'
                        }} value="32/172 Soi Lamparo 32"></TextInput>
                        <View style={{ ...styles.divider, marginBottom: hp('3%') }}></View>
                    </View>

                    <View>
                        <Text style={{ color: 'rgba(60,60,67,0.6)', fontWeight: '300', fontSize: hp('2%') }}>Amphire</Text>
                        <TouchableOpacity style={{ ...styleScoped.boxDropDown }}>
                            <Text style={{ fontSize: hp('2%'), color: '#12173C', }}>Chatuchak</Text>
                            <Icon name="chevron-down" size={hp('3%')} color="#12173C" />
                        </TouchableOpacity>
                        <View style={{ ...styles.divider, marginBottom: hp('3%') }}></View>
                    </View>

                    <View>
                        <Text style={{ color: 'rgba(60,60,67,0.6)', fontWeight: '300', fontSize: hp('2%') }}>District</Text>
                        <TouchableOpacity style={{ ...styleScoped.boxDropDown }}>
                            <Text style={{ fontSize: hp('2%'), color: '#12173C', }}>Chan Kasem</Text>
                            <Icon name="chevron-down" size={hp('3%')} color="#12173C" />
                        </TouchableOpacity>
                        <View style={{ ...styles.divider, marginBottom: hp('3%') }}></View>
                    </View>

                    <View>
                        <Text style={{ color: 'rgba(60,60,67,0.6)', fontWeight: '300', fontSize: hp('2%') }}>Province</Text>
                        <TouchableOpacity style={{ ...styleScoped.boxDropDown }}>
                            <Text style={{ fontSize: hp('2%'), color: '#12173C', }}>Bangkok</Text>
                            <Icon name="chevron-down" size={hp('3%')} color="#12173C" />
                        </TouchableOpacity>
                        <View style={{ ...styles.divider, marginBottom: hp('2%') }}></View>
                    </View>

                    <View>
                        <Text style={{ color: 'rgba(60,60,67,0.6)', fontWeight: '300', fontSize: hp('2%') }}>Zip Code</Text>
                        <TextInput value="10310" style={{
                            fontSize: hp('2%'),
                            color: '#12173C',
                            marginVertical: Platform.OS === 'ios' ?  hp('2%') : hp('0.3%'),
                            width: '100%'
                        }}></TextInput>
                        <View style={{ ...styles.divider, marginBottom: hp('3%') }}></View>
                    </View>


                    <View>
                        <Text style={{ color: 'rgba(60,60,67,0.6)', fontWeight: '300', fontSize: hp('2%') }}>Tax Id</Text>
                        <TextInput value="1100220102313" style={{
                            fontSize: hp('2%'),
                            color: '#12173C',
                            marginVertical: Platform.OS === 'ios' ?  hp('2%') : hp('0.3%'),
                            width: '100%'
                        }}></TextInput>
                        <View style={{ ...styles.divider, marginBottom: hp('3%') }}></View>
                    </View>



                    <Button title="Confirm" buttonStyle={{ ...styles.btnPrimary }} onPress={() => Actions.push('ProfileDetail')} />
                </ScrollView>



            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    boxDropDown: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: hp('2%')
    }

})


