import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView
} from 'react-native';
import styles from '../../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../../components/Navbar'
import { Button } from 'react-native-elements';



export default class updateWebsite extends Component {
    render() {
        return (

            <View style={{ flex: 1 }}>
                <Navbar name="Website" backTo="ProfileDetail"></Navbar>

                <View style={{ ...styles.container, marginTop: hp('3%') }} >
                    <TextInput value="beaverech.net" style={{
                        fontSize: hp('2%'),
                        color: '#12173C',
                        marginVertical:Platform.OS === 'ios' ?  hp('2%') : hp('0.3%'),
                        width:'100%'
                    }}></TextInput>
                    <View style={{ ...styles.divider  , marginBottom:hp('2%')}}></View>
                    <Button title="Confirm" buttonStyle={{ ...styles.btnPrimary }} onPress={() => Actions.push('ProfileDetail')} />
                </View>



            </View>

        )
    }
};


const styleScoped = StyleSheet.create({


})


