import React, { Component } from 'react';
import {
    View, Text, StyleSheet, TextInput, TouchableOpacity, KeyboardAvoidingView, ScrollView
} from 'react-native';
import styles from '../../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../../components/Navbar'
import { Button } from 'react-native-elements';
import Picker from 'react-native-picker';



export default class updateAddress extends Component {

    state = {
        address: '32/172 Soi Lamparo 32',
        zipcode: '10310'
    }

    onAmphire() {
        Picker.init({
            pickerData: ["Amphire 1", "Amphire 2", "Amphire 3"],
            pickerTitleText: 'Please select currency',
            onPickerConfirm: data => { },
            onPickerCancel: data => { },
            onPickerSelect: data => { }
        });
        Picker.show();
    }

    onDistrict() {
        Picker.init({
            pickerData: ["District 1", "District 2", "District 3"],
            pickerTitleText: 'Please select currency',
            onPickerConfirm: data => { },
            onPickerCancel: data => { },
            onPickerSelect: data => { }
        });
        Picker.show();
    }

    onProvince() {
        Picker.init({
            pickerData: ["Province 1", "Province 2", "Province 3"],
            pickerTitleText: 'Please select currency',
            onPickerConfirm: data => { },
            onPickerCancel: data => { },
            onPickerSelect: data => { }
        });
        Picker.show();
    }

    render() {
        const { address, zipcode } = this.state
        return (
            <KeyboardAvoidingView style={{ flex: 1 }} >

                <ScrollView style={{ flex: 1 }}>
                    <Navbar name="Address" backTo="ProfileDetail"></Navbar>
                    <View style={{ ...styles.container, marginTop: hp('5%') }} >

                        <View>
                            <Text style={{ color: 'rgba(60,60,67,0.6)', fontWeight: '300', fontSize: hp('2%') }}>Address</Text>
                            <TextInput
                                onChangeText={(address) => this.setState({ address })}
                                style={{
                                    fontSize: hp('2%'),
                                    color: '#12173C',
                                    marginVertical: Platform.OS === 'ios' ? hp('2%') : hp('0.3%'),
                                    width: '100%'
                                }} value={address}></TextInput>
                            <View style={{ ...styles.divider, marginBottom: hp('3%') }}></View>
                        </View>

                        <View>
                            <Text style={{ color: 'rgba(60,60,67,0.6)', fontWeight: '300', fontSize: hp('2%') }}>Amphire</Text>
                            <TouchableOpacity style={{ ...styleScoped.boxDropDown }} onPress={() => this.onAmphire()}>
                                <Text style={{ fontSize: hp('2%'), color: '#12173C', }}>Chatuchak</Text>
                                <Icon name="chevron-down" size={hp('3%')} color="#12173C" />
                            </TouchableOpacity>
                            <View style={{ ...styles.divider, marginBottom: hp('3%') }}></View>
                        </View>

                        <View>
                            <Text style={{ color: 'rgba(60,60,67,0.6)', fontWeight: '300', fontSize: hp('2%') }}>District</Text>
                            <TouchableOpacity style={{ ...styleScoped.boxDropDown }} onPress={() => this.onDistrict()}>
                                <Text style={{ fontSize: hp('2%'), color: '#12173C', }}>Chan Kasem</Text>
                                <Icon name="chevron-down" size={hp('3%')} color="#12173C" />
                            </TouchableOpacity>
                            <View style={{ ...styles.divider, marginBottom: hp('3%') }}></View>
                        </View>

                        <View>
                            <Text style={{ color: 'rgba(60,60,67,0.6)', fontWeight: '300', fontSize: hp('2%') }}>Province</Text>
                            <TouchableOpacity style={{ ...styleScoped.boxDropDown }} onPress={() => this.onProvince()}>
                                <Text style={{ fontSize: hp('2%'), color: '#12173C', }}>Bangkok</Text>
                                <Icon name="chevron-down" size={hp('3%')} color="#12173C" />
                            </TouchableOpacity>
                            <View style={{ ...styles.divider, marginBottom: hp('2%') }}></View>
                        </View>

                        <View>
                            <Text style={{ color: 'rgba(60,60,67,0.6)', fontWeight: '300', fontSize: hp('2%') }}>Zip Code</Text>
                            <TextInput
                                value={zipcode}
                                onChangeText={(zipcode) => this.setState({ zipcode })}
                                style={{
                                    fontSize: hp('2%'),
                                    color: '#12173C',
                                    marginVertical: Platform.OS === 'ios' ? hp('2%') : hp('0.3%'),
                                    width: '100%'
                                }}
                            ></TextInput>
                            <View style={{ ...styles.divider, marginBottom: hp('5%') }}></View>
                        </View>



                        <Button title="Confirm" buttonStyle={{ ...styles.btnPrimary }} onPress={() => Actions.push('ProfileDetail')} />
                    </View>

                </ScrollView>
            </KeyboardAvoidingView>


        )
    }
};


const styleScoped = StyleSheet.create({
    boxDropDown: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: hp('2%')
    }

})


