import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, Platform
} from 'react-native';
import styles from '../../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../../components/Navbar'
import { Button } from 'react-native-elements';
import theme from '../../../constants'


export default class updateEmail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            valueEmail: 'long.pikku@gmail.com'
        }
    }

    render() {
        const { valueEmail } = this.state
        let colorEmail = valueEmail ? theme.colors.primary : theme.colors.secondary
        return (

            <View style={{ flex: 1 }}>
                <Navbar name="Email" backTo="ProfileDetail"></Navbar>
                <View style={{ ...styles.container, marginTop: hp('4%') }} >
                    <TextInput
                        value={valueEmail}
                        onChangeText={(text) => this.setState({ valueEmail: text })}
                        style={{
                            fontSize: hp('2%'),
                            color: '#12173C',
                            marginVertical: Platform.OS === 'ios' ? hp('2%') : hp('0.3%'),
                            width: '100%',
                            padding: 0
                        }}></TextInput>
                    <View style={{ ...styles.divider, marginBottom: hp('2%'), borderBottomColor: colorEmail }}></View>
                    <Button title="Confirm" buttonStyle={{ ...styles.btnPrimary }} onPress={() => Actions.push('ProfileDetail')} />
                </View>
            </View>

        )
    }
};


const styleScoped = StyleSheet.create({


})


