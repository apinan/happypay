import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MenuBar from '../../components/MenuBar'
import Navbar from '../../components/Navbar'
export default class UsageInfomation extends Component {
    render() {
        return (

            <SafeAreaView style={{ flex: 1 }}>
                <Navbar name="UsageInfomation" backTo="Profile"></Navbar>

                <ScrollView style={{ flex: 1 }}>

                    <View style={{ ...styles.container }} >

                        <View style={{ ...styles.cardCustom, ...styles.shadowDefault, marginTop: hp('4%') }}>
                            <View style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ fontSize: hp('2%') }}> Sale Type </Text>
                                <Text style={{ fontSize: hp('2%'), color: '#F679A0' }}>  Installment sale </Text>
                            </View>

                            <View style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ fontSize: hp('2%') }}> Sale Type </Text>
                                <Text style={{ fontSize: hp('2%'), color: '#F679A0' }}>  Socail Shop </Text>
                            </View>

                            <View style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ fontSize: hp('2%') }}> Bill Payment </Text>
                                <Text style={{ fontSize: hp('2%'), color: '#F679A0' }}>  Yes </Text>
                            </View>

                            <View style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ fontSize: hp('2%') }}> QR Credit </Text>
                                <Text style={{ fontSize: hp('2%'), color: '#F679A0' }}>  No </Text>
                            </View>

                            <View style={{ ...styleScoped.boxInfoMerchant , borderBottomWidth:0 }}>
                                <Text style={{ fontSize: hp('2%') }}> WeChat Pay  </Text>
                                <Text style={{ fontSize: hp('2%'), color: '#F679A0' }}>  Yes </Text>
                            </View>

                        </View>

                    </View>

                </ScrollView>

                <MenuBar ></MenuBar>

            </SafeAreaView>

        )
    }
};


const styleScoped = StyleSheet.create({
    linearGradient: {
        height: hp('75%'),
        borderBottomRightRadius: hp('13%'),
    },
    boxInfoMerchant: {
        paddingVertical: hp('2%'),
        borderBottomWidth: 2,
        borderBottomColor: '#F3F3F3',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,
    }

})


