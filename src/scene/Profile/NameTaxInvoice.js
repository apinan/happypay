import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MenuBar from '../../components/MenuBar'
import Navbar from '../../components/Navbar'
export default class NameTaxInvoice extends Component {
    render() {
        return (

            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <Navbar name="Name Tax Invoice" backTo="Profile"></Navbar>

                <ScrollView style={{ flex: 1  , backgroundColor:"#F2F2F7"}}>

                    <View style={{ ...styles.container, backgroundColor: 'white', marginTop: hp('3%') }} >

                        <View style={{ ...styles.cardCustom }}>
                            <View style={{ ...styleScoped.boxInfoMerchant, }}>
                                <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}> Name for Tax Invoice </Text>
                                <View style={{ ...styleScoped.boxEditDetail }}>
                                    <Text style={{ fontSize: hp('2%'), color: '#F679A0',marginRight: hp('1%') }}>Thana Wangdee </Text>
                                    <Icon name="chevron-right" size={hp('3%')} color="#12173C" onPress={() => Actions.push('UpdateNameTaxInvoice')} />
                                </View>
                            </View>
                            <View style={{ ...styles.divider }}></View>
                            <View style={{ ...styleScoped.boxInfoMerchant , alignItems:'flex-start' }}>
                                <View>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Address</Text>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>for Tax Invoice</Text>
                                </View>
                                <View style={{ ...styleScoped.boxEditDetail  }}>
                                    <Text style={{ fontSize: hp('2%'), color: '#F679A0', width: wp('40%'), textAlign: 'left', marginRight: hp('1%') }}>
                                        32/172 Soi Lamparo 32 Chatuchak Chan Kasem Bangkok 10310
                                    </Text>
                                    <Icon name="chevron-right" size={hp('3%')} color="#12173C" onPress={() => Actions.push('UpdateAddressTaxInvoice')} />
                                </View>

                            </View>

                        </View>

                    </View>

                </ScrollView>
            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    linearGradient: {
        height: hp('75%'),
        borderBottomRightRadius: hp('13%'),
    },
    boxInfoMerchant: {
        paddingVertical: hp('2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boxEditDetail: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,
    }

})


