import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'
import { Button } from 'react-native-elements';
import theme from '../../constants'



export default class ChangePassword extends Component {

    state = {
        oldPassword: null,
        newPassword: null,
        confirmPassword: null,
        showOld: false,
        showNew: false,
        showConfirm: false
    }

    render() {
        const { oldPassword, newPassword, confirmPassword, showOld, showNew, showConfirm } = this.state

        let colorOld = oldPassword ? theme.colors.primary : theme.colors.secondary
        let colorNew = newPassword ? theme.colors.primary : theme.colors.secondary
        let colorConfirm = confirmPassword ? theme.colors.primary : theme.colors.secondary

        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Chnage Password" backTo="Setting" ></Navbar>

                <View style={{
                    flex: 1, position: 'relative',
                    zIndex: 20,
                    paddingBottom: hp('3%'),
                    paddingTop: hp('3%')
                }}>

                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                        ...styles.container,
                        backgroundColor: 'white',
                    }}>
                        <View>
                            <View style={{ ...styleScoped.boxInputPassword, marginBottom: hp('2%'), borderBottomColor: colorOld }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                    <TextInput
                                        style={{ fontSize: hp('2%'), padding: 0 }}
                                        color={colorOld}
                                        placeholder="Enter Old Password"
                                        secureTextEntry={!showOld}
                                        value={oldPassword}
                                        onChangeText={(oldPassword) => this.setState({ oldPassword })}
                                    ></TextInput>
                                </View>
                                <View>
                                    <Icon
                                        name={showOld ? 'eye-off-outline' : "eye-outline"}
                                        size={hp('2.5%')}
                                        color={colorOld}
                                        onPress={() => this.setState({ showOld: showOld ? false : true })}
                                    />
                                </View>
                            </View>

                            <View style={{ ...styleScoped.boxInputPassword, marginBottom: hp('2%'), borderBottomColor: colorNew }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                    <TextInput
                                        style={{ fontSize: hp('2%'), padding: 0 }}
                                        color={colorNew}
                                        placeholder="Enter New Password"
                                        secureTextEntry={!showNew}
                                        value={newPassword}
                                        onChangeText={(newPassword) => this.setState({ newPassword })}
                                    ></TextInput>
                                </View>
                                <View>
                                    <Icon
                                        name={showNew ? 'eye-off-outline' : "eye-outline"}
                                        size={hp('2.5%')}
                                        color={colorNew}
                                        onPress={() => this.setState({ showNew: showNew ? false : true })}
                                    />
                                </View>
                            </View>

                            <View style={{ ...styleScoped.boxInputPassword, marginBottom: hp('2%'), borderBottomColor: colorConfirm }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                    <TextInput
                                        style={{ fontSize: hp('2%'), padding: 0 }}
                                        color={colorConfirm}
                                        placeholder="Enter Confirm Password"
                                        secureTextEntry={!showConfirm}
                                        value={confirmPassword}
                                        onChangeText={(confirmPassword) => this.setState({ confirmPassword })}
                                    ></TextInput>
                                </View>
                                <View>
                                    <Icon
                                        name={showConfirm ? 'eye-off-outline' : "eye-outline"}
                                        size={hp('2.5%')}
                                        color={colorConfirm}
                                        onPress={() => this.setState({ showConfirm: showConfirm ? false : true })}
                                    />
                                </View>
                            </View>



                        </View>
                        <Button title="Confirm Change Password" buttonStyle={{ ...styles.btnPrimary }} onPress={() => Actions.push('Setting')} />
                    </View>

                </View>
            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    btnPrimary: {
        borderRadius: 50,
        backgroundColor: '#ED4F9D',
        padding: hp('1.8%'),
        fontSize: hp('2%')
    },
    boxInputPassword: {
        paddingVertical: hp('1.5%'),
        borderBottomColor: '#C6C6C6',
        borderBottomWidth: hp('0.1%'),
        marginBottom: hp('5%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
})


