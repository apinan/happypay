import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'
import theme from '../../constants'





export default class ChangeLanguage extends Component {

    state = {
        Language: 'English',
        LanguageList: ['English', 'ไทย']
    }

    render() {
        const { Language, LanguageList } = this.state
        let primary = theme.colors.primary
        let secondary = theme.colors.secondary
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Chnage Language" backTo="Setting" ></Navbar>

                <View style={{
                    flex: 1, position: 'relative',
                    zIndex: 20,
                    paddingBottom: hp('3%'),
                    paddingTop: hp('3%')
                }}>
                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        justifyContent: 'space-between',
                        ...styles.container,
                        backgroundColor: 'white',
                    }}>

                        <View style={{ ...styles.container }}>
                            {LanguageList.map((item, index) => {
                                let colorSelected = item == Language ? primary : secondary
                                return (
                                    <>
                                        <TouchableOpacity
                                            style={{
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                alignItems: 'center',
                                                marginVertical: hp('2%')
                                            }}
                                            key={`lang__${index}`}
                                            onPress={() => this.setState({ Language: item })}
                                        >
                                            <Text style={{ fontSize: hp('2.5%'), color: colorSelected }} >{item}</Text>
                                            {item == Language ? <Icon name="check" size={hp('3%')} color={colorSelected} /> : null}
                                        </TouchableOpacity>
                                        {(index + 1) != LanguageList.length ? <View style={{ ...styles.divider }}></View> : null}
                                    </>
                                )
                            })}
                         
                        </View>

                    </View>
                </View>

            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    btnPrimary: {
        borderRadius: 50,
        backgroundColor: '#ED4F9D',
        padding: hp('1.8%'),
        fontSize: hp('2%')
    },
    boxInputPassword: {
        paddingVertical: hp('1.5%'),
        borderBottomColor: '#C6C6C6',
        borderBottomWidth: hp('0.1%'),
        marginBottom: hp('5%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
})


