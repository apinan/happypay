import React, { Component } from 'react';
import {
    View, Text, Image, StyleSheet, TouchableOpacity, ImageBackground, Platform, StatusBar, Animated, ScrollView
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import theme from '../../constants'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Carousel from '../../components/Carousel'
import { Scene, Stack, Router } from 'react-native-router-flux';

export default class MainScene extends Component {

    constructor(props) {
        super(props)
        this.getImageUser()
        this.RefreshRotateValue = new Animated.Value(0)
    }

    state = {
        userImage: require('../../assets/image/Profile_ex.png')
    }

    async getImageUser() {
        let result = await AsyncStorage.getItem('googleLogin')
        if (result) {
            let userInfo = JSON.parse(result)
            this.setState({ userImage: { uri: userInfo.photo } })
        }
    }

    async getUserPincode() {
        try {
            let user_pincode = await AsyncStorage.getItem('user_pincode')
            if (!user_pincode) {
                Actions.push('RegisterEnterNewPin')
            }
        } catch (error) {

        }
    }

    componentDidMount() {
        this.getUserPincode()
    }

    render() {
        const { userImage } = this.state
        return (
            <View style={{ ...styleScoped.sectionMain }}>
                <View style={{}}>
                    {/* header */}
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', ...styles.container, alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => Actions.push('Notification')}>
                            <Icon name="bell-outline" size={hp('4%')} color="#12173C" />
                            <View style={{ ...styleScoped.noti }}></View>
                        </TouchableOpacity>
                        <View style={{ ...styleScoped.imageLogo }}>
                            <Image source={require('../../assets/image/Logo.png')} style={{ ...styles.imageContain }} />
                        </View>
                        <TouchableOpacity
                            style={{ ...styleScoped.imageProfile }}
                            onPress={() => Actions.push('Profile')}
                        >
                            <Image source={userImage} style={{ ...styles.imageContain, borderRadius: 50 }} />
                        </TouchableOpacity>
                    </View>
                    {/* end header  */}

                    {/* box wallet */}
                    <View style={{ ...styles.container, marginTop: hp('3%'), height: hp('20%'), }}>
                        <ImageBackground source={require('../../assets/image/BgHorizontal.png')}
                            style={{ ...styleScoped.boxImageBackgroundWallet }}
                            imageStyle={{ borderRadius: 20 }}
                        >
                            <View style={{ ...styleScoped.boxTextNameWallet }}>
                                <Text style={{ ...styleScoped.textNameWallet }}>HI. PAIPAI</Text>
                                <TouchableOpacity onPress={() => Actions.push('MyWallet')}>
                                    <Icon name="chevron-right" size={hp('4%')} color={theme.colors.text} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ ...styleScoped.boxTextAmount }}>
                                <Text style={{ ...styleScoped.textAmount }}>฿35,000</Text>
                            </View>
                            <View style={{ ...styleScoped.boxTimeWallet }}>
                                <TouchableOpacity>
                                    <Icon name="refresh" size={hp('3%')} color="#12173C" style={{ marginRight: hp('1%'), fontWeight: '200' }} />
                                </TouchableOpacity>
                                <Text style={{ ...styleScoped.textTimeWallet }}>Balance at 8:59 PM</Text>
                            </View>
                        </ImageBackground>
                    </View>
                    {/* end box wallet */}


                    <View style={{ marginTop: hp('4%'), paddingHorizontal: hp('5%') }}>
                        <View style={{ ...styleScoped.boxMenu }}>
                            <TouchableOpacity style={{ ...styleScoped.boxMenuItem }} onPress={() => Actions.push('ScanQrcode')}>
                                <View style={{ ...styleScoped.boxImageMenu }} >
                                    <Image source={require('../../assets/icon/qrcode.png')} style={{ ...styles.imageContain }} />
                                </View>
                                <Text style={{ ...styleScoped.textMenu }}>SCAN QR</Text>
                            </TouchableOpacity>
                            <View style={{ ...styles.dividerVertical }}></View>
                            <TouchableOpacity style={{ ...styleScoped.boxMenuItem }} onPress={() => Actions.push('ChargePincode')}>
                                <View style={{ ...styleScoped.boxImageMenu }}>
                                    <Image source={require('../../assets/icon/getmoney.png')} style={{ ...styles.imageContain }} />
                                </View>
                                <Text style={{ ...styleScoped.textMenu }}>เรียกเก็บเงิน</Text>
                            </TouchableOpacity>
                            <View style={{ ...styles.dividerVertical }}></View>
                            <TouchableOpacity style={{ ...styleScoped.boxMenuItem }} onPress={() => Actions.push('ListBill')}>
                                <View style={{ ...styleScoped.boxImageMenu }}>
                                    <Image source={require('../../assets/icon/bill.png')} style={{ ...styles.imageContain }} />
                                </View>
                                <Text style={{ ...styleScoped.textMenu }}>ชำระบิล</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{ marginTop: hp('2%'), ...styles.container }}>
                        <View style={{ ...styleScoped.boxTitleFavorite }}>
                            <Text style={{ ...styleScoped.textTitleFavorite }}  >Favorite</Text>
                            <Icon name="chevron-right" size={hp('3%')} color="#021038" onPress={() => Actions.push('Favorite')} />
                        </View>
                    </View>

                    <View style={{ marginTop: hp('2%'), ...styles.container }} >
                        <ScrollView horizontal={true}>

                            <TouchableOpacity style={{ ...styleScoped.boxItemFavorite }} onPress={() => Actions.push('ChargePincode')}>
                                <View style={{ ...styleScoped.boxImageFavorite }} >
                                    <View style={{ width: hp('4%'), wp: hp('4%') }}>
                                        <Image source={require('../../assets/icon/getmoneywhite.png')} style={{ ...styles.imageContain }} />
                                    </View>
                                </View>
                                <View style={{ marginTop: hp('1%') }}>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Plyphichcha</Text>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Pipopvorac…</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ ...styleScoped.boxItemFavorite }} onPress={() => Actions.push('DetailBillPayment')}>
                                <View style={{ ...styleScoped.boxImageFavorite, backgroundColor: '#A54F9D' }} >
                                    <View style={{ width: hp('4%'), wp: hp('4%') }}>
                                        <Image source={require('../../assets/icon/billwhite.png')} style={{ ...styles.imageContain }} />
                                    </View>
                                </View>
                                <View style={{ marginTop: hp('1%') }}>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Soros</Text>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Athinarumit</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ ...styleScoped.boxItemFavorite }} onPress={() => Actions.push('DetailBillPayment')}>
                                <View style={{ ...styleScoped.boxImageFavorite }} >
                                    <View style={{ width: hp('4%'), wp: hp('4%') }}>
                                        <Image source={require('../../assets/icon/qrcodewhite.png')} style={{ ...styles.imageContain }} />
                                    </View>
                                </View>
                                <View style={{ marginTop: hp('1%') }}>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Weeraya</Text>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Pattanakrit…</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ ...styleScoped.boxItemFavorite }} onPress={() => Actions.push('ChargePincode')}>
                                <View style={{ ...styleScoped.boxImageFavorite }} >
                                    <View style={{ width: hp('4%'), wp: hp('4%') }}>
                                        <Image source={require('../../assets/icon/getmoneywhite.png')} style={{ ...styles.imageContain }} />
                                    </View>
                                </View>
                                <View style={{ marginTop: hp('1%') }}>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Plyphichcha</Text>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Pipopvorac…</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ ...styleScoped.boxItemFavorite }} onPress={() => Actions.push('DetailBillPayment')}>
                                <View style={{ ...styleScoped.boxImageFavorite }} >
                                    <View style={{ width: hp('4%'), wp: hp('4%') }}>
                                        <Image source={require('../../assets/icon/qrcodewhite.png')} style={{ ...styles.imageContain }} />
                                    </View>
                                </View>
                                <View style={{ marginTop: hp('1%') }}>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Naris</Text>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Paireekayard</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ ...styleScoped.boxItemFavorite }} onPress={() => Actions.push('DetailBillPayment')}>
                                <View style={{ ...styleScoped.boxImageFavorite, backgroundColor: '#A54F9D' }} >
                                    <View style={{ width: hp('4%'), wp: hp('4%') }}>
                                        <Image source={require('../../assets/icon/billwhite.png')} style={{ ...styles.imageContain }} />
                                    </View>
                                </View>
                                <View style={{ marginTop: hp('1%') }}>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Soros</Text>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Athinarumit</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ ...styleScoped.boxItemFavorite }} onPress={() => Actions.push('DetailBillPayment')}>
                                <View style={{ ...styleScoped.boxImageFavorite }} >
                                    <View style={{ width: hp('4%'), wp: hp('4%') }}>
                                        <Image source={require('../../assets/icon/qrcodewhite.png')} style={{ ...styles.imageContain }} />
                                    </View>
                                </View>
                                <View style={{ marginTop: hp('1%') }}>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Naris</Text>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Paireekayard</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ ...styleScoped.boxItemFavorite }} onPress={() => Actions.push('DetailBillPayment')}>
                                <View style={{ ...styleScoped.boxImageFavorite }} >
                                    <View style={{ width: hp('4%'), wp: hp('4%') }}>
                                        <Image source={require('../../assets/icon/qrcodewhite.png')} style={{ ...styles.imageContain }} />
                                    </View>
                                </View>
                                <View style={{ marginTop: hp('1%') }}>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Naris</Text>
                                    <Text style={{ ...styleScoped.textNameFavorite }}>Paireekayard</Text>
                                </View>
                            </TouchableOpacity>
                        </ScrollView>
                    </View>
                    <View style={{ height: hp('17%'), marginTop: hp('2%'), ...styles.container, borderRadius: 10 }}>
                        <Carousel />
                    </View>
                </View>
            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    sectionMain: {
        flex: 1, position: 'relative',
        zIndex: 30,
        // showdow
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
        // end showdow
        backgroundColor: 'white',
        borderBottomLeftRadius: hp('5%'),
        borderBottomRightRadius: hp('5%'),
        paddingBottom: hp('3%'),
        paddingTop: hp('5%')
    },
    imageProfile: {
        width: hp('5%'),
        height: hp('5%'),
        backgroundColor: '#ED4F9D',
        borderRadius: 100,
    },
    imageLogo: {
        height: hp('5%'),
        width: wp('30%')
    },
    textNameWallet: {
        fontSize: hp('3%'),
        color: theme.colors.text
    },
    textAmount: {
        fontSize: hp('5%'),
        color: theme.colors.text
    },
    boxTextNameWallet: {
        padding: hp('2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boxTextAmount: {
        paddingHorizontal: hp('3%'),
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: Platform.OS === 'ios' ? hp('1%') : hp('0%'),
    },
    boxTimeWallet: {
        paddingHorizontal: hp('3%'),
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: hp('0.8%'),
        alignItems: 'center'
    },
    textTimeWallet: {
        fontSize: hp('2%'),
        color: 'rgba(0,0,0,0.6)',
        fontWeight: '300'
    },
    boxMenu: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    boxMenuItem: {
        paddingHorizontal: Platform.OS === 'ios' ? hp('2%') : hp('3%')
    },
    boxImageMenu: {
        height: hp('5%'),
        width: hp('10%')
    },
    textMenu: {
        textAlign: 'center',
        marginTop: hp('2%'),
        fontSize: Platform.OS === 'ios' ? hp('1.8%') : hp('2%'),
        color: theme.colors.text,
    },
    boxTitleFavorite: {
        marginTop: hp('1.2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    textTitleFavorite: {
        fontSize: hp('2.5%'),
        color: theme.colors.text
    },
    boxImageFavorite: {
        height: hp('8%'),
        width: hp('8%'),
        borderRadius: 50,
        backgroundColor: theme.colors.primary,
        alignItems: 'center'
    },
    textNameFavorite: {
        textAlign: 'center',
        fontSize: Platform.OS === 'ios' ? hp('1.5%') : hp('1.8%'),
        color: theme.colors.text
    },
    boxItemFavorite: {
        marginRight: wp('5%'),
        alignItems: "center"
    },
    linearGradient: {
        height: hp('75%'),
        borderBottomRightRadius: hp('13%'),
    },
    noti: {
        width: hp('1%'),
        height: hp('1%'),
        backgroundColor: 'red',
        borderRadius: hp('50%'),
        position: 'absolute',
        right: 3
    },
    boxImageBackgroundWallet: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
        borderRadius: 25,
        ...Platform.select({
            ios: {
                shadowColor: "#480018",
                shadowOffset: {
                    width: 0,
                    height: 10,
                },
                shadowOpacity: 0.2,
                shadowRadius: 20,
            },
            android: {
                elevation: 15,
                backgroundColor: 'rgba(0,0,0,0.01)',
                // backgroundColor: 'white',
                // borderRadius:20
            },
        }),
    },
    shadow: {

    }

})


