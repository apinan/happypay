
import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';

export default class Login extends Component {
    render() {
        return (
            <>
                {/* <SafeAreaView style={{ flex: 1 }}> */}
                <LinearGradient colors={['#FF7F76', '#EB4060']} style={styleScoped.linearGradient}>
                    <View style={{ marginTop: hp('20%') }}>
                        <Text style={{ textAlign: 'center', fontSize: hp('8%') ,  }}>HAPPY PAY.</Text>
                    </View>
                    <View style={{ marginTop: hp('10%'), ...styles.container }} >
                        <Text style={{ textAlign: 'center', fontSize: hp('3.5%'), color: 'white' }}>ใส่หมายเลขโทรศัพท์</Text>
                        <Text style={{ textAlign: 'center', fontSize: hp('1.2%'), marginTop: hp('1%'), color: 'white' }}>หมายเลขโทรศัพท์ที่จะใช้ในการลงทะเบียนเข้าสู่ระบบ</Text>

                        <View style={{ paddingHorizontal: hp('3%'), marginTop: hp('5%') }}>
                            <View style={{ paddingVertical: hp('1.5%'), borderBottomWidth: 1, borderBottomColor: 'white' }}>
                                <TextInput style={{ fontSize: hp('2.2%'), color: 'white' }} value="ไทย"></TextInput>
                            </View>
                            <View style={{ marginTop: hp('2%'), flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <View style={{ paddingVertical: hp('1.5%'), borderBottomWidth: 1, borderBottomColor: 'white',width:wp('10%'),marginRight:hp('3%') }}>
                                    <TextInput style={{ fontSize: hp('2.2%'), color: 'white' }} value="+66"></TextInput>
                                </View>
                                <View style={{ paddingVertical: hp('1.5%'), borderBottomWidth: 1, borderBottomColor: 'white' , width:wp('61%') }}>
                                    <TextInput style={{ fontSize: hp('2.2%'), color: 'white' }} value="81-555-4466"></TextInput>
                                </View>
                            </View>
                        </View>

                    </View>
                </LinearGradient>
                <View style={{ backgroundColor: '#EB4060', height: hp('15%') }}>
                    <View style={{ backgroundColor: 'white', height: '100%', borderTopLeftRadius: hp('13%'), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity style={{ width: wp('60%'), backgroundColor: '#E4345F', padding: hp('1.5%'), borderRadius: 50, ...styleScoped.shadow }} onPress={()=>Actions.push('OTP')}>
                            <Text style={{ color: 'white', fontSize: hp('2%'), textAlign: 'center' }}>ถัดไป</Text>
                        </TouchableOpacity>
                    </View>
                </View>


                {/* </SafeAreaView> */}
            </>
        )
    }
};


const styleScoped = StyleSheet.create({
    linearGradient: {
        height: hp('75%'),
        borderBottomRightRadius: hp('13%'),
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,
    }

})


