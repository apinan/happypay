
import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import theme from '../../constants'
import { LoginManager } from "react-native-fbsdk";
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin';

import AsyncStorage from '@react-native-async-storage/async-storage';



export default class LoginMain extends Component {



    componentDidMount() {
        GoogleSignin.configure({
            webClientId: '966481048110-ohe328gfgbgj4n5ka4qmj6402es1f2lj.apps.googleusercontent.com',
        });
    }

    async googleSignin() {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            await AsyncStorage.setItem('googleLogin',JSON.stringify(userInfo.user))
            console.log('User : ',userInfo.user.givenName , 'is login.')
        } catch (error) {
            console.log(error)
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
            } else {
                // some other error happened
            }
        }
        Actions.push('Main')
    }
    facebookLogin() {
        LoginManager.logInWithPermissions(["public_profile"]).then(
            function (result) {
                if (result.isCancelled) {
                    console.log("Login cancelled");
                } else {
                    console.log(
                        "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );
                }
                Actions.push('Main')
            },
            function (error) {
                console.log("Login fail with error: " + error);
                Actions.push('Main')
            }
        );
    }

    render() {

        return (
            <ImageBackground source={require('../../assets/image/Bg01.png')} style={{ flex: 1 }} >
                <View style={{ marginTop: hp('8%'), ...styles.container }}>
                    <Icon name="chevron-left" size={hp('4%')} color="#ED4F9D" onPress={() => Actions.push('LoginMain')} />
                </View>
                <View style={{ marginTop: hp('8%'), ...styles.container, paddingHorizontal: hp('4%') }}>
                    <Text style={{ ...styles.textNameTitleScene }}>Welcome</Text>
                    <Text style={{ ...styles.textNameTitleScene }}>Back.</Text>
                </View>
                <View style={{ flexDirection: 'column', justifyContent: 'flex-end', flex: 1, }}>
                    <View style={{ ...styleScoped.bottomSheetForlogin }}>
                        <TouchableOpacity style={{ ...styleScoped.btnLoginStyle }} onPress={() => Actions.push('EmailLogin')}>
                            <View style={{ ...styleScoped.boxBtnImage }}>
                                <Image source={require('../../assets/image/Email.png')} style={{ ...styles.imageContain }} />
                            </View>
                            <View style={{ width: '85%' }}>
                                <Text style={{ ...styleScoped.textBtn }}>Sign in with Email</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ ...styleScoped.btnLoginStyle, marginTop: hp('2.8%') }} onPress={() => this.googleSignin()}>
                            <View style={{ ...styleScoped.boxBtnImage }}>
                                <Image source={require('../../assets/image/Google.png')} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                            </View>
                            <View style={{ width: '85%' }}>
                                <Text style={{ ...styleScoped.textBtn }}>Sign in with Google</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ ...styleScoped.btnLoginStyle, marginTop: hp('2.8%'), backgroundColor: '#1E82EE', borderColor: '#1E82EE' }} onPress={() => this.facebookLogin()}>
                            <View style={{ ...styleScoped.boxBtnImage }}>
                                <Image source={require('../../assets/image/Facebook.png')} style={{ width: '100%', height: '100%', resizeMode: 'contain' }} />
                            </View>
                            <View style={{ width: '85%' }}>
                                <Text style={{ ...styleScoped.textBtn, color: 'white' }}>Sign in with Facebook</Text>
                            </View>

                        </TouchableOpacity>


                    </View>
                </View>

                <View style={{ ...styles.container, marginTop: hp('2%') }}>

                </View>
            </ImageBackground>
        )
    }
};


const styleScoped = StyleSheet.create({
    boxBtnImage: {
        width: wp('10%'),
        height: hp('5%')
    },
    textBtn: {
        fontSize: hp('2%'),
        color: theme.colors.text,
        textAlign: 'center'
    },
    btnLoginStyle: {
        padding: hp('1%'),
        backgroundColor: 'white',
        borderRadius: 50,
        borderWidth: 0.5,
        borderColor: '#C6C6C6',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    btnRegisterStlye: {
        borderRadius: hp('50%'),
        padding: hp('1.8%'),
        fontSize: hp('2%'),
        marginTop: hp('0.5')
    },
    bottomSheetForlogin: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.5,
        shadowRadius: 30,
        elevation: 20,
        backgroundColor: 'white',
        ...styles.container,
        paddingBottom: hp('8%'),
        paddingTop: hp('5%'),
        borderTopLeftRadius: hp('5%'),
        borderTopRightRadius: hp('5%'),
        marginBottom: hp('-2%')
    },
    shadowLogin: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.5,
        shadowRadius: 30,
        elevation: 20,
    }
})


