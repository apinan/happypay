
import React, { Component } from 'react';
import {
    View, Text, Image, StyleSheet, TouchableOpacity, ImageBackground, StatusBar,
    Platform
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import theme from '../../constants'
import { Actions } from 'react-native-router-flux';
import { Button } from 'react-native-elements';

import AsyncStorage from '@react-native-async-storage/async-storage';


export default class LoginMain extends Component {

    

    async getPincode(){
        let pincode = await AsyncStorage.getItem('user_pincode')
        if(pincode){
            Actions.push('Main')
        }
    }

    componentDidMount() {
        this.getPincode()
    }


    render() {
        return (
            <ImageBackground source={require('../../assets/image/Bg01.png')} style={{ flex: 1 }} >
                <StatusBar barStyle="light-content" backgroundColor="transparent" translucent={true} />
                <View >
                    <View style={{ ...styleScoped.boxLogo }}>
                        <Image source={require('../../assets/image/Logo.png')} style={{ ...styles.imageContain, alignSelf: 'center' }} />
                    </View>
                </View>
                <View style={{ marginTop: Platform.OS === 'ios' ? hp('23%') : hp('27%') }}>
                    <Text style={{ textAlign: 'center', fontSize: hp('2%') }}>Mobile Wallet</Text>
                    <Text style={{ textAlign: 'center', fontSize: hp('2%') }}>and Payment Solution</Text>
                    <Text style={{ textAlign: 'center', fontSize: hp('1.4% '), marginTop: hp('1%') }}>
                        Your Happy Wallet for YourHappy Lifestyle
                    </Text>
                </View>
                <View style={{ ...styles.container, marginTop: hp('3.8%') }}>
                    <Button title="Sign in"
                        buttonStyle={{ ...styles.btnPrimary }}
                        onPress={() => Actions.push('ChooseTypeLogin')}
                        titleStyle={{ fontSize: hp('2%') }} />
                    <TouchableOpacity
                        style={{ ...styles.btnSecondary, marginTop: hp('2%') }}
                        onPress={() => Actions.push('ChooseTypeRegister')}>
                        <Text style={{ fontSize: hp('2%'), color: '#12173C', textAlign: 'center' }}>Register</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        )
    }
};


const styleScoped = StyleSheet.create({
    boxLogo: {
        marginTop: Platform.OS === 'ios' ? hp('30%') : hp('30%'),
        width: wp('90%'),
        height: hp('10%')
    },
})


