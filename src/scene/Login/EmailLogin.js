
import React, { Component } from 'react';
import {
    View, KeyboardAvoidingView, Text, StyleSheet, TextInput, TouchableOpacity, ImageBackground, Platform, Alert
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import { Button, ThemeContext } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import theme from '../../constants'
import { emailLogin } from '../../service/AuthService'
import AsyncStorage from '@react-native-async-storage/async-storage';



export default class EmailLogin extends Component {

    state = {
        showPassword: false,
        email: null,
        password: null
    }

    toggleShowPassword() {
        let showPassword = this.state.showPassword ? false : true
        this.setState({ showPassword })
    }

    async signIn() {
        try {
            const { email, password } = this.state
            let { result, token } = await emailLogin(email, password) // request Login  will return 'result'  and 'token'
            if (result === 'success') {
                await AsyncStorage.setItem('token', token) // set token to item stroge name 'token'
                Actions.push('Main')
            } else {
                
            }
        } catch (error) { // if login error
            Alert.alert(
                "แจ้งเตือน",
                "อีเมลล์หรือรหัสผ่านของท่านไม่ถูกต้อง",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
        }
        // Actions.push('Main')
    }


    render() {
        const { showPassword, email, password } = this.state
        return (

            <ImageBackground source={require('../../assets/image/Bg01.png')} style={{ flex: 1 }} >

                <View style={{ marginTop: hp('8%'), ...styles.container }}>
                    <Icon name="chevron-left" size={hp('4%')} color={theme.colors.primary} onPress={() => Actions.push('LoginMain')} />
                </View>
                <View style={{ marginTop: hp('8%'), ...styles.container, paddingHorizontal: hp('4%') }}>
                    <Text style={{ ...styles.textNameTitleScene }}>Sign in</Text>
                    <Text style={{ ...styles.textNameTitleScene }}>with Email.</Text>
                </View>
                <View style={{ flexDirection: 'column', justifyContent: 'flex-end', flex: 1, }}>
                    <View style={{ ...styles.bottomSheetForlogin }}>
                        <KeyboardAvoidingView>
                            {/* email input  */}
                            <View style={{
                                ...styleScoped.boxEmail,
                                borderBottomColor: email ? theme.colors.primary : theme.colors.secondary
                            }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Icon
                                        name="email" size={hp('2.5%')}
                                        color={email ? theme.colors.primary : theme.colors.secondary}
                                        style={{ marginRight: hp('3%') }} />
                                    <TextInput
                                        value={email}
                                        style={{ fontSize: hp('2%'), width: '80%', padding: 0 }}
                                        color={email ? theme.colors.primary : theme.colors.secondary}
                                        placeholder="Email"
                                        onChangeText={(email) => this.setState({ email })}
                                    ></TextInput>
                                </View>
                                <View>
                                    <Icon name="check" size={hp('2.5%')} color={email ? theme.colors.primary : theme.colors.secondary} />
                                </View>
                            </View>
                            {/* end email input */}

                            {/* password input */}
                            <View style={{
                                ...styleScoped.boxInputPassword,
                                borderBottomColor: password ? theme.colors.primary : theme.colors.secondary
                            }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <Icon
                                        name="key-variant"
                                        size={hp('2.5%')}
                                        color={password ? theme.colors.primary : theme.colors.secondary}
                                        style={{ marginRight: hp('3%') }} />
                                    <TextInput
                                        value={password}
                                        style={{ fontSize: hp('2%'), width: '80%', padding: 0 }}
                                        color={password ? theme.colors.primary : theme.colors.secondary}
                                        placeholder="password"
                                        secureTextEntry={!showPassword}
                                        onChangeText={(password) => this.setState({ password })}
                                    ></TextInput>
                                </View>
                                <View>
                                    <Icon
                                        name={showPassword ? "eye-off-outline" : "eye-outline"}
                                        size={hp('2.5%')}
                                        color={password ? theme.colors.primary : theme.colors.secondary}
                                        onPress={() => this.toggleShowPassword()} />
                                </View>
                            </View>
                            {/* end password input  */}

                            <Button title="Sign in" buttonStyle={{ ...styles.btnPrimary }} onPress={() => this.signIn()} />

                            <View style={{ borderBottomColor: '#C6C6C6', borderBottomWidth: hp('0.1%'), marginVertical: hp('3%'), flexDirection: 'row', justifyContent: 'center' }}>
                                <View style={{ ...styleScoped.lineOr }}>
                                    <Text style={{ ...styleScoped.textOr }}>or</Text>
                                </View>
                            </View>


                            <TouchableOpacity style={{ ...styles.btnSecondary }} onPress={() => Actions.push('ChooseTypeRegister')}>
                                <Text style={{ ...styleScoped.textBtnRegister }}>Register</Text>
                            </TouchableOpacity>
                        </KeyboardAvoidingView>

                    </View>

                </View>

                <View style={{ ...styles.container, marginTop: hp('2%') }}>

                </View>

            </ImageBackground >

        )
    }
};

const styleScoped = StyleSheet.create({
    textOr: {
        textAlign: 'center',
        fontSize: hp('1.5%'),
        color: '#C6C6C6'
    },
    lineOr: {
        padding: hp('1%'),
        backgroundColor: 'white',
        marginTop: hp('-2.2%'),
        position: 'absolute'
    },
    textBtnRegister: {
        fontSize: hp('2%'),
        color: theme.colors.text,
        textAlign: 'center'
    },
    boxEmail: {
        paddingVertical: Platform.OS === 'ios' ? hp('1.5%') : hp('1%'),
        borderBottomColor: '#ED4F9D',
        borderBottomWidth: hp('0.1%'),
        marginBottom: hp('2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boxInputPassword: {
        paddingVertical: Platform.OS === 'ios' ? hp('1.5%') : hp('1%'),
        borderBottomColor: '#C6C6C6',
        borderBottomWidth: hp('0.1%'),
        marginBottom: hp('5%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    btnLoginStyleRegister: {
        padding: hp('1.8%'),
        // backgroundColor:'white',
        borderRadius: 50,
        borderWidth: 0.5,
        borderColor: '#C6C6C6'
    },
    btnLoginStyle: {
        borderRadius: 50,
        backgroundColor: '#ED4F9D',
        padding: hp('1.8%'),
        fontSize: hp('2%')
    },
    btnRegisterStlye: {
        borderRadius: hp('50%'),
        padding: hp('1.8%'),
        fontSize: hp('2%'),
        marginTop: hp('0.5')
    },
    shadowLogin: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.5,
        shadowRadius: 30,

        elevation: 20,
    }
})


