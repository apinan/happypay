
import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';

export default class Login extends Component {
    render() {
        return (
            <>
                {/* <SafeAreaView style={{ flex: 1 }}> */}
                <LinearGradient colors={['#FF7F76', '#EB4060']} style={styleScoped.linearGradient}>
                    <View style={{ marginTop: hp('20%') }}>
                        <Text style={{ textAlign: 'center', fontSize: hp('8%') }}>HAPPY PAY.</Text>
                    </View>
                    <View style={{ marginTop: hp('10%'), ...styles.container }} >
                        <Text style={{ textAlign: 'center', fontSize: hp('3.5%'), color: 'white' }}>ใส่รหัสยืนยัน</Text>
                        <Text style={{ textAlign: 'center', fontSize: hp('1.2%'), marginTop: hp('1%'), color: 'white' }}>รหัสยืนยันถูกต้องไปยัง +66-082-456-7890</Text>

                        <View style={{ paddingHorizontal: hp('3%'), marginTop: hp('5%') }}>
                            <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                                <Text style={{fontSize:hp('2%'),color:'white',marginRight:wp('25%')}}>OTP</Text>
                                <Text style={{fontSize:hp('2%'),color:'white'}}>Ref code <Text style={{fontWeight:'bold'}}>QwERDa24df</Text></Text>
                            </View>
                            <View style={{ paddingVertical: hp('1.5%'), borderBottomWidth: 1, borderBottomColor: 'white' , marginTop:hp('3%')}}>
                                <TextInput style={{ fontSize: hp('5%'), color: 'white' , fontWeight:"bold" , textAlign:'center' }} value="1 2 3 4 5 6"></TextInput>
                            </View>
                        </View>

                    </View>
                </LinearGradient>
                <View style={{ backgroundColor: '#EB4060', height: hp('15%') }}>
                    <View style={{ backgroundColor: 'white', height: '100%', borderTopLeftRadius: hp('13%'), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableOpacity style={{ width: wp('60%'), backgroundColor: '#E4345F', padding: hp('1.5%'), borderRadius: 50, ...styleScoped.shadow }} onPress={()=>Actions.push('Profile')}>
                            <Text style={{ color: 'white', fontSize: hp('2%'), textAlign: 'center' }}>ตกลง</Text>
                        </TouchableOpacity>
                    </View>
                </View>


                {/* </SafeAreaView> */}
            </>
        )
    }
};


const styleScoped = StyleSheet.create({
    linearGradient: {
        height: hp('75%'),
        borderBottomRightRadius: hp('13%'),
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,
    }

})


