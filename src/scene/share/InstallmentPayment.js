import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'
import { Button } from 'react-native-elements';


export default class PaymentSummary extends Component {

    constructor(props) {
        super(props)
        this.state = {
            fullamount: true,
            multipleTime: false
        }
    }



    render() {
        const { fullamount, multipleTime } = this.state
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>


                <ScrollView style={{
                    flex: 1,
                    position: 'relative',
                    backgroundColor: '#F2F2F7',
                    paddingTop:hp('5%')
                   
                }}>

                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <View style={{ width: wp('30%'), height: hp('8%') }}>
                            <Image source={require('../../assets/image/Logo.png')} style={{ ...styles.imageContain }} />
                        </View>
                    </View>

                    <View style={{ ...styles.container ,paddingBottom:hp('10%')}}>

                        <View style={{ ...styleScoped.customCard }}>
                            <Text style={{ fontSize: hp('2.5%'), textAlign: 'center', marginVertical: hp('2%'), color: '#12173C' }}>Installment payment</Text>
                            <Text style={{ fontSize: hp('1.8%'), textAlign: 'center', color: '#12173C' }}>Total Amount</Text>
                            <Text
                                style={{
                                    marginTop: hp('2%'),
                                    textAlign: 'center',
                                    fontSize: hp('4%'),
                                    color: '#12173C'
                                }}><Text style={{ color: '#ED4F9D' }}>฿</Text> 35,000</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: hp('2%') }}>
                                <View style={{ width: '48%', borderRightColor: '#C6C6C6', borderRightWidth: 1 }}>
                                    <Text style={{ textAlign: 'center', fontSize: hp('1.7%'), color: 'rgba(0,0,0,0.16)' }}>Month</Text>
                                    <Text style={{ textAlign: 'center', marginTop: hp('1%'), fontSize: hp('2%'), color: '#12173C' }}>8</Text>
                                </View>
                                <View style={{ width: '48%' }}>
                                    <Text style={{ textAlign: 'center', fontSize: hp('1.7%'), color: 'rgba(0,0,0,0.16)' }}>Rate</Text>
                                    <Text style={{ textAlign: 'center', marginTop: hp('1%'), fontSize: hp('2%'), color: '#12173C' }}>7.6%</Text>
                                </View>
                            </View>


                            <View style={{ marginTop: hp('3%'), ...styleScoped.boxInputCard }}>
                                <TextInput placeholder="Card Number" style={{ fontSize: hp('2.5%') , padding:0 }}></TextInput>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{ marginTop: hp('1%'), ...styleScoped.boxInputCard, width: '49%' }}>
                                    <TextInput placeholder="MM / YY" style={{ fontSize: hp('2.5%'), padding:0 }}></TextInput>
                                </View>
                                <View style={{ marginTop: hp('1%'), ...styleScoped.boxInputCard, width: '49%' }}>
                                    <TextInput placeholder="CVV" style={{ fontSize: hp('2.5%'), padding:0 }}></TextInput>
                                </View>
                            </View>

                            <Text style={{ marginTop: hp('2%'), textAlign: 'center', color: 'rgba(0,0,0,0.16)' }}>
                                Lorem Ipsum is simply dummy text of the printing
                                and typesetting industry
                            </Text>

                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <View style={{ width: wp('40%'), height: hp('5%') }}>
                                    <Image source={require('../../assets/image/all_card.png')} style={{ ...styles.imageContain }} />
                                </View>
                            </View>

                            <View style={{ marginVertical: hp('2%') }}>
                                <Button title="Pay now" buttonStyle={{ ...styles.btnPrimary }} onPress={() => Actions.push('ShareChooseMethod')} />
                            </View>


                            <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Payment Detail</Text>
                            <Text style={{ fontSize: hp('1.5%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('1%'), width: wp('78%') }}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore
                                magna aliqua. Ut enim ad minim veniam, quis nostrud e
                                xercitation
                                </Text>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: hp('2%') }}>
                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Expiry Date</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('1%') }}>09/10/2020 , 8:56 PM</Text>
                                </View>

                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Payment Type</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('0.8%') }}>เก็บเต็มจำนวน</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: hp('2%') }}>
                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Usage</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('1%') }}>1-Time</Text>
                                </View>
                            </View>


                 
                            <View style={{ ...styles.divider, marginVertical: hp('3%') }}></View>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Customer Name</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('1%') }}>Customer Name</Text>
                                </View>

                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Email</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('0.8%') }}>justtheemail@gmail.com</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: hp('2%') }}>
                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Customer Address</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('1%'), width: '90%' }}>
                                        555 อ่อนนุช 66 สุขุมวิท 77
                                        สวนหลวง สวนหลวง กทม
                                        10250
                                    </Text>
                                </View>

                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Mobile No.</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('0.8%') }}>081-268-2628</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: hp('2%') , alignItems:'flex-start' }}>
                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Tax Id</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('1%'), width: '90%' }}>
                                    1100999540251
                                    </Text>
                                </View>

                                <View style={{ width: wp('40%') , flexDirection:'row' , justifyContent:'flex-start'  , alignItems:'center'  }}>
                                    <Icon name="circle-outline" size={hp('3%')}  color="#C6C6C6" style={{marginRight:hp('1%')}}/>
                                    <Text style={{fontSize:hp('1.8%'),fontWeight:'300',color:'#12173C'}}>ต้องการใบกำกับภาษี</Text>
                                </View>
                            </View>

                        </View>
                    </View>


                </ScrollView>
            </View >

        )
    }
};


const styleScoped = StyleSheet.create({

    boxBtnSelected: {
        paddingHorizontal: hp('1%'),
        paddingVertical: hp('1%'),
        backgroundColor: 'white',
        borderRadius: hp('50%')
    },
    boxInputCard: {
        borderWidth: 1,
        padding: hp('1.5%'),
        borderRadius: 10,
        borderColor: '#C6C6C6'
    },
    customCard: {
        marginTop: hp('2%'),
        backgroundColor: 'white',
        padding: hp('2%'),
        borderRadius: hp('2%')
    }

})


