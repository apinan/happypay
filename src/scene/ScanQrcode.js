import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground, Platform, KeyboardAvoidingView, Alert
} from 'react-native';
import styles from '../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../components/Navbar'
import { Overlay, Button } from 'react-native-elements'
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import theme from '../constants'


export default class Transaction extends Component {

    constructor(props) {
        super(props)
        this.state = {
            visibleFilter: false,
            cameraType: 'back',
            cameraOn: true
        }
    }

    onSuccess(e) {
        Alert.alert('Scanqr', e.data,
            [
                { text: 'OK', onPress: () => this.backToMain() }
            ]
        )
    }

    componentWillUnmount() {

    }

    backToMain() {
        this.setState({ cameraOn: false })
        Actions.push('Main')
    }



    renderTop() {

    }

    render() {
        const { cameraType, cameraOn } = this.state
        return (

            <View style={{ backgroundColor: 'black', flex: 1, paddingTop: hp('5%'), }}>
                <View style={{ ...styles.container }}>
                    <View style={{ flexDirection: "row", justifyContent: 'space-between', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => this.backToMain()}>
                            <Icon name="chevron-left" size={hp('5%')} color="#ED4F9D" />
                        </TouchableOpacity>
                        <Icon name="image-outline" size={hp('5%')} color="#ED4F9D" />
                    </View>
                </View>
                {
                    cameraOn && <QRCodeScanner
                        topContent={<Text style={{ color: 'white' }}>Please scan barcode / QR code here</Text>}
                        cameraType={cameraType}
                        containerStyle={{ backgroundColor: 'black' }}
                        onRead={this.onSuccess}
                        showMarker={true}
                        markerStyle={{ borderRadius: 10, borderColor: '#ED4F9D' }}
                        customMarker={this.renderCustomMarker()}
                    />
                }

            </View>

        )
    }



    renderCustomMarker() {

        return (
            <View style={{ width: wp('60%'), height: wp('60%') }}>

                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 1, ...styleScoped.leftTop }}></View>
                    <View style={{ flex: 1 }}></View>
                    <View style={{ flex: 1, ...styleScoped.rightTop }}></View>
                </View>
                <View style={{ flex: 1 }}></View>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 1, ...styleScoped.letfBottom }}></View>
                    <View style={{ flex: 1 }}></View>
                    <View style={{ flex: 1, ...styleScoped.rightBottom }}></View>
                </View>

            </View>
        )
    }
};


const styleScoped = StyleSheet.create({
    leftTop: {
        borderLeftWidth: 2,
        borderLeftColor: theme.colors.primary,
        borderTopWidth: 2,
        borderTopColor: theme.colors.primary,
    },
    rightTop: {
        borderRightWidth: 2,
        borderRightColor: theme.colors.primary,
        borderTopWidth: 2,
        borderTopColor: theme.colors.primary,
    },
    letfBottom: {
        borderLeftWidth: 2,
        borderLeftColor: theme.colors.primary,
        borderBottomWidth: 2,
        borderBottomColor: theme.colors.primary,
    },
    rightBottom: {
        borderRightWidth: 2,
        borderRightColor: theme.colors.primary,
        borderBottomWidth: 2,
        borderBottomColor: theme.colors.primary,
    }
})


