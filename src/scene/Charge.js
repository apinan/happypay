import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, Platform
} from 'react-native';
import styles from '../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../components/Navbar'
import { Button } from 'react-native-elements';


export default class Charge extends Component {

    constructor(props) {
        super(props)
        this.state = {
            fullamount: true,
            multipleTime: false,
            valueAmount: '500'
        }
    }



    render() {
        const { fullamount, multipleTime, valueAmount } = this.state
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="เรียกเก็บเงิน" backTo="Main" ></Navbar>
                <ScrollView style={{ flex: 1, backgroundColor: '#F2F2F7', position: 'relative' }}>

                    <View style={{ marginTop: hp('3%'), ...styles.container }}>

                        <View style={{ ...styleScoped.boxBtnSelected }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                {
                                    fullamount
                                        ?
                                        <TouchableOpacity style={{ ...styles.btnPrimary, width: '49%', padding: hp('1.5%') }} >
                                            <Text style={{ textAlign: 'center', fontSize: hp('1.8%'), color: 'white' }}>เก็บเต็มจำนวน</Text>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity
                                            style={{ ...styles.btnPrimary, width: '49%', backgroundColor: 'white', padding: hp('1.5%') }}
                                            onPress={() => this.setState({ fullamount: true })}>
                                            <Text style={{ textAlign: 'center', fontSize: hp('1.8%'), color: '#707070' }}>เก็บเต็มจำนวน</Text>
                                        </TouchableOpacity>
                                }

                                {
                                    fullamount
                                        ?
                                        <TouchableOpacity
                                            style={{ ...styles.btnPrimary, width: '49%', backgroundColor: 'white', padding: hp('1.5%') }}
                                            onPress={() => this.setState({ fullamount: false })}>
                                            <Text style={{ textAlign: 'center', fontSize: hp('1.8%'), color: '#707070' }}>ผ่อนชำระ</Text>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity style={{ ...styles.btnPrimary, width: '49%', padding: hp('1.5%') }}>
                                            <Text style={{ textAlign: 'center', fontSize: hp('1.8%'), color: 'white' }}>ผ่อนชำระ</Text>
                                        </TouchableOpacity>
                                }
                            </View>
                        </View>

                        <View style={{ ...styleScoped.customCard }}>
                            <Text style={{ color: '#12173C', fontSize: hp('2%'), fontWeight: '300' }}>Input your amount</Text>
                            <View style={{ ...styleScoped.customBoxInput, marginBottom: hp('3%') }}>
                                <Text style={{ fontSize: hp('3%'), color: '#ED4F9D', marginRight: hp('1%') }}>฿</Text>
                                <TextInput
                                    value={valueAmount}
                                    keyboardType="number-pad"
                                    onTextInput={(text) => this.setState({ valueAmount: text })}
                                    style={{
                                        fontSize: hp('3%'),
                                        color: '#12173C',
                                        fontWeight: '300',
                                        padding: 0,
                                        width: '85%'
                                    }}></TextInput>
                            </View>

                            <Text style={{ color: '#12173C', fontSize: hp('2%'), fontWeight: '300' }}>Payment Detail</Text>
                            <View style={{ ...styleScoped.customBoxInput, marginBottom: hp('2%') }}>
                                <TextInput placeholder="Input your payment Detail"
                                    style={{
                                        fontSize: hp('2%'),
                                        color: '#12173C',
                                        fontWeight: '300',
                                        textAlignVertical: 'top',
                                        padding:0
                                    }}
                                    multiline={true}
                                    numberOfLines={4}
                                    minHeight={Platform.OS === 'ios' ? 90 : null}
                                ></TextInput>
                            </View>
                        </View>


                        {
                            !fullamount
                                ?
                                <View style={{ ...styleScoped.customCard }}>
                                    <Text style={{ color: '#12173C', fontSize: hp('2%'), fontWeight: '300' }}>Select Bank</Text>
                                    <View style={{ ...styleScoped.customBoxSelectoin, justifyContent: 'space-between', alignItems: 'center' }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                            <View style={{ height: hp('2%'), width: hp('2%'), marginRight: hp('1%') }}>
                                                <Image source={require('../assets/icon/kbank.png')} style={{ ...styles.imageContain }} />
                                            </View>
                                            <Text style={{ fontSize: hp('2%'), color: '#12173C', fontWeight: '300' }}>Kasikorn Bank</Text>
                                        </View>
                                        <Icon name="chevron-down" size={hp('2.8%')} />
                                    </View>
                                    <View style={{ ...styleScoped.customBoxSelectoin, justifyContent: 'space-between', alignItems: 'center' }}>
                                        <Text style={{ fontSize: hp('2%'), color: '#12173C', fontWeight: '300' }}>8 month</Text>
                                        <Icon name="chevron-down" size={hp('2.8%')} />
                                    </View>
                                    <Text style={{ marginLeft: hp('1%'), marginTop: hp('1%'), fontSize: hp('1.5%'), color: '#ED4F9D' }}>Rate : 7.9%</Text>
                                </View>
                                :
                                null
                        }


                        <View style={{ ...styleScoped.customCard }}>
                            <Text style={{ color: '#12173C', fontSize: hp('2%'), fontWeight: '300' }}>Usage</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: hp('1.2%') }}>
                                {
                                    multipleTime
                                        ?
                                        <TouchableOpacity style={{ ...styles.btnSecondary, width: '49%', padding: hp('1.5%') }} onPress={() => this.setState({ multipleTime: false })}>
                                            <Text style={{ textAlign: 'center', fontSize: hp('1.8%'), color: '#12173C' }}>1 - Time</Text>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity style={{ ...styles.btnPrimary, width: '49%', padding: hp('1.5%') }} >
                                            <Text style={{ textAlign: 'center', fontSize: hp('1.8%'), color: 'white' }}>1 - Time</Text>
                                        </TouchableOpacity>
                                }

                                {
                                    multipleTime
                                        ?
                                        <TouchableOpacity style={{ ...styles.btnPrimary, width: '49%', padding: hp('1.5%') }} >
                                            <Text style={{ textAlign: 'center', fontSize: hp('1.8%'), color: 'white' }}>Multiple Time</Text>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity style={{ ...styles.btnSecondary, width: '49%', padding: hp('1.5%') }} onPress={() => this.setState({ multipleTime: true })}>
                                            <Text style={{ textAlign: 'center', fontSize: hp('1.8%'), color: '#12173C' }}>Multiple Time</Text>
                                        </TouchableOpacity>
                                }

                            </View>
                        </View>

                        <View style={{ ...styleScoped.customCard }}>
                            <Text style={{ color: '#12173C', fontSize: hp('2%'), fontWeight: '300' }}>Expiry Date</Text>
                            <View style={{ ...styleScoped.customBoxSelectoin, justifyContent: 'space-between', alignItems: 'center' }}>
                                <Text style={{ fontSize: hp('2%'), color: '#12173C', fontWeight: '300' }}>1 Day</Text>
                                <Icon name="chevron-down" size={hp('2.8%')} />
                            </View>
                        </View>

                        {
                            !multipleTime
                                ?
                                <View style={{ ...styleScoped.customCard }}>
                                    <Text style={{ color: '#12173C', fontSize: hp('2%'), fontWeight: '300' }}>Customer info</Text>
                                    <View style={{ ...styleScoped.customBoxInputInfo, marginBottom: hp('1%')  }}>
                                        <TextInput placeholder="Customer Name" style={{ ...styleScoped.customTextInpuInfo }}></TextInput>
                                    </View>
                                    <View style={{ ...styleScoped.customBoxInputInfo, marginBottom: hp('1%') }}>
                                        <TextInput placeholder="Email Address" style={{ ...styleScoped.customTextInpuInfo }}></TextInput>
                                    </View>
                                    <View style={{ ...styleScoped.customBoxInputInfo, marginBottom: hp('1%') }}>
                                        <TextInput placeholder="Customer Address" style={{ ...styleScoped.customTextInpuInfo }}></TextInput>
                                    </View>
                                    <View style={{ ...styleScoped.customBoxInputInfo, marginBottom: hp('1%') }}>
                                        <TextInput placeholder="Mobile No." keyboardType="number-pad" style={{ ...styleScoped.customTextInpuInfo }}></TextInput>
                                    </View>
                                    <View style={{ ...styleScoped.customBoxInputInfo, marginBottom: hp('1%') }}>
                                        <TextInput placeholder="Tax Id" style={{ ...styleScoped.customTextInpuInfo }}></TextInput>
                                    </View>
                                </View>
                                :
                                null
                        }
                        <View style={{ marginTop: hp('2%'), marginBottom: hp('5%') }}>
                            <Button title="Create Invoice" buttonStyle={{ ...styles.btnPrimary }} onPress={() => Actions.push('PaymentSummary')} />
                        </View>







                    </View>

                </ScrollView>
            </View >

        )
    }
};


const styleScoped = StyleSheet.create({

    boxBtnSelected: {
        paddingHorizontal: hp('1%'),
        paddingVertical: hp('1%'),
        backgroundColor: 'white',
        borderRadius: hp('50%')
    },
    customBoxInput: {
        ...Platform.select({
            ios: {
                marginTop: hp('1%'),
                borderWidth: 0.5,
                padding: hp('1%'),
                borderColor: '#C6C6C6',
                borderRadius: hp('1.2%'),
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center'
            },
            android: {
                marginTop: hp('1%'),
                borderWidth: 0.5,
                paddingHorizontal: hp('1%'),
                paddingVertical: hp('1%'),
                borderColor: '#C6C6C6',
                borderRadius: hp('1.2%'),
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center'
            }
        })
    },
    customBoxInputInfo: {
        ...Platform.select({
            ios: {
                marginTop: hp('1%'),
                borderWidth: 0.5,
                padding: hp('1%'),
                borderColor: '#C6C6C6',
                borderRadius: hp('1.2%'),
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center'
            },
            android: {
                marginTop: hp('1%'),
                borderWidth: 0.5,
                paddingHorizontal: hp('1%'),
                paddingVertical: hp('0.5%'),
                borderColor: '#C6C6C6',
                borderRadius: hp('1.2%'),
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center'
            }
        })
    },
    customBoxSelectoin: {
        marginTop: hp('1%'),
        borderWidth: 0.5,
        padding: hp('1%'),
        borderColor: '#C6C6C6',
        borderRadius: hp('1.2%'),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    customTextInpuInfo: {
        width:'100%',
        fontSize: hp('2%'),
        color: '#12173C',
        fontWeight: '300',
        padding: Platform.OS === 'ios' ? null : 0 
    },
    customCard: {
        marginTop: hp('2%'),
        backgroundColor: 'white',
        padding: hp('2%'),
        borderRadius: hp('2%')
    }

})


