import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground, Share
} from 'react-native';
import styles from '../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../components/Navbar'
import { Overlay, Button } from 'react-native-elements'


export default class PaymentSummary extends Component {

    constructor(props) {
        super(props)

        this.state = {
            visibleShare: false
        }
    }


    onShare = async () => {
        try {
            const result = await Share.share({
                message:
                    'TEST React Native | A framework for building native apps using React',
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    toggleShare() {
        this.setState({ visibleShare: this.state.visibleShare ? false : true })
    }

    renderOverlayShare() {
        const { visibleShare } = this.state
        return (
            <Overlay isVisible={visibleShare}
                onBackdropPress={() => this.toggleShare()}
                overlayStyle={{ ...styleScoped.boxOverlayFilter }} >
                <TouchableOpacity style={{ ...styleScoped.boxSelectShare }}>
                    <View style={{ ...styleScoped.boxImageShare }}>
                        <Image source={require('../assets/image/qrcode.png')} style={{ ...styles.imageContain }} />
                    </View>
                    <Text style={{ ...styleScoped.textSelectShare }}>QR Cash</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{ ...styleScoped.boxSelectShare }}>
                    <View style={{ ...styleScoped.boxImageShare }}>
                        <Image source={require('../assets/image/wechat.png')} style={{ ...styles.imageContain }} />
                    </View>
                    <Text style={{ ...styleScoped.textSelectShare }}>WeChat</Text>
                </TouchableOpacity>

            </Overlay>
        )
    }


    render() {
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Payment Summary" backTo="Charge" ></Navbar>
                <ScrollView style={{
                    flex: 1,
                    position: 'relative',
                    backgroundColor: '#F2F2F7',
                }}>

                    <View style={{ marginTop: hp('1%'), ...styles.container, marginBottom: hp('3%') }}>

                        <View style={{ ...styleScoped.customCard }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                <Icon name="heart-outline" size={hp('3%')} style={{ color: '#C6C6C6' }} />
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <View style={{ width: wp('90%'), height: hp('25%') }}>
                                    <Image source={require('../assets/image/qr_code_payment.png')} style={{ ...styles.imageContain }} />
                                </View>
                            </View>
                            <Text
                                style={{
                                    marginTop: hp('2%'),
                                    textAlign: 'center',
                                    fontSize: hp('4%'),
                                    color: '#12173C'
                                }}><Text style={{ color: '#ED4F9D' }}>฿</Text> 35,000</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: hp('2%') }}>
                                <TouchableOpacity onPress={() => this.toggleShare()}>
                                    <Icon name="qrcode" size={hp('5%')} color="#ED4F9D" style={{ textAlign: 'center' }} />
                                    <Text style={{ fontSize: hp('1.8%'), color: '#979797', marginTop: hp('1%') }}>Share QR Code</Text>
                                </TouchableOpacity>
                                <View style={{ ...styles.dividerVertical, marginHorizontal: wp('10%') }}></View>
                                <TouchableOpacity onPress={() => this.onShare()}>
                                    <Icon name="link-variant" size={hp('5%')} color="#ED4F9D" style={{ textAlign: 'center' }} />
                                    <Text style={{ fontSize: hp('1.8%'), color: '#979797', marginTop: hp('1%') }}>Share link</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{ ...styles.divider, marginVertical: hp('3%') }}></View>


                            <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Payment Detail</Text>
                            <Text style={{ fontSize: hp('1.5%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('1%'), width: wp('78%') }}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore
                                magna aliqua. Ut enim ad minim veniam, quis nostrud e
                                xercitation
                                </Text>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: hp('2%') }}>
                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Expiry Date</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('1%') }}>09/10/2020 , 8:56 PM</Text>
                                </View>

                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Payment Type</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('0.8%') }}>เก็บเต็มจำนวน</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: hp('2%') }}>
                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Usage</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('1%') }}>1-Time</Text>
                                </View>

                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Bank</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('0.8%') }}>Kasikorn Bank</Text>
                                </View>
                            </View>


                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: hp('2%') }}>
                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Month</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('1%') }}>8 </Text>
                                </View>
                            </View>

                            <View style={{ ...styles.divider, marginVertical: hp('3%') }}></View>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Customer Name</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('1%') }}>Customer Name</Text>
                                </View>

                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Email</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('0.8%') }}>justtheemail@gmail.com</Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: hp('2%') }}>
                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Customer Address</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('1%'), width: '90%' }}>
                                        555 อ่อนนุช 66 สุขุมวิท 77
                                        สวนหลวง สวนหลวง กทม
                                        10250
                                    </Text>
                                </View>

                                <View style={{ width: wp('40%') }}>
                                    <Text style={{ fontSize: hp('2%'), fontWeight: '300', color: '#12173C' }}>Mobile No.</Text>
                                    <Text style={{ fontSize: hp('1.6%'), fontWeight: '300', color: '#ED4F9D', marginTop: hp('0.8%') }}>081-268-2628</Text>
                                </View>
                            </View>



                        </View>

                    </View>



                </ScrollView>
                {this.renderOverlayShare()}
            </View >

        )
    }
};


const styleScoped = StyleSheet.create({

    boxOverlayFilter: {
        paddingVertical: hp('2%'),
        paddingHorizontal: hp('2%'),
        borderRadius: 20,
        width: wp('90%'),
    },
    boxSelectShare: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        padding: hp('1.8%'),
        borderColor: 'rgba(0,0,0,0.16)',
        borderRadius: 50,
        borderWidth: 0.5,
        width: '100%',
        marginTop: hp('1%')
    },
    boxImageShare: {
        height: hp('3%'),
        width: wp('8%'),
        marginRight: hp('1%')
    },
    textSelectShare: {
        fontSize: hp('2%'),
        color: '#12173C',
        textAlign: 'center',
        fontWeight: '300'
    },
    textTitleOverlayFilter: {
        fontSize: hp('2.2%'),
        color: '#12173C',
    },
    warpperSelectFilter: {
        marginVertical: hp('1%'),
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    },
    boxBtnSelected: {
        paddingHorizontal: hp('1%'),
        paddingVertical: hp('1%'),
        backgroundColor: 'white',
        borderRadius: hp('50%')
    },
    customBoxInput: {
        marginTop: hp('1%'),
        borderWidth: 0.5,
        padding: hp('1%'),
        borderColor: '#C6C6C6',
        borderRadius: hp('1.2%'),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    customCard: {
        marginTop: hp('2%'),
        backgroundColor: 'white',
        padding: hp('2%'),
        borderRadius: hp('2%')
    }

})


