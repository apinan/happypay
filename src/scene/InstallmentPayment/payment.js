import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground
} from 'react-native';
import styles from '../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../components/Navbar'
import { Button } from 'react-native-elements';


export default class PaymentSummary extends Component {

    constructor(props) {
        super(props)
        this.state = {
            fullamount: true,
            multipleTime: false
        }
    }



    render() {
        const { fullamount, multipleTime } = this.state
        return (

            <SafeAreaView style={{ backgroundColor: '#F2F2F7', flex: 1 }}>
                <ScrollView style={{
                    flex: 1,
                    position: 'relative',
                    backgroundColor: '#F2F2F7',
                }}>




                </ScrollView>
            </SafeAreaView >

        )
    }
};


const styleScoped = StyleSheet.create({

    boxBtnSelected: {
        paddingHorizontal: hp('1%'),
        paddingVertical: hp('1%'),
        backgroundColor: 'white',
        borderRadius: hp('50%')
    },
    customBoxInput: {
        marginTop: hp('1%'),
        borderWidth: 0.5,
        padding: hp('1%'),
        borderColor: '#C6C6C6',
        borderRadius: hp('1.2%'),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    customCard: {
        marginTop: hp('2%'),
        backgroundColor: 'white',
        padding: hp('2%'),
        borderRadius: hp('2%')
    }

})


