import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'
import { Button } from 'react-native-elements';




export default class TopupSuccess extends Component {


    render() {
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Successfully" ></Navbar>


                <View style={{
                    flex: 1, position: 'relative',
                    zIndex: 20,
                    backgroundColor: '#F2F2F7',
                    paddingBottom: hp('3%'),
                    paddingTop: hp('3%')
                }}>

                    <View >
                        <View style={{ ...styles.container, }}>
                            <View style={{ borderRadius: 20, backgroundColor: 'white', padding: hp('2%'), marginBottom: hp('2%') }}>

                                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                    <Icon name="heart-outline" size={hp('3%')} color="#C6C6C6" />
                                </View>

                                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                    <View style={{ height: hp('15%'), width: wp('30%') }}>
                                        <Image source={require('../../assets/image//TopupSuccess.png')} style={{ ...styles.imageContain }} />
                                    </View>
                                </View>
                                <Text style={{ fontSize: hp('2%'), marginTop: hp('2%'), textAlign: 'center', color: '#ED4F9D' }}>Hurrah!</Text>
                                <Text style={{ fontSize: hp('2.5%'), textAlign: 'center', color: '#021038' }}>Topup Successfully Done</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: hp('4%'), alignItems: 'center' }}>
                                    <Text style={{ fontSize: hp('2%'), color: 'rgba(0,0,0,.16)' }}>จำนวนทั้งหมด</Text>
                                    <Text style={{ fontSize: hp('2%'), color: '#021038' }}><Text style={{ color: '#ED4F9D' }}>฿</Text>500</Text>
                                </View>

                                <Button title="Back to home" buttonStyle={{ ...styles.btnPrimary }} onPress={() => Actions.push('MyWallet')} />

                            </View>


                        </View>


                    </View>
                </View>




            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    boxInputSearch: {
        width: '100%',
        padding: hp('1%'),
        backgroundColor: '#EEEEF0',
        borderRadius: hp('1%'),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    boxListTransaction: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: hp('2%')
    },
    ListBill: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    iconBill: {
        width: hp('5%'),
        height: hp('5%'),
        marginRight: hp('2%')
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    }

})


