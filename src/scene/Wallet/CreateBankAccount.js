import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'
import { Button } from 'react-native-elements';




export default class CreateBankAccount extends Component {



    render() {
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Add Bank Account" backTo="MyWallet" ></Navbar>
                <View style={{
                    flex: 1, position: 'relative',
                    zIndex: 20,
                    backgroundColor: '#F2F2F7',
                    paddingBottom: hp('3%'),
                    paddingTop: hp('3%')
                }}>

                    <View style={{ }}>
                        <View style={{ ...styleScoped.boxListNameBankAccount }}>
                            {/* scb */}
                            <TouchableOpacity style={{...styleScoped.listNameBankAccount}} onPress={()=>Actions.push('AddBankAccount')}>
                                <View style={{...styleScoped.boxImageBankAccount}}>
                                    <Image source={require('../../assets/icon/scb.png')} style={{...styles.imageContain}} />
                                </View>
                                <Text style={{...styleScoped.titleNameBankAccount}}>ธนาคารไทยพาณิชย์</Text>
                            </TouchableOpacity>
                            <View style={{...styles.divider}}></View>
                            {/* kbank */}
                            <TouchableOpacity style={{...styleScoped.listNameBankAccount}}>
                                <View style={{...styleScoped.boxImageBankAccount}}>
                                    <Image source={require('../../assets/icon/kbank.png')} style={{...styles.imageContain}} />
                                </View>
                                <Text style={{...styleScoped.titleNameBankAccount}}>ธนาคารกสิกรไทย</Text>
                            </TouchableOpacity>
                            <View style={{...styles.divider}}></View>
                            {/* krungthai */}
                            <TouchableOpacity style={{...styleScoped.listNameBankAccount}}>
                                <View style={{...styleScoped.boxImageBankAccount}}>
                                    <Image source={require('../../assets/icon/ktb.png')} style={{...styles.imageContain}} />
                                </View>
                                <Text style={{...styleScoped.titleNameBankAccount}}>ธนาคารกรุงไทย</Text>
                            </TouchableOpacity>
                            <View style={{...styles.divider}}></View>
                            {/* bangkok */}
                            <TouchableOpacity style={{...styleScoped.listNameBankAccount}}>
                                <View style={{...styleScoped.boxImageBankAccount}}>
                                    <Image source={require('../../assets/icon/bank.png')} style={{...styles.imageContain}} />
                                </View>
                                <Text style={{...styleScoped.titleNameBankAccount}}>ธนาคารกรุงเทพ</Text>
                            </TouchableOpacity>
                            <View style={{...styles.divider}}></View>
                            {/* GSB */}
                            <TouchableOpacity style={{...styleScoped.listNameBankAccount}}>
                                <View style={{...styleScoped.boxImageBankAccount}}>
                                    <Image source={require('../../assets/icon/gsb.png')} style={{...styles.imageContain}} />
                                </View>
                                <Text style={{...styleScoped.titleNameBankAccount}}>ธนาคารออมสิน</Text>
                            </TouchableOpacity>
                            <View style={{...styles.divider}}></View>
                            {/* krung sri */}
                            <TouchableOpacity style={{...styleScoped.listNameBankAccount}}>
                                <View style={{...styleScoped.boxImageBankAccount}}>
                                    <Image source={require('../../assets/icon/bay.png')} style={{...styles.imageContain}} />
                                </View>
                                <Text style={{...styleScoped.titleNameBankAccount}}>ธนาคารไทยพาณิชย์</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>



            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    boxListNameBankAccount: {
        backgroundColor: 'white',
        paddingVertical: hp('1%'),
        ...styles.container,
    },
    listNameBankAccount: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems:'center',
        marginVertical:hp('1%')
    },
    titleNameBankAccount:{
        fontSize:hp('1.8%'),
        fontWeight:'200',
        color:'#12173C'
    },
    boxImageBankAccount:{
        height:hp('4.5%'),
        width:hp('4.5%'),
        marginRight:wp('4%')
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    }

})


