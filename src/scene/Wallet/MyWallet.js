import React, { Component } from 'react';
import {
    View, Text, Image, StyleSheet, TextInput, TouchableOpacity, ImageBackground, Platform
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import RBSheet from "react-native-raw-bottom-sheet";
import { Button, Tooltip } from 'react-native-elements';
import Picker from 'react-native-picker';
import Popover, { PopoverMode, PopoverPlacement } from 'react-native-popover-view';

export default class MyWallet extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isVisibleMenuBankAndCredit: false,
            type: '',
            isVisibleTooltip: false
        }

    }

    openTopup(value) {
        this.setState({ type: value })
        this.RBSheet.open()
        this.setState({ isVisible: true })
    }
    selectMenu(value) {
        if (value == 'bankaccount') {
            this.setState({ isVisibleMenuBankAndCredit: false })
            Actions.push('CreateBankAccount')
        } else if (value == 'creditcard') {
            this.setState({ isVisibleMenuBankAndCredit: false })
            Actions.push('CreateCreditCard')
        }
    }

    confirmTopup() {
        this.RBSheet.close()
        if (this.state.type == 'topup') {
            Actions.push('TopupPincode')
        } else {
            Actions.push('WithDrawPincode')
        }
    }

    onBankList() {
        Picker.init({
            pickerData: ["Bank 1", "Bank 2", "Bank 3"],
            pickerTitleText: 'Please select currency',
            onPickerConfirm: data => { },
            onPickerCancel: data => { },
            onPickerSelect: data => { }
        });
        Picker.show();
    }

    renderSelection() {
        const { isVisibleTooltip } = this.state
        return (
            <View style={{ padding: 0 }}>
                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        alignItems: 'center'
                    }}
                    onPress={() => Actions.push('CreateBankAccount')}

                >
                    <View style={{ width: wp('8%'), height: hp('3%'), marginRight: hp('2%') }}>
                        <Image source={require('../../assets/icon/addbankacoount.png')} style={{ ...styles.imageContain }} />
                    </View>
                    <Text style={{ fontSize: hp('1.8%') }}>Add Bank Account</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        marginTop: hp('1.5%')
                    }}
                    onPress={() => Actions.push('CreateCreditCard')}
                >
                    <View style={{ width: wp('8%'), height: hp('3%'), marginRight: hp('2%') }}>
                        <Image source={require('../../assets/icon/addcreditcard.png')} style={{ ...styles.imageContain }} />
                    </View>
                    <Text style={{ fontSize: hp('1.8%') }}>Add Credit / Debit Card</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        const { isVisibleMenuBankAndCredit } = this.state
        //onPress={() => Actions.push('CreateBankAccount')}
        return (
            <>
                <ImageBackground source={require('../../assets/image/bg_main_bottom.png')} style={{ flex: 1 }}>
                    <ImageBackground source={require('../../assets/image/BgHorizontal.png')} style={{ ...styleScoped.imageTop }} imageStyle={{ ...styleScoped.borderBottomRaduis }}>
                        <View style={{ paddingTop: hp('5%') }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', ...styles.container }}>
                                <Icon name="chevron-left" size={hp('4%')} color="#ED4F9D" onPress={() => Actions.push('Main')} />
                                <Text style={{ fontSize: hp('2.3%'), }}>My Wallet</Text>
                                <Popover
                                    placement="bottom"
                                    verticalOffset={hp('-3%')}
                                    isVisible={isVisibleMenuBankAndCredit}
                                    from={(
                                        <TouchableOpacity onPress={() => this.setState({ isVisibleMenuBankAndCredit: true })}>
                                            <Icon name="plus-circle-outline" size={hp('3.5%')} color="#ED4F9D" />
                                        </TouchableOpacity>
                                    )}>
                                    <View style={{ padding: hp('2%') }}>
                                        <TouchableOpacity
                                            style={{
                                                flexDirection: 'row',
                                                justifyContent: 'flex-start',
                                                alignItems: 'center'
                                            }}
                                            onPress={() => this.selectMenu('bankaccount')}>
                                            <View style={{ width: wp('8%'), height: hp('3%'), marginRight: hp('2%') }}>
                                                <Image source={require('../../assets/icon/addbankacoount.png')} style={{ ...styles.imageContain }} />
                                            </View>
                                            <Text style={{ fontSize: hp('1.8%') }}>Add Bank Account</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            style={{
                                                flexDirection: 'row',
                                                justifyContent: 'flex-start',
                                                alignItems: 'center',
                                                marginTop: hp('1.5%')
                                            }}
                                            onPress={() => this.selectMenu('creditcard')}>
                                            <View style={{ width: wp('8%'), height: hp('3%'), marginRight: hp('2%') }}>
                                                <Image source={require('../../assets/icon/addcreditcard.png')} style={{ ...styles.imageContain }} />
                                            </View>
                                            <Text style={{ fontSize: hp('1.8%') }}>Add Credit / Debit Card</Text>
                                        </TouchableOpacity>
                                    </View>
                                </Popover>
                            </View>

                            <View style={{ marginTop: hp('6%'), flexDirection: 'row', justifyContent: 'center' }}>
                                <View>
                                    <View style={{ width: "100%" }}>
                                        <Text style={{ fontSize: hp('4%'), color: '#12173C', textAlign: 'center' }}><Text style={{ color: '#ED4F9D' }}>฿</Text> 35,600</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width: "100%", marginTop: hp('1%') }}>
                                        <Icon name="refresh" size={hp('2.5%')} color="#979797" style={{ marginRight: hp('1%'), fontWeight: '200' }} />
                                        <Text style={{ fontSize: hp('1.8%'), color: '#12173C', fontWeight: '200', textAlign: 'center' }}>Balance at 8:59 PM</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{ paddingHorizontal: wp('20%'), marginTop: hp('3%') }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => this.openTopup('topup')}>
                                        <View style={{ width: wp('10%'), height: hp('5%') }}>
                                            <Image source={require('../../assets/icon/topup.png')} style={{ ...styles.imageContain }} />
                                        </View>
                                        <Text style={{ marginTop: hp('2.5%'), fontSize: hp('1.5%'), color: '#021038', textAlign: 'center' }}>เติมเงิน</Text>
                                    </TouchableOpacity>
                                    <View style={{ ...styles.dividerVertical }}></View>
                                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => this.openTopup('withdraw')}>
                                        <View style={{ width: wp('10%'), height: hp('5%') }}>
                                            <Image source={require('../../assets/icon/withdraw.png')} style={{ ...styles.imageContain }} />
                                        </View>
                                        <Text style={{ marginTop: hp('2.5%'), fontSize: hp('1.5%'), color: '#021038', textAlign: 'center' }}>ถอนเงิน</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </ImageBackground>
                    <View style={{ ...styles.container, marginTop: hp('5%') }}>
                        <TouchableOpacity onPress={() => Actions.push('CreditAndDetail')}>
                            <ImageBackground source={require('../../assets/image/bg_kbank.png')}
                                style={{
                                    paddingLeft: hp('2%'),
                                    paddingRight: hp('0.5%'),
                                    paddingVertical: hp('4%'),
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center'
                                }} imageStyle={{ borderRadius: hp('2%'), }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <View style={{ width: hp('4%'), height: hp('4%'), marginRight: hp('1.5%') }}>
                                        <Image source={require('../../assets/icon/kbank_white.png')} style={{ ...styles.imageContain }} />
                                    </View>
                                    <View>
                                        <Text style={{ fontSize: hp('1.8%'), color: 'white' }}>
                                            บัตรส่วนตัว
                                    </Text>
                                        <Text style={{ fontSize: hp('1.6%'), color: 'white' }}>
                                            xxxx-xxxx-xxxx-1924
                                    </Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                                    <View style={{
                                        borderRadius: 10,
                                        padding: hp('0.8%'),
                                        paddingHorizontal: hp('3%'),
                                        backgroundColor: 'rgba(0,0,0,0.16)',

                                    }}>
                                        <Text style={{ color: 'white', fontSize: hp('1.6%'), textAlignVertical: 'center' }}>บัตรหลัก</Text>
                                    </View>
                                    <Icon name="chevron-right" color="white" size={hp('3%')} />
                                </View>
                            </ImageBackground>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ ...styleScoped.boxCreateCredit, marginTop: hp('2%') }} onPress={() => Actions.push('CreateCreditCard')}>
                            <Icon name="plus-circle-outline" size={hp('7.5%')} color="rgba(255, 255, 255, 0.3)" style={{ marginRight: hp('2%') }} />
                            <View>
                                <Text style={{ fontSize: hp('2.3%'), color: 'white' }}>Add Bank Account</Text>
                                <Text style={{ fontSize: hp('2.3%'), color: 'white', textAlign: 'center' }}>or</Text>
                                <Text style={{ fontSize: hp('2.3%'), color: 'white' }}>Credit / Debit Card</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
                <RBSheet
                    ref={ref => {
                        this.RBSheet = ref;
                    }}
                    height={hp('60%')}
                    openDuration={250}
                    customStyles={{
                        container: {
                            ...styleScoped.boxBottomSheetTopup
                        }
                    }}
                >
                    <View>
                        <TouchableOpacity style={{ ...styleScoped.boxSelectBankAccount }} onPress={() => this.onBankList()}>
                            <View style={{ ...styleScoped.boxSelectBankAccountLeft }}>
                                <View style={{ ...styleScoped.boxImageSelectBankAccount }}>
                                    <Image source={require('../../assets/icon/kbank_bank_account.png')} style={{ ...styles.imageContain }} />
                                </View>
                                <View>
                                    <Text style={{ color: '#12173C' }}>บัญชีส่วนตัว</Text>
                                    <Text style={{ color: '#12173C' }}>xxxx-xxxx-xxxx-1924</Text>
                                </View>
                            </View>
                            <Icon name="chevron-down" size={hp('3%')} color="#12173C" />
                        </TouchableOpacity>
                        <View style={{ ...styleScoped.boxTopupAmount }}>
                            <Text style={{ ...styleScoped.titleTopupAmount }}>Top-up amount</Text>
                            <View style={{ ...styleScoped.boxInputAmount }}>
                                <Text style={{ fontSize: hp('3%'), color: '#ED4F9D', marginRight: hp('2%') }}>฿</Text>
                                <TextInput value="500" style={{ fontSize: hp('3%'), color: '#12173C', marginRight: hp('2%'), width: '80%', padding: 0 }}></TextInput>
                            </View>
                            <Text style={{ fontSize: hp('1.5%'), marginTop: hp('0.8%'), color: 'rgba(0,0,0,0.16)' }}>Avaliable from ฿50 to ฿50,000</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap', marginTop: hp('2%'), width: "100%" }}>
                                <TouchableOpacity style={{ ...styleScoped.boxSelectAmount }}>
                                    <Text style={{ ...styleScoped.textSelectAmount }}>50</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ ...styleScoped.boxSelectAmount }}>
                                    <Text style={{ ...styleScoped.textSelectAmount }}>100</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ ...styleScoped.boxSelectAmount }}>
                                    <Text style={{ ...styleScoped.textSelectAmount }}>300</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ ...styleScoped.boxSelectAmount }}>
                                    <Text style={{ ...styleScoped.textSelectAmount }}>500</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ ...styleScoped.boxSelectAmount }}>
                                    <Text style={{ ...styleScoped.textSelectAmount }}>1,000</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ ...styleScoped.boxSelectAmount }}>
                                    <Text style={{ ...styleScoped.textSelectAmount }}>3,000</Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: "flex-start", marginTop: hp('3%'), ...styles.container }}>
                            <TouchableOpacity
                                style={{ ...styles.btnSecondary, marginRight: hp('1%'), width: hp('15%'), borderColor: '#ED4F9D' }}
                                onPress={() => this.RBSheet.close()}
                            >
                                <Text style={{ textAlign: 'center', fontSize: hp('2%'), color: "#ED4F9D" }}>Cancle</Text>
                            </TouchableOpacity>
                            <Button title="Confirm" buttonStyle={{ ...styles.btnPrimary, width: '90%' }} onPress={() => this.confirmTopup()} />
                        </View>
                    </View>
                </RBSheet>
            </>
        )
    }






};


const styleScoped = StyleSheet.create({
    boxSelectBankAccount: {
        flexDirection: 'row',
        justifyContent: "space-between",
        padding: hp('1%'),
        borderRadius: 20,
        backgroundColor: 'white',
        alignItems: "center"
    },
    boxSelectBankAccountLeft: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    boxImageSelectBankAccount: {
        width: hp('4%'),
        height: hp('4%'),
        marginRight: hp('1%')
    },
    boxBottomSheetTopup: {
        paddingHorizontal: hp('2%'),
        paddingVertical: hp('4%'),
        backgroundColor: '#F2F2F7',
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
    },
    boxTopupAmount: {
        marginTop: hp('2%'),
        padding: hp('2%'),
        borderRadius: 20,
        backgroundColor: 'white',
    },
    boxInputAmount: {
        paddingVertical: hp('1%'),
        borderBottomColor: '#C6C6C6',
        borderBottomWidth: 1,
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center'
    },
    titleTopupAmount: {
        fontSize: hp('1.8%'),
        color: '#12173C',
        fontWeight: '300'
    },
    boxSelectAmount: {
        width: '30%',
        paddingVertical: hp('1.5%'),
        borderRadius: 50,
        borderColor: '#C6C6C6',
        borderWidth: 1,
        marginRight: hp('1%'),
        marginBottom: hp('1%')
    },
    textSelectAmount: {
        fontSize: hp('2%'),
        fontWeight: '300',
        color: '#12173C',
        textAlign: 'center'
    },
    imageTop: {
        width: '100%',
        position: 'relative',
        zIndex: 10,
        resizeMode: 'repeat',
        paddingBottom: hp('3%')
    },
    borderBottomRaduis: {
        borderBottomRightRadius: hp('5%'),
        borderBottomLeftRadius: hp('5%'),
    },
    boxCreateCredit: {
        padding: hp('2.5%'),
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        borderRadius: hp('2%'),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    },
    wrapperBoxTooltip: {
        padding: 0
    }

})


