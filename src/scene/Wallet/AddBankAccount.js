import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'
import { Button } from 'react-native-elements';




export default class CreateBankAccount extends Component {



    render() {
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Add Bank Account" backTo="MyWallet" ></Navbar>
                <View style={{
                    flex: 1, 
                    position: 'relative',
                    zIndex: 20,
                    backgroundColor: '#F2F2F7',
                    paddingBottom: hp('3%'),
                    paddingTop: hp('3%')
                }}>

                    <View style={{}}>
                        <View style={{ ...styles.container }}>
                            <ImageBackground source={require('../../assets/image/bg_kbank.png')}
                                style={{ paddingVertical: hp('5%') }}
                                imageStyle={{ borderRadius: hp('2%'), }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                        <View style={{ width: wp('50%'), height: hp('8%') }}>
                                            <Image source={require('../../assets/image/kbank_detail.png')} style={{ ...styles.imageContain }} />
                                        </View>
                                    </View>

                                </View>
                                <Text style={{ textAlign: 'center', marginTop: hp('1%'), fontSize: hp('2%'), color: 'white' }}>ผูกบัญชีธนาคารกับ Happy Pay</Text>
                            </ImageBackground>
                            <View style={{ marginTop: hp('3%') }}>
                                <Button title="Link Account Now" buttonStyle={{ ...styles.btnPrimary }} onPress={() => Actions.push('AddBankAccountPincode')} />
                            </View>
                        </View>

                    </View>

                </View>



            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    boxListNameBankAccount: {
        backgroundColor: 'white',
        paddingVertical: hp('1%'),
        ...styles.container,
    },
    listNameBankAccount: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginVertical: hp('1%')
    },
    titleNameBankAccount: {
        fontSize: hp('1.8%'),
        fontWeight: '200',
        color: '#12173C'
    },
    boxImageBankAccount: {
        height: hp('4.5%'),
        width: hp('4.5%'),
        marginRight: wp('4%')
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    }

})


