import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView
} from 'react-native';
import styles from '../../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../../components/Navbar'
import { Button } from 'react-native-elements';



export default class updateCradName extends Component {
    render() {
        return (

            <View style={{ flex: 1 }}>
                <Navbar name="Card Name" backTo="CreditAndDetail"></Navbar>

                <View style={{ ...styles.container, marginTop: hp('3%') }} >
                    <TextInput value="บัตรส่วนตัว" style={{
                        fontSize: hp('2%'),
                        color: '#12173C',
                        width:'100%',
                        padding:0
                    }}></TextInput>
                    <View style={{ ...styles.divider  , marginTop:hp('2%'), marginBottom:hp('3%')}}></View>
                    <Button title="Confirm" buttonStyle={{ ...styles.btnPrimary }} onPress={() => Actions.push('CreditAndDetail')} />
                </View>



            </View>

        )
    }
};


const styleScoped = StyleSheet.create({


})


