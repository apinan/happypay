import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'
import { Button } from 'react-native-elements';




export default class CreateCreditCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
            exp: null,
            card_number:null
        }
    }

    render() {
        const { exp , card_number } = this.state
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Add Credit / Debit Card" backTo="MyWallet" ></Navbar>
                <View style={{
                    flex: 1, position: 'relative',
                    zIndex: 20,
                    backgroundColor: '#F2F2F7',
                    paddingBottom: hp('3%'),
                    paddingTop: hp('3%')
                }}>

                    {/* section 1  */}
                    <View style={{ ...styleScoped.boxListSetting, ...styles.container }}>
                        <View style={{ ...styleScoped.listSetting }}>
                            <Text style={{ ...styleScoped.titleSetting, marginRight: wp('10%') }}>Credit/Debit No.</Text>
                            <TextInput
                                placeholder={"xxxx xxxx xxxx xxxx"}
                                maxLength={19}
                                value={card_number}
                                style={{ color: '#ED4F9D', fontSize: hp('1.9%'), fontWeight: '300', padding: 0 }}
                                keyboardType="number-pad"
                                onChangeText={text => {
                                    let cardOriginal = text.split(' ').join('')

                                    if (cardOriginal.length > 0) {
                                        cardOriginal = cardOriginal.match(new RegExp('.{1,4}', 'g')).join(' ')
                                    }

                                    let { card_number } = this.state
                                    card_number = cardOriginal
                                    this.setState({ card_number })
                                }}
                            ></TextInput>
                        </View>
                        <View style={{ ...styles.divider }}></View>
                        <TouchableOpacity style={{ ...styleScoped.listSetting }}>
                            <Text style={{ ...styleScoped.titleSetting, marginRight: wp('27%') }}>Name</Text>
                            <TextInput placeholder="Enter Name on card" style={{
                                color: '#ED4F9D',
                                fontSize: hp('1.9%'),
                                fontWeight: '300',
                                width: wp('50%'),
                                padding: 0
                            }}></TextInput>
                        </TouchableOpacity>
                    </View>
                    {/* end section 1 */}

                    {/* section 2  */}
                    <View style={{ ...styleScoped.boxListSetting, ...styles.container, marginTop: hp('2%') }}>
                        <View style={{ ...styleScoped.listSetting }}>
                            <Text style={{ ...styleScoped.titleSetting, marginRight: wp('30%') }}>EXP</Text>
                            <TextInput
                                placeholder="MM / YY"
                                value={exp}
                                style={{
                                    color: '#ED4F9D',
                                    fontSize: hp('1.9%'),
                                    fontWeight: '300',
                                    padding: 0
                                }}
                                maxLength={5}
                                keyboardType="number-pad"
                                onChangeText={text => {
                                    let cardOriginal = text.split('/').join('')

                                    if (cardOriginal.length > 0) {
                                        cardOriginal = cardOriginal.match(new RegExp('.{1,2}', 'g')).join('/')
                                    }

                                    let { exp } = this.state
                                    exp = cardOriginal
                                    this.setState({ exp })
                                }}
                            ></TextInput>
                        </View>
                        <View style={{ ...styles.divider }}></View>
                        <TouchableOpacity style={{ ...styleScoped.listSetting }}>
                            <Text style={{ ...styleScoped.titleSetting, marginRight: wp('29%') }}>CVV</Text>
                            <TextInput
                                placeholder="CVV"
                                style={{
                                    color: '#ED4F9D',
                                    fontSize: hp('1.9%'),
                                    fontWeight: '300',
                                    width: wp('48%'),
                                    padding: 0
                                }}
                                maxLength={3}
                                keyboardType="number-pad"
                            ></TextInput>
                            <Icon name="help-circle" size={hp('2.5%')} color="#ED4F9D" />
                        </TouchableOpacity>
                    </View>
                    {/* end section 2 */}

                    <View style={{ ...styles.container }}>
                        <View style={{ marginTop: hp('3%'), marginBottom: hp('1%'), flexDirection: 'row', justifyContent: 'flex-start', alignItems: "center" }}>
                            <Icon name="check-circle" size={hp('4%')} color="#0EC163" style={{ marginRight: hp('2%') }} />
                            <Text style={{ fontSize: hp('1.6%'), marginRight: hp('2%'), color: '#14133E' }}>ยอมรับ</Text>
                            <Text style={{ fontSize: hp('1.6%'), marginRight: hp('2%'), color: '#ED4F9D' }}>ข้อตกลงการใช้งาน</Text>
                        </View>
                        <Button title="Confirm" buttonStyle={{ ...styleScoped.btnLoginStyle }} onPress={() => Actions.push('CreateCreditCardPincode')} />
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: 'center', marginTop: hp('16%') }}>
                        <View style={{ width: wp('80%') }}>
                            <Text style={{ fontSize: hp('1.6%'), textAlign: 'center', fontWeight: '200', color: '#12173C' }}>Lorem Ipsum is simply dummy text of the printing and typesetting industry</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: 'center', marginTop: hp('2%') }}>
                        <View style={{ height: hp('4%'), width: hp('4%'), marginRight: hp('1%') }}>
                            <Image source={require('../../assets/image/visa.png')} style={{ ...styles.imageContain }} />
                        </View>
                        <View style={{ height: hp('4%'), width: hp('4%'), marginRight: hp('1%') }}>
                            <Image source={require('../../assets/image/master_card.png')} style={{ ...styles.imageContain }} />
                        </View>
                        <View style={{ height: hp('4%'), width: hp('4%'), marginRight: hp('1%') }}>
                            <Image source={require('../../assets/image/jcb.png')} style={{ ...styles.imageContain }} />
                        </View>
                    </View>


                </View>



            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    btnLoginStyle: {
        borderRadius: 50,
        backgroundColor: '#ED4F9D',
        padding: hp('1.8%'),
        fontSize: hp('2%')
    },
    boxInputSearch: {
        width: wp('72%'),
        padding: hp('1%'),
        backgroundColor: '#EEEEF0',
        borderRadius: hp('1%'),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    boxListSetting: {
        backgroundColor: 'white',
        marginTop: hp('1%'),
    },
    listSetting: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingVertical: hp('2%')
    },
    titleSetting: {
        fontSize: hp('1.8%'),
        fontWeight: '300',
        color: '#12173C'
    },
    boxListTransaction: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomColor: '#707070',
        borderBottomWidth: 0.5,
        paddingVertical: hp('2%')
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    }

})


