import React, { Component } from 'react';
import {
    View, Text, SafeAreaView, Image, StyleSheet, TextInput, TouchableOpacity, ScrollView, ImageBackground
} from 'react-native';
import styles from '../../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navbar from '../../components/Navbar'
import { Button } from 'react-native-elements';




export default class CreditAndDetail extends Component {



    render() {
        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <Navbar name="Credit card Detail" backTo="MyWallet" ></Navbar>
                <View style={{
                    flex: 1,
                    position: 'relative',
                    zIndex: 20,
                    backgroundColor: '#F2F2F7',
                    paddingBottom: hp('3%'),
                    paddingTop: hp('3%')
                }}>



                    <View >

                        <View style={{ ...styles.container }}>
                            <TouchableOpacity >
                                <ImageBackground source={require('../../assets/image/bg_kbank.png')}
                                    style={{ padding: hp('3%') }}
                                    imageStyle={{ borderRadius: hp('2%'), }}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-start' }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                                            <View style={{ width: hp('6%'), height: hp('6%') }}>
                                                <Image source={require('../../assets/icon/kbank_white.png')} style={{ ...styles.imageContain }} />
                                            </View>
                                        </View>

                                        <View style={{
                                            borderRadius: 10,
                                            paddingVertical: hp('1%'),
                                            paddingHorizontal: hp('3%'),
                                            backgroundColor: 'rgba(0,0,0,0.16)',
                                            // height: hp('4.5%')
                                        }}>
                                            <Text style={{ color: 'white', fontSize: hp('1.6%') }}>บัตรหลัก</Text>
                                        </View>
                                    </View>
                                    <View style={{ marginTop: hp('2%') }}>
                                        <Text style={{ fontSize: hp('2.3%'), color: 'white' }}>บัตรส่วนตัว </Text>
                                        <Text style={{ fontSize: hp('2%'), color: 'white' }}>xxxx-xxxx-xxxx-1924</Text>
                                    </View>
                                </ImageBackground>
                            </TouchableOpacity>
                        </View>

                        <View style={{ ...styleScoped.boxListNameBankAccount, marginTop: hp('2%') }}>
                            <TouchableOpacity style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ ...styleScoped.topic }}>Card Name</Text>
                                <View style={{ ...styleScoped.boxEditDetail }}>
                                    <Text style={{ ...styleScoped.detail }}>บัตรส่วนตัว</Text>
                                    <Icon name="chevron-right" size={hp('3%')} color="#12173C" onPress={() => Actions.push('UpdateCardName')} />
                                </View>
                            </TouchableOpacity>
                            <View style={{ ...styles.divider }}></View>
                            <View style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ ...styleScoped.topic }}>Default payment option</Text>
                                <Icon name="check-circle" size={hp('3%')} color="#0EC163" />
                            </View>
                            <View style={{ ...styles.divider }}></View>
                            <TouchableOpacity style={{ ...styleScoped.boxInfoMerchant }}>
                                <Text style={{ ...styleScoped.topic }}>Daily Transaction limit</Text>
                                <View style={{ ...styleScoped.boxEditDetail }}>
                                    <Text style={{ ...styleScoped.detail }}>90,000</Text>
                                    <Icon name="chevron-right" size={hp('3%')} color="#12173C" onPress={() => Actions.push('UpdateTransactionLimit')} />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>



            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    boxListNameBankAccount: {
        backgroundColor: 'white',
        paddingVertical: hp('1%'),
        ...styles.container,
    },
    listNameBankAccount: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginVertical: hp('1%')
    },
    titleNameBankAccount: {
        fontSize: hp('1.8%'),
        fontWeight: '200',
        color: '#12173C'
    },
    boxImageBankAccount: {
        height: hp('4.5%'),
        width: hp('4.5%'),
        marginRight: wp('4%')
    },
    shadow: {
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
    },
    topic: {
        fontSize: hp('2%'),
        color: '#12173C',
        fontWeight: '300'
    },
    detail: {
        fontSize: hp('2%'),
        color: '#ED4F9D',
        fontWeight: '300'
    },
    boxEditDetail: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    boxInfoMerchant: {
        paddingVertical: hp('2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },

})


