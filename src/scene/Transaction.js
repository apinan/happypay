import React, { Component } from 'react';
import {
    View, Text, Image, StyleSheet, TouchableOpacity, ImageBackground, Platform, StatusBar, Animated, ScrollView
} from 'react-native';
import styles from '../styles/base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import theme from '../constants'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Carousel from '../components/Carousel'
import Transacetion from '../scene/Main/Transacetion'
import MainScene from '../scene/Main/MainScene'
import Setting from '../scene/Main/Setting'


import { Scene, Stack, Router } from 'react-native-router-flux';

export default class Main extends Component {

    constructor(props) {
        super(props)
        this.getImageUser()
        this.RefreshRotateValue = new Animated.Value(0)
    }

    state = {
        userImage: require('../assets/image/Profile_ex.png'),
        scene: 'Transaction'
    }

    async getImageUser() {
        let result = await AsyncStorage.getItem('googleLogin')
        if (result) {
            let userInfo = JSON.parse(result)
            this.setState({ userImage: { uri: userInfo.photo } })
        }
    }

    async getUserPincode() {
        try {
            let user_pincode = await AsyncStorage.getItem('user_pincode')
            if (!user_pincode) {
                Actions.push('RegisterEnterNewPin')
            }
        } catch (error) {

        }
    }

    componentDidMount() {
        this.getUserPincode()
    }

    render() {
        const { userImage, scene } = this.state
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <StatusBar barStyle="dark-content" backgroundColor="transparent" translucent={true} />
                { scene == 'Main' ? <MainScene></MainScene> : null}
                {
                    scene == 'Transaction' ?
                        <View style={{ ...styleScoped.sectionMain }}>
                            <Transacetion></Transacetion>
                        </View>
                        : null
                }
                { scene == 'Setting' ?
                    <View style={{ ...styleScoped.sectionMain , backgroundColor: '#F2F2F7', }}>
                        <Setting></Setting>
                    </View>
                    : null
                }

                {/* menu bar */}
                <View style={{ height: hp('15%'), marginTop: hp('-5%'), position: 'relative', zIndex: 0 }}>
                    <ImageBackground source={require('../assets/image/bg_main_bottom.png')} style={{ flex: 1 }}>
                        <View style={{ paddingTop: hp('6%'), ...styles.container }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: hp('1%') }}>
                                <TouchableOpacity onPress={() => this.setState({ scene: 'Main' })}>
                                    <View style={{ height: hp('3%'), width: wp('20%') }}>
                                        {
                                            scene == 'Main'
                                                ? <Image source={require('../assets/image/menu_home_active.png')} style={{ ...styles.imageContain }} />
                                                : <Image source={require('../assets/image/menu_home.png')} style={{ ...styles.imageContain }} />
                                        }
                                        {/* <Image source={require('../assets/image/menu_home_active.png')} style={{ ...styles.imageContain }} /> */}
                                    </View>
                                    <Text style={{ textAlign: 'center', marginTop: hp('1%'), color: 'white', fontSize: hp('1.6%') }}>Home</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({ scene: 'Transaction' })}>
                                    <View style={{ height: hp('3%'), width: wp('20%') }}>
                                        {
                                            scene == 'Transaction'
                                                ? <Image source={require('../assets/image/menu_transaction_active.png')} style={{ ...styles.imageContain }} />
                                                : <Image source={require('../assets/image/menu_transaction.png')} style={{ ...styles.imageContain }} />
                                        }
                                        {/* <Image source={require('../assets/image/menu_transaction.png')} style={{ ...styles.imageContain }} /> */}
                                    </View>
                                    <Text style={{ textAlign: 'center', marginTop: hp('1%'), color: '#979797', fontSize: hp('1.6%') }}>Transaction</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.setState({ scene: 'Setting' })}>
                                    <View style={{ height: hp('3%'), width: wp('20%') }}>
                                        {
                                            scene == 'Setting'
                                                ? <Image source={require('../assets/image/menu_setting_active.png')} style={{ ...styles.imageContain }} />
                                                : <Image source={require('../assets/image/menu_setting.png')} style={{ ...styles.imageContain }} />
                                        }
                                        {/* <Image source={require('../assets/image/menu_setting.png')} style={{ ...styles.imageContain }} /> */}
                                    </View>
                                    <Text style={{ textAlign: 'center', marginTop: hp('1%'), color: '#979797', fontSize: hp('1.6%') }}>Setting</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ImageBackground>
                </View>
            </View>

        )
    }
};


const styleScoped = StyleSheet.create({
    sectionMain: {
        flex: 1, position: 'relative',
        zIndex: 20,
        // showdow
        shadowColor: "#480018",
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.2,
        shadowRadius: 20,
        elevation: 10,
        // end showdow
        backgroundColor: 'white',
        borderBottomLeftRadius: hp('5%'),
        borderBottomRightRadius: hp('5%'),
        paddingBottom: hp('3%'),
        // paddingTop: hp('5%')
    },
    imageProfile: {
        width: hp('5%'),
        height: hp('5%'),
        backgroundColor: '#ED4F9D',
        borderRadius: 100,
    },
    imageLogo: {
        height: hp('5%'),
        width: wp('30%')
    },
    textNameWallet: {
        fontSize: hp('3%'),
        color: theme.colors.text
    },
    textAmount: {
        fontSize: hp('5%'),
        color: theme.colors.text
    },
    boxTextNameWallet: {
        padding: hp('2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boxTextAmount: {
        paddingHorizontal: hp('3%'),
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: Platform.OS === 'ios' ? hp('1%') : hp('0%'),
    },
    boxTimeWallet: {
        paddingHorizontal: hp('3%'),
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: hp('0.8%'),
        alignItems: 'center'
    },
    textTimeWallet: {
        fontSize: hp('2%'),
        color: 'rgba(0,0,0,0.6)',
        fontWeight: '300'
    },
    boxMenu: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    boxMenuItem: {
        paddingHorizontal: Platform.OS === 'ios' ? hp('2%') : hp('3%')
    },
    boxImageMenu: {
        height: hp('5%'),
        width: hp('10%')
    },
    textMenu: {
        textAlign: 'center',
        marginTop: hp('2%'),
        fontSize: Platform.OS === 'ios' ? hp('1.8%') : hp('2%'),
        color: theme.colors.text,
    },
    boxTitleFavorite: {
        marginTop: hp('1.2%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    textTitleFavorite: {
        fontSize: hp('2.5%'),
        color: theme.colors.text
    },
    boxImageFavorite: {
        height: hp('8%'),
        width: hp('8%'),
        borderRadius: 50,
        backgroundColor: theme.colors.primary,
        alignItems: 'center'
    },
    textNameFavorite: {
        textAlign: 'center',
        fontSize: Platform.OS === 'ios' ? hp('1.5%') : hp('1.8%'),
        color: theme.colors.text
    },
    boxItemFavorite: {
        marginRight: wp('5%'),
        alignItems: "center"
    },
    linearGradient: {
        height: hp('75%'),
        borderBottomRightRadius: hp('13%'),
    },
    noti: {
        width: hp('1%'),
        height: hp('1%'),
        backgroundColor: 'red',
        borderRadius: hp('50%'),
        position: 'absolute',
        right: 3
    },
    boxImageBackgroundWallet: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
        borderRadius: 25,
        ...Platform.select({
            ios: {
                shadowColor: "#480018",
                shadowOffset: {
                    width: 0,
                    height: 10,
                },
                shadowOpacity: 0.2,
                shadowRadius: 20,
            },
            android: {
                elevation: 15,
                backgroundColor: 'rgba(0,0,0,0.01)',
                // backgroundColor: 'white',
                // borderRadius:20
            },
        }),
    },
    shadow: {

    }

})


